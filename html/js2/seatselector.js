// (function( $ ) {
// $.fn.seatSelection = function(options) {
//  var settings = $.extend({
//                           layout: "",
//                           occupied: "",
//                           }, options);

//  var s14A = [
//   [1,0,0,0,1],
//   [0,1,1,1,1],
//   [0,1,1,1,1,],
//   [1,1,1,1,1]
// ];
// var s15A = [
//   [1,0,0,0,1],
//   [1,1,1,1,1],
//   [0,1,1,1,1,],
//   [1,1,1,1,1]
// ];

// var bus_layouts = {
//   "A14": s14A,
//   "A15": s15A
// };
// var n14A = [
//   [1,  0, 0,  0, 11],
//   [0,  2, 5, 8, 12],
//   [0,  3, 6, 9, 13],
//   ["d", 4,7, 10, 14]
// ];

// var n15A = [
//   [2,  0, 0,  0, 15],
//   [1,  5, 8, 11, 14],
//   [0,  4, 7, 10, 13],
//   ["d", 3,6, 9, 12]
// ];

// var s_number = {
//   "A14": n14A,
//   "A15": n15A
// };

//     if(settings.layout !="" || settings.layout != null && bus_layouts[settings.layout]){
//       slayout = bus_layouts[settings.layout];
//       snumber = s_number[settings.layout];

//       for(var i= 0; i < slayout.length; i++){
//        list = $("<ul></ul>").addClass("seat-list");

//         $(this).append(list);
//         for(var j=0; j < slayout[i].length; j++){
//           if(slayout[i][j] == 1){
//             occupied = $.inArray(snumber[i][j], settings.occupied)
//             if(occupied != -1){
//               list.append($("<li></li>").text("x").attr('disabled', 'disabled').addClass("seat-list-item occupied"));
//             }
//             else{
//               list.append($("<li></li>").addClass("seat-list-item").html($("<span></span>").text(snumber[i][j])));
//               }
//           }
//           if(slayout[i][j] == 0){
//             list.append($("<li></li>").text("o").attr('disabled', 'disabled').addClass("seat-list-item empty"));
//           }
//         }
//       }
//    }
//    seat_selected = [];
//    $(this).append(list);
//    var selector = $(this).attr('class');

//    $(document.body).on('click', '.seat-list-item', function(){

//     text = parseInt($(this).text());

//     if(!isNaN(text)){
//       contains = $.inArray(text, seat_selected);

//       if(contains != -1){
//         seat_selected.splice(contains, 1);
//         $(this).removeClass("seat-selected");
//         $("."+selector).attr("data-payload", seat_selected.join(","));
//       }
//       else{
//         // if(settings.chargedPassengers > seat_selected.length){
//           seat_selected.push(text);
//           $(this).addClass("seat-selected");
//           $("."+selector).attr("data-payload", seat_selected.join(","));

//         // }
//         // else{
//         //   alert("seat chosen for all passengers");
//         // }
//       }
//     }
//    });
// };
// }(jQuery));
(function( $ ) {
$.fn.seatSelection = function(options) {
 var settings = $.extend({
            layout: "",
            occupied: "",
            chargedPassengers: "",
        }, options);

 var s14A = [
	[1,0,0,0,1],
  [0,1,1,1,1],
  [0,1,1,1,1,],
  [1,1,1,1,1]
];
var s15A = [
	[1,0,0,0,1],
  [1,1,1,1,1],
  [0,1,1,1,1,],
  [1,1,1,1,1]
];

var s15B = [
	[1,0,0,1,1],
  [0,1,1,1,1],
  [0,1,1,1,1,],
  [1,1,1,1,1]
];



var bus_layouts = {
	"A14": s14A,
  "A15": s15A,
  "B15": s15B
};

var n14A = [
	[1,  0, 0,  0, 11],
  [0,  2, 5, 8, 12],
  [0,  3, 6, 9, 13],
  ["d", 4,7, 10, 14]
];

var n15A = [
	[2,  0, 0,  0, 15],
  [1,  5, 8, 11, 14],
  [0,  4, 7, 10, 13],
  ["d", 3,6, 9, 12]
];

var n15B = [
	[1,  0, 0,  11, 15],
  [0,  4, 7, 10, 14],
  [0,  3, 6, 9, 13],
  ["d", 2,5, 8, 12]
];


var s_number = {
	"A14": n14A,
  "A15": n15A,
  "B15": n15B
};

   if(settings.layout !="" || settings.layout != null && bus_layouts[settings.layout]){
   		slayout = bus_layouts[settings.layout];
   		snumber = s_number[settings.layout];

      for(var i= 0; i < slayout.length; i++){
       list = $("<ul></ul>").addClass("list");

   			$(this).append(list);
      	for(var j=0; j < slayout[i].length; j++){
          if(slayout[i][j] == 1){
          	occupied = $.inArray(snumber[i][j], settings.occupied)
          	if(occupied != -1){
            	list.append($("<li></li>").text("x").attr('disabled', 'disabled').addClass("occupied"));
            }
            else{
        			list.append($("<li></li>").html($("<span></span>").text(snumber[i][j])));
              }
          }
          if(slayout[i][j] == 0){
          	list.append($("<li></li>").text("o").attr('disabled', 'disabled').addClass("empty"));
          }
        }
      }
   }
   seat_selected = [];
   $(this).append(list);
   var selector = $(this).attr('class');

   $(".list>li").click(function(){

   	text = parseInt($(this).text());

    if(!isNaN(text)){
    	contains = $.inArray(text, seat_selected);
      if(contains != -1){
				seat_selected.splice(contains, 1);
         $(this).removeClass("selected");
      }else{
      	if(settings.chargedPassengers <= seat_selected.length){
        	seat_selected.push(text);
        	$(this).addClass("selected");
          $("."+selector).attr("data-payload", seat_selected.join(","));

        }
        else{
        	alert("seat chosen for charged passengers");
        }
      }
    }
   });
};
}(jQuery))