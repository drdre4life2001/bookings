<?php

namespace App\Http\Controllers;

use App\NyscBooking;
use App\NyscPassenger;
use App\NyscTrip;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Curl;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Faker\Factory as Faker;
use Auth;
use Mail;
use Session;


class BookingsController extends Controller
{


    public function Add(Request $request, $trip_id, $trip_fare, $passport, $psg, $children, $infants, $date,
                        $end = null)
    {


        //dd($request);
        $bus_fee = env("BUS_FEE");
        if (Session::has('round_trip_stage') && Session::get('round_trip_stage') == 1) {
            Session::set('round_trip_stage', 2);
            Session::set('first_trip_id', $trip_id);
            Session::set('first_trip_date', $date);
            return redirect()->route('loading');
        }
        $p_type = $passport !== 0 ? $passport : "";
        $user = Auth::user();
        $return_date = (date('Y-m-d', $end));
        if (Session::has('round_trip_stage') && Session::get('round_trip_stage') == 2) {
            $trip = Curl::to(config('api.online.get-trip'))
                ->withData(['trip_id' => Session::get('first_trip_id'), 'departure_date' => Session::get('first_trip_date')])
                ->post();
            $trip = (json_decode($trip)->data);
            $fare = $trip_fare !== 0 ? $trip_fare : $trip->fare;
            $return_trip = Curl::to(config('api.online.get-trip'))
                ->withData(['trip_id' => $trip_id, 'departure_date' => $date])
                ->post();
            $return_trip = (json_decode($return_trip)->data);
            $return_date = $date;
            $passport_option = $request->passport_type;
            $date = Session::get('first_trip_date');
            $final_cost = $psg * $fare + ((1 - ($trip->operator->local_children_discount / 100)) * $trip->fare *
                    $children);
//            $final_cost += $psg * $return_trip->fare + ((1 - ($trip->operator->local_children_discount / 100)) * $trip->fare * $children);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     $final_cost += $psg * $return_trip                                                ->fare + ((1 - ($trip->operator->local_children_discount / 100)) * $trip->fare * $children);
        } else {

            $trip = Curl::to(config('api.online.get-trip'))
                ->withData(['trip_id' => $trip_id, 'departure_date' => $date])
                ->post();
            //dd($trip);
            $trip = (json_decode($trip)->data);
            $fare = $trip_fare !== 0 ? $trip_fare : $trip->trip->fare;
            $final_cost = $psg * $fare + ((1 - ($trip->operator->local_children_discount / 100)) *
                    $fare * $children);
        }
        $final_cost += config('custom.convenience_fee');
        $occupied_seats = [];
        collect($trip->seats)->each(function ($item, $key) use (&$occupied_seats) {
            if ($item->seat_no != "") {
                $occupied_seats[] = $item->seat_no;
            }
        });

        if ($request->isMethod('post')) {

            $ip =  \Request::ip();
            $today = date('Y-m-d');
            $today = strtotime($today);
            $departure = strtotime($request->departure_date);
            $user = $request->final_cost;
            $user_cost = (int)$user;
            if( $final_cost != $user_cost){

                return redirect()->back();
            }
            $this->validate($request, [

                'contact_name' => 'required',
                'contact_email' => 'required|email',
                'contact_phone' => 'required|digits:11',
                'kin_name' => 'required',
                'kin_phone' => 'required|numeric|digits:11',
            ]);

            $seat = $request->selected_seats;

            $basefare = $trip->trip->international !== 0 ? $trip_fare : $trip->trip->fare;
            if (Session::has('round_trip_stage') && Session::get('round_trip_stage') == 2)

                $basefare += $return_trip->fare;
            $infants = ($request->infant_count != "" || $request->infant_count != null) ? $request->infant_count : $infants;
            $children = ($request->children_count != "" || $request->children_count != null) ? $request->children_count : $children;
            $infant_seats = 0;
            if ($infants > 0) {
                $infant = [];
                $counter = (int)$infants;
                for ($j = 1; $j <= $counter; $j++) {
                    $infantsx = 'infants' . $j;
                    $i_age = 'i_age' . $j;
                    $i_gender = 'i_gender' . $j;
                    $infant[$j]['name'] = $request->$infantsx;
                    $infant[$j]['age'] = $request->$i_age;
                    $infant[$j]['gender'] = $request->$i_gender;
                    $infant_arr = get_infant_discount($infant[$j], unserialize($trip->operator->rule));
                    $infant_arr['discount_percentage'] = isset($infant_arr['discount_percentage']) ? $infant_arr['discount_percentage'] : 100;
                    $infant[$j]['price'] = $basefare - ($basefare * ($infant_arr['discount_percentage'] / 100));
                    $infant[$j]['discount_percentage'] = $infant_arr['discount_percentage'];
                    $final_cost += $infant[$j]['price'];
                    if (isset($infant_arr['discount_percentage']) && $infant_arr['discount_percentage'] != 100)
                        $infant_seats += 1;
                }
            }
            if (array_get($request, 'password')) {
                $this->validate($request, [
                    'contact_name' => 'required',
                    'contact_phone' => 'required',
                    'password' => 'required|confirmed',
                    'password_confirmation' => 'required'
                ]);
                $save_customer = Curl::to(config('api.online.save-customer'))
                    ->withData([
                        'name' => $request->input('contact_name'),
                        'email' => $request->input('contact_email'),
                        'phone' => $request->input('contact_phone'),
                        'password' => $request->input('password'),
                    ])
                    ->post();
                $customer = (json_decode($save_customer)->data);
            }

            if (!isset($customer)) {
                $save = Curl::to(config('api.online.save-customer'))
                    ->withData([
                        'name' => $request->input('contact_name'),
                        'email' => $request->input('contact_email'),
                        'phone' => $request->input('contact_phone'),
                        'password' => $request->input('password'),
                    ])
                    ->post();
                $get_customer = Curl::to(config('api.online.get-customer-data-from-phone'))
                    ->withData(['phone' => $request->contact_phone])
                    ->post();

                if (isset($get_customer)) {
                    $customer = isset($get_customer) ? (json_decode($get_customer)->data) : [];
                }
                //
            }

            $seats = explode(",", $request->selected_seats);
            $seat_index = 0;
            if ($request->international == 1) {
                switch ($request->passport_type) {
                    case($request->passport_type == 'virgin_passport');
                        $passport_cost = $trip->virgin_passport;
                        $final_cost += $passport_cost;
                        $p_cost = $passport_cost;
                        $is_intl = 1;
                        break;
                    case($request->passport_type == 'regular_passport');
                        $passport_cost = $trip->regular_passport;
                        $final_cost += $passport_cost;
                        $p_cost = $passport_cost;
                        $is_intl = 1;
                        break;
                    case($request->passport_type == 'no_passport');
                        $passport_cost = $trip->no_passport;
                        $final_cost += $passport_cost;
                        $p_cost = $passport_cost;
                        $is_intl = 1;
                        break;
                }
                $passport_expiry_date = $request->passport_expiry_date;
                $passport_issue_date = $request->passport_date_of_issue;
                $passport_issue_place = $request->passport_place_of_issue;
                $passport_no = $request->passport_number;
                $passport_type = $request->passport_type;
                $pss_cost = $fare;
            } else {
                $passport_expiry_date = null;
                $passport_issue_date = null;
                $passport_issue_place = null;
                $passport_no = null;
                $passport_type = null;
                $final_cost += 0;
                $pss_cost = 0;
                $is_intl = null;
            }

            $save = Curl::to(config('api.online.save-booking'))
                ->withData(array(
                    'trip_id' => $trip->trip->id,
                    'date' => $request->departure_date,
                    'passenger_count' => intval($psg + $children),
                    'unit_cost' => isset($trip->trip->fare) ? $trip->trip->fare : $fare,
                    'final_cost' => $final_cost,
                    'contact_name' => $request->contact_name,
                    'contact_email' => $request->contact_email,
                    'contact_phone' => $request->contact_phone,
                    'next_of_kin_name' => $request->kin_name,
                    'next_of_kin_phone' => $request->kin_phone,
                    'customer_id' => isset($customer->id) ? $customer->id : "",
                    'return_trip_id' => isset($return_trip) ? $return_trip->id : null,
                    'return_date' => isset($return_trip) ? $return_date : null,
                    'gender' => $request->gender,
                    'is_intl_trip' => $is_intl,
                    'seat' => isset($seats[$seat_index]) ? $seats[$seat_index] : $seats[0],
                    'passport_type' => $passport_type,
                    'passport_number' => $passport_no,
                    'passport_date_of_issue' => $passport_issue_date,
                    'passport_place_of_issue' => $passport_issue_place,
                    'passport_expiry_date' => $passport_expiry_date,
                    'departure_date' => $request->departure_date,
                    'passport_cost' => $pss_cost,
                    'user_ip' =>$ip
                ))
                ->post();

            $booking = (json_decode($save)->data);

            $booking_code = $booking->booking->booking_code;
            if ($psg > 1) {  //other passengers...
                for ($i = 1; $i < $psg; $i++) {
                    $pp = 'passengers' . $i;
                    $p_gender = 'p_gender' . $i;
                    $seat_index++;
                    $ptype = 'passport_type' . $i;
                    $pno = 'passport_number' . $i;
                    $pexp = 'passport_expiry_date' . $i;
                    $ppoi = 'passport_place_of_issue' . $i;
                    $pdoi = 'passport_date_of_issue' . $i;
                    Curl::to(config('api.online.save-psg'))
                        ->withData(array(
                            'booking_id' => $booking->booking->id,
                            'name' => $request->$pp,
                            'gender' => $request->$p_gender,
                            'seat' => isset($seats[$seat_index]) ? $seats[$seat_index] : $seats[0],
                            'age_group' => 'Adult',
                            'passport_no' => isset($request->$pno) ? $request->$pno : "",
                            'passport_type' => isset($request->$ptype) ? $request->$ptype : "",
                            'passport_expiry_date' => isset($request->$pexp) ? $request->$pexp : "",
                            'passport_place_of_issue' => isset($request->$ppoi) ? $request->$ppoi : "",
                            'passport_date_of_issue' => isset($request->$pdoi) ? $request->$pdoi : ""
                        ))->post();
                }
            }

            if ($children > 0) { //children
                for ($j = 1; $j <= $children; $j++) {
                    $infantx = 'child' . $j;
                    $i_age = 'child_age' . $j;
                    $i_gender = 'child_gender' . $j;
                    Curl::to(config('api.online.save-psg'))
                        ->withData(array(
                            'booking_id' => $booking->booking->id,
                            'name' => $request->$infantx,
                            'gender' => $request->$i_gender,
                            'age' => $request->$i_age,
                            'seat' => isset($seats[$seat_index]) ? $seats[$seat_index] : $seats[0],
                            'age_group' => 'Child',
                            'trip_id' => $trip->trip->id,
                        ))->post();
                }
            }

            if ($infants > 0) {
                for ($j = (1 + $children); $j <= ($infants + $children); $j++) {
                    $infantx = 'infants' . $j;
                    $i_age = 'i_age' . $j;
                    $i_gender = 'i_gender' . $j;
                    Curl::to(config('api.online.save-psg'))
                        ->withData(array(
                            'booking_id' => $booking->booking->id,
                            'name' => $request->$infantx,
                            'gender' => $request->$i_gender,
                            'age' => $request->$i_age,
                            'age_group' => 'Infant',
                            'trip_id' => $trip->trip->id,
                        ))->post();
                }
            }
            if ($request->selected_seats !== '') {
                $seats = explode(",", $request->selected_seats);
                for ($i = 0; $i < count($seats); $i++) {
                  $ch =  Curl::to(config('api.online.save-booked-seat'))
                        ->withData([
                            'trip_id' => $booking->booking->trip_id,
                            'seat_no' => $seats[$i],
                            'booking_id' => $booking->booking->id,
                            'departure_date' => $booking->booking->date
                        ])->post();
                }

            }
            $payRef = $request->transaction_ref;

            $trip = Curl::to(config('api.online.trip-from-booking-id'))
                ->withData(array('booking_code' => $booking_code))
                ->post();
            $bus_fee = env("BUS_FEE");

            $booking = (json_decode($trip)->data);
            $data = Curl::to(config('api.online.banks'))->get();
            $data = (json_decode($data)->data);
            $banks = $data->banks;
            $cash_offices = $data->cash_offices;
            $trip = $booking->trip;
            $psg = count($booking->passengers);
            $p_occupied_seats = [];
            $date = $booking->booking->date;
            $final_cost = $booking->booking->final_cost;
            $booking_code = $booking->booking->booking_code;
            $fare = $booking->booking->final_cost;
            $parent_booking_details = $booking->booking->parent_booking_id == 0 ? null : $booking->booking->parent_booking_id;

            if (!is_null($parent_booking_details)) {
                $parent_trip = Curl::to(config('api.online.get-customer-booking-by-id'))->withData(['booking_id' => $booking->booking->parent_booking_id])->post();
                $parent_booking = (json_decode($parent_trip)->data);
                collect($parent_booking->seats)->each(function ($itemx, $keyx) use (&$p_occupied_seats) {
                    if ($itemx->seat_no != "") {
                        $p_occupied_seats[] = $itemx->seat_no;
                    }
                });
                $p_occupied_seats = implode(",", $p_occupied_seats);
            }
            $occupied_seats = [];
            $booking->seats = json_encode($booking->seats);

            collect($booking->seats)->each(function ($item, $key) use (&$occupied_seats) {
                $new = json_decode($item,true);
             if (!empty($new['seat_no']) && $new['seat_no'] != "") {
                    $occupied_seats[] = $new['seat_no'];
               }
            });
            $occupied_seats = implode(",", $occupied_seats);
            $infants = $children = $adults = 0;
            collect($booking->passengers)->each(function ($item, $key) use (&$infants) {
                if ($item->age_group == 'Infant') {
                    $infants += 1;
                }
            });
            collect($booking->passengers)->each(function ($item, $key) use (&$children) {
                if ($item->age_group == 'Child') {
                    $children += 1;
                }
            });
            collect($booking->passengers)->each(function ($item, $key) use (&$adults) {
                if ($item->age_group == 'Adult') {
                    $adults += 1;
                }
            });
            $seat = $request->selected_seats;
            $data = compact('trip', 'psg', 'occupied_seats','seat', 'booking', 'p_occupied_seats', 'fare', 'parent_booking',
                'banks',
                'cash_offices');

            $secret_key = 'sk_test_ed387414b4b93ffc3aa7251ea091787a5ffef518';
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.paystack.co/transaction/verify/" . $payRef,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer " . $secret_key,
                    "cache-control: no-cache"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $new_response = json_decode($response, true);
            $status = $new_response['status'];


            if ($status == "true") {

                if (!is_null($parent_booking_details)) {
                    $total = number_format($booking->final_cost + $parent_booking->final_cost);
                    $msg = "Bus.com.ng purchase notice: Book Code: " . $parent_booking->booking_code . " \nfrom: " . $parent_booking->trip->name . "\nDetails are: " . $parent_booking->contact_name . "\nDeparture Date: " . date("D, d/m/Y", strtotime($parent_booking->date)) . "\nDeparture Time: " . $parent_booking->trip->departure_time . "\nTotal Passengers: " . $parent_booking->passenger_count . "\nSeats: " . $request->selected_seats . "\n Trip Cost: NGN " . $parent_booking->final_cost . "\nReturn Trip\nTrip Book Code: " . $booking->booking_code . " \nfrom: " . $booking->trip->name . "\nDetails are: " . $booking->contact_name . "\nDeparture Date: " . date("D, d/m/Y", strtotime($booking->date)) . "\nDeparture Time: " . $booking->trip->departure_time . "\nTotal Passengers: " . $booking->passenger_count . "\nSeats: " . $occupied_seats . "\n Trip Cost: " . $booking->final_cost . "\nTotal Trip Cost: NGN " . $total;
                    Mail::send('emails.send', $data, function ($message) use ($data) {
                        $message->subject("Bus.com.ng purchase notice : Ticket Confirmation");
                        $message->from(env('APP_EMAIL'), env('APP_NAME'));
                        $message->to($data['booking']->contact_email);
                    });


                } else {

                    $trans = Curl::to(config('api.online.save-transaction'))
                        ->withData(array('booking_id' => $booking->booking->id, 'booking_code' => $booking->booking->booking_code,
                            'departure_date' => $request->departure_date, 'unit_cost' => isset($trip->trip->fare) ? $trip->trip->fare : $fare, 'date' =>
                                $request->departure_date, 'trip_id' => $booking->trip->id,
                            'contact_name' => $request->contact_name,
                            'contact_email' => $request->contact_email,
                            'contact_phone' => $request->contact_phone,
                            'next_of_kin_name' => $request->kin_name,
                            'next_of_kin_phone' => $request->kin_phone,
                            'gender' => $request->gender,
                            'customer_id' => isset($customer->id) ? $customer->id : "",
                            'status' => "PAID", 'response' => "successfull", 'payment_method' => "online"))
                        ->post();
                    $status = (json_decode($trans)->status);
                    $chi_route = self::getChiscoTrips($booking);
                    $route = json_decode($chi_route);
                    if (isset($route->leaving[0]->scheduleId)) {

                        $sheduleID = $route->leaving[0]->scheduleId;

                        $avillableSeat = $chi_route->leaving[0]->availableSeat;
                        foreach ($avillableSeat as $seat) {
                            if ($seats == $seat) {
                                $chi_seat = $seats;
                            } else {
                                $chi_seat = $seats++;
                            }

                        }
                        $resp = self::saveChisco($booking, $route, $chi_seat);
                    }

                    $msg = "Bus.com.ng purchase notice:\nBooking Number: " . $booking->booking->booking_code . " \nfrom: " . $booking->trip->name . "\nDetails are: " . $booking->booking->contact_name . "\nDeparture Date: " . date("D, d/m/Y", strtotime($booking->booking->date)) . "\nDeparture Time: " . $booking->trip->departure_time . "\nTotal Passengers: " . count($booking->passengers) . "\nSeats: " . $request->selected_seats . "\nFinal Cost: NGN " . number_format($booking->booking->final_cost);
                    Mail::send('emails..bookings.paid', $data, function ($message) use ($data) {
                        $message->subject('Bus.com.ng Successful!');
                        $message->from(env('APP_EMAIL'), env('APP_NAME'));
                        $message->to($data['booking']->booking->contact_email);
                    });
                }
            } else {
                $total = number_format($booking->booking->final_cost + @$parent_booking->final_cost);
                if (!is_null($parent_booking_details)) {
                    $msg = "Bus.com.ng on hold:\nBooking Number: " . $parent_booking->booking_code . " \nfrom: "
                        . $parent_booking->trip->name . "\nDetails are: "
                        . $parent_booking->contact_name . "\n
                    Departure Date: " . date("D, d/m/Y", strtotime($parent_booking->date)) . "\n
                    Departure Time: " . $parent_booking->trip->departure_time . "\n
                    Total Passengers: " . $parent_booking->passenger_count . "\n
                    Seats: " . $request->selected_seats . "\n
                    Trip Cost: NGN " . $parent_booking->final_cost . "\n
                    Return Trip\n
                    Trip Book Code: " . $booking->booking_code . " \n
                    from: " . $booking->trip->name . "\n
                    Details are: " . $booking->contact_name . "\n
                    Departure Date: " . date("D, d/m/Y", strtotime($booking->date)) . "\n
                    Departure Time: " . $booking->trip->departure_time . "\n
                    Total Passengers: " . $booking->passenger_count . "\n
                    Seats: " . $occupied_seats . "\n
                    Trip Cost: " . $booking->final_cost . "\n
                    Total Trip Cost: NGN " . $total;
                    Mail::send('emails.send', $data, function ($message) use ($data) {
                        $message->from(env('APP_EMAIL'), env('APP_NAME'));
                        $message->to($data['booking']->contact_email);
                    });
                } else {

                    $msg = "Bus.com.ng on hold:\nBooking Number: " . $booking->booking->booking_code . " \nfrom: " . $booking->trip->name . "\nDetails are: " . $booking->booking->contact_name . "\nDeparture Date: " . date("D, d/m/Y", strtotime($booking->booking->date)) . "\nDeparture Time: " . $booking->trip->departure_time . "\nTotal Passengers: " . count($booking->passengers) . "\nSeats: " . $request->selected_seats . "\nFinal Cost: NGN " . number_format($booking->booking->final_cost);
                    Mail::send('emails.bookings.pending', $data, function ($message) use ($data) {
                        $message->subject('Bus.com.ng on hold');
                        $message->from(env('APP_EMAIL'), env('APP_NAME'));
                        $message->to($data['booking']->booking->contact_email);
                    });
                    Mail::send('emails.bookings.pending', $data, function ($message) use ($data) {
                        $message->subject('Bus.com.ng - New Pending Booking');
                        $message->from(env('APP_EMAIL'), env('APP_NAME'));
                        $message->to('info@bus.com.ng');
                    });
                }
                $msg .= "\n\nBank details for your Booking:\n";
                foreach ($banks as $bank) {
                    $msg .= "\nBank: " . $bank->name . " \nAccnt Name: " . $bank->account_name . "\nAccnt Num" . $bank->acount_number . " \n";
                }
            }

           $sms = send_sms($booking->booking->contact_phone, $msg);
            $step = 4;
            $name = $booking->destpark->name;
            if($booking->trip->operator_id ==42 && $booking->sourcepark->name == 'Okota [in Lagos]' || $booking->sourcepark->name == 'IYANA IPAJA [in Lagos]' )   {
                $sub = DB::select("SELECT * FROM sub_trips WHERE name = '$name' ");
                $otrip =$sub[0]->id;
                $resp = self::saveokeyson($booking,$seat,$otrip);
            }
            $booktype = $request->booktype;

            return view('bookings.book_success', compact('trip', 'psg', 'booking', 'bus_fee', 'on_hold', 'fare', 'step',
                'occupied_seats', 'adults', 'parent_booking', 'infants', 'children', 'date', 'final_cost', 'booking_code', 'booktype'));


//            return redirect()->route('payment_success', [$booking_code, $fare, $occupied_seats, $request->email, $request->final_cost, 'psg' => $psg, 'booktype' => $booktype,
//                'transaction_ref' => $request->transaction_ref]);

        }
        $round_trip = ($end != null) ? 1 : 0;
        $step = 2;
        $getCustomerDataUrl = config('api.online.get-customer-data');
        $intl = $trip->trip->international;
       // $booking =

        $data = Curl::to(config('api.online.banks'))->get();
        $data = (json_decode($data)->data);
        $banks = $data->banks;
        $cash_offices = $data->cash_offices;
        $paystack_key = env('PAYSTACK_KEY');

        return view('bookings.add', compact('trip', 'date','paystack_key', 'psg', 'booking', 'booking_code', 'banks', 'cash_offices', 'intl', 'return_date', 'step', 'children',
            'infants', 'p_type', 'fare', 'bus_fee', 'adults',
            'getCustomerDataUrl', 'final_cost', 'user', 'occupied_seats', 'round_trip', 'return_trip', 'return_date'));
    }

    public function getChiscoTrips($booking)
    {

        $routeId = $booking->trip->trip_code;
        $departureDate = $booking->booking->date;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://sandboxadmin.chiscotransport.com.ng/api/v1/SearchBooking/LoadSchedules",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"routeId\":\"$routeId\", \"dapartureDate\":\"$departureDate\"}",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
                "x-apikey: 7e36afbe9f01cb84d1d9579a53623e9398c8eaaf",
                "x-userid: e4fc9a1c-ace1-e711-80ee-001c4281ad62"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return $response;
        }


    }
//save booking to okeyson db

    public function saveOkeyson($booking,$seat,$trip_id){
           if (isset($booking)){
               //dd($booking->booking->id);
               $booking_id = $booking->booking->id;
        $booking_code = $booking->booking->booking_code;
        $date = $booking->booking->date;
        $contact_name = $booking->booking->contact_name;
        $contact_phone = $booking->booking->contact_phone;
        $passenger_count = $booking->booking->passenger_count;
        $contact_email = $booking->booking->contact_email;
        $contact_gender = $booking->booking->contact_gender;
        $next_of_kin = $booking->booking->next_of_kin;
        $next_of_kin_phone = $booking->booking->next_of_kin_phone;
        $unit_cost = $booking->booking->unit_cost;
        $final_cost = $unit_cost *$passenger_count;
        $seat_number = $seats = $seat;
        $payment_method = 1;
        $status = 'PAID';
        $booked_by = 'bus.com.ng';
        $created  = date('Y-m-d H:i:s');
        $age = 30;
        $operator_id = 1;
               $passenger = DB::insert("INSERT INTO passengers( daily_trip_id, age, 
              name,  gender, seat_no,  created_at) VALUES('$trip_id',
            '$age', '$contact_name', '$contact_gender','$seat_number', '$created') ");
               $trans = DB::insert("INSERT INTO transactions( operator_id, booking_id, 
              status,   created_at) VALUES('$operator_id',
            '$booking_id', '$status', '$created') ");


    $trips = DB::insert("INSERT INTO bookings( trip_id, date,passenger_count, unit_cost, final_cost, payment_method_id, status,paid_date, booking_code, 
              contact_name, contact_phone, contact_email, gender, seat_no, next_of_kin, next_of_kin_phone, created_at, booked_by) VALUES('$trip_id', '$date', '$passenger_count', '$unit_cost', '$final_cost',
           '$payment_method', '$status', '$date','$booking_code', '$contact_name', '$contact_phone', '$contact_email',  '$contact_gender','$seat_number', '$next_of_kin', '$next_of_kin_phone', '$created', '$booked_by') ");

    return $trips;


}
    }


    public function saveChisco($booking, $chi_route, $seat)
    {
        $chi_route = $chi_route->leaving[0]->scheduleId;


        $trip_schedule = $booking->trip->trip_code;
        $departure_date = $booking->booking->date;
        $resp = Curl::to(config('api.online.chisco-trips'))->withData(array('routeID' => $trip_schedule,
            'departureDate' => $departure_date))->post();


        $contact_name = $booking->booking->contact_name;
        $contact_phone = $booking->booking->contact_phone;
        $contact_email = $booking->booking->contact_email;
        $contact_gender = $booking->booking->contact_gender;
        $next_of_kin = $booking->booking->next_of_kin;
        $next_of_kin_phone = $booking->booking->next_of_kin_phone;


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://sandboxadmin.chiscotransport.com.ng/api/v1/Booking/VendorBookTicket",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "\n{\n    \"leaving\": {\n        \"tripScheduleId\":\" $chi_route\",\n        \"departureDateTime\":\"$departure_date\",\n   
      \"selectedSeats\":[ $seat]\n        \n    },\n    \"info\":{\n    \t\"passengers\":[\n    \t\t{\n    \t\t\"name\":\t \" $contact_name\",\n    \t\t\"gender\": \"$contact_gender\",\n  
        \t\t\"documentationType\": 0\n
          \t\t}\n    \t\t\n    \t\t],\n    \t\t\"contactInfo\": {\n    \t\t\t\"phone\":\"$contact_phone\" ,\n    \t\t\t\"email\": \"$contact_email\"\n    \t\t\t\n   
           \t\t},\n    \t\t\"nextOfKinInfo\": {\n    \t\t\t\"phone\":\"$next_of_kin_phone\" ,\n    \t\t\t\"email\": \"\"\n    \t\t}\n    }\n    \n}",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return $response;

        }


    }

    public function Payment(Request $request, $booking_code, $fare = null)
    {
        if (Session::has('round_trip_stage')) {
            Session::remove("round_trip_stage");
            Session::remove("second_trip_to");
            Session::remove("second_trip_from");
            Session::remove("second_trip_departing");
            Session::remove('first_search');
            Session::remove('second_search');
            Session::remove('round_trip_passengers');
            Session::remove('round_trip_infants');
            Session::remove('first_trip_id');
            Session::remove('first_trip_date');

        }
        $bus_fee = env("BUS_FEE");
        $trip = Curl::to(config('api.online.trip-from-booking-id'))->withData(array('booking_code' => $booking_code))->post();
        $booking = (json_decode($trip)->data);
        $intl = $booking->booking->is_intl_trip;
        $parent_booking_details = $booking->booking->parent_booking_id == 0 ? null : $booking->booking->parent_booking_id;
        if (!is_null($parent_booking_details)) {
            $parent_trip = Curl::to(config('api.online.get-customer-booking-by-id'))->withData(['booking_id' => $booking->booking->parent_booking_id])->post();
            $parent_booking = (json_decode($parent_trip)->data);
        }

        $infants = $children = $adults = 0;
        collect($booking->passengers)->each(function ($item, $key) use (&$infants) {
            if ($item->age_group == 'Infant') {
                $infants += 1;
            }
        });

        collect($booking->passengers)->each(function ($item, $key) use (&$children) {
            if ($item->age_group == 'Child') {
                $children += 1;
            }
        });

        collect($booking->passengers)->each(function ($item, $key) use (&$adults) {
            if ($item->age_group == 'Adult') {
                $adults += 1;
            }
        });
        $data = Curl::to(config('api.online.banks'))->get();
        $data = (json_decode($data)->data);
        $banks = $data->banks;
        $cash_offices = $data->cash_offices;
        $trip = $booking->trip;
        $psg = count($booking->passengers);
        $date = $booking->booking->date;
        $today = date('Y-m-d');
        $date_timestamp = strtotime($date);
        $today_timestamp = strtotime($today);
        if ($today_timestamp > $date_timestamp) {
            return redirect()->intended('/')->with('error', 'Sorry! You cant pay for this ticket anymore!');
        }
        $final_cost = $booking->booking->final_cost;
        if (Session::remove("parent_booking_code"))
            Session::remove("parent_booking_code");
        $step = 3;

        return view('trips.payment', compact('trip', 'psg', 'booking', 'bus_fee', 'intl', 'booking_code', 'banks', 'fare', 'cash_offices', 'step', 'adults', 'infants', 'parent_booking', 'children', 'date', 'final_cost'));
    }

    public function SuccessPayment(Request $request, $booking_code, $on_hold = FALSE)
    {
        $trip = Curl::to(config('api.online.trip-from-booking-id'))
            ->withData(array('booking_code' => $booking_code))
            ->post();
        $bus_fee = env("BUS_FEE");
        $booking = (json_decode($trip)->data);
        $data = Curl::to(config('api.online.banks'))->get();
        $data = (json_decode($data)->data);
        $banks = $data->banks;
        $cash_offices = $data->cash_offices;
        $trip = $booking->trip;
        $psg = count($booking->passengers);
        $p_occupied_seats = [];
        $date = $booking->booking->date;
        $final_cost = $booking->booking->final_cost;
        $booking_code = $booking->booking->booking_code;
        $fare = $booking->booking->final_cost;
        $parent_booking_details = $booking->booking->parent_booking_id == 0 ? null : $booking->booking->parent_booking_id;
        if (!is_null($parent_booking_details)) {
            $parent_trip = Curl::to(config('api.online.get-customer-booking-by-id'))->withData(['booking_id' => $booking->booking->parent_booking_id])->post();
            $parent_booking = (json_decode($parent_trip)->data);
            collect($parent_booking->seats)->each(function ($itemx, $keyx) use (&$p_occupied_seats) {
                if ($itemx->seat_no != "") {
                    $p_occupied_seats[] = $itemx->seat_no;
                }
            });
            $p_occupied_seats = implode(",", $p_occupied_seats);
        }
        $occupied_seats = [];
        collect($booking->seats)->each(function ($item, $key) use (&$occupied_seats) {
            if ($item->seat_no != "") {
                $occupied_seats[] = $item->seat_no;
            }
        });
        $occupied_seats = implode(",", $occupied_seats);
        $infants = $children = $adults = 0;
        collect($booking->passengers)->each(function ($item, $key) use (&$infants) {
            if ($item->age_group == 'Infant') {
                $infants += 1;
            }
        });
        collect($booking->passengers)->each(function ($item, $key) use (&$children) {
            if ($item->age_group == 'Child') {
                $children += 1;
            }
        });
        collect($booking->passengers)->each(function ($item, $key) use (&$adults) {
            if ($item->age_group == 'Adult') {
                $adults += 1;
            }
        });
        $data = compact('trip', 'psg', 'occupied_seats', 'booking', 'p_occupied_seats', 'fare', 'parent_booking',
            'banks',
            'cash_offices');
        $booktype = $request->booktype;
        //dd($booktype);


        if ($booktype == "paystack") {

            $payRef = 1;

            $secret_key = env('sk_test_ed387414b4b93ffc3aa7251ea091787a5ffef518');
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.paystack.co/transaction/verify/" . $payRef,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer " . $secret_key,
                    "cache-control: no-cache"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $new_response = json_decode($response, true);
            $status = $new_response['status'];
            //dd($status);


            if (!is_null($parent_booking_details)) {
                $total = number_format($booking->final_cost + $parent_booking->final_cost);
                $msg = "Bus.com.ng purchase notice:\nBook Code: " . $parent_booking->booking_code . " \nfrom: " . $parent_booking->trip->name . "\nDetails are: " . $parent_booking->contact_name . "\nDeparture Date: " . date("D, d/m/Y", strtotime($parent_booking->date)) . "\nDeparture Time: " . $parent_booking->trip->departure_time . "\nTotal Passengers: " . $parent_booking->passenger_count . "\nSeats: " . $p_occupied_seats . "\n Trip Cost: NGN " . $parent_booking->final_cost . "\nReturn Trip\nTrip Book Code: " . $booking->booking_code . " \nfrom: " . $booking->trip->name . "\nDetails are: " . $booking->contact_name . "\nDeparture Date: " . date("D, d/m/Y", strtotime($booking->date)) . "\nDeparture Time: " . $booking->trip->departure_time . "\nTotal Passengers: " . $booking->passenger_count . "\nSeats: " . $occupied_seats . "\n Trip Cost: " . $booking->final_cost . "\nTotal Trip Cost: NGN " . $total;
                Mail::send('emails.send', $data, function ($message) use ($data) {
                    $message->subject("Bus.com.ng purchase notice : Ticket Confirmation");
                    $message->from(env('APP_EMAIL'), env('APP_NAME'));
                    $message->to($data['booking']->contact_email);
                });


            } else {


                $trans = Curl::to(config('api.online.save-transaction'))
                    ->withData(array('booking_id' => $booking->booking->id, 'booking_code' => $booking->booking->booking_code, 'status' => "PAID", 'response' => "successfull", 'payment_method' => "online", 'cancelled' => "false"))
                    ->post();
                $status = (json_decode($trans)->status);

                $msg = "Bus.com.ng purchase notice:\nBook Code: " . $booking->booking->booking_code . " \nfrom: " . $booking->trip->name . "\nDetails are: " . $booking->booking->contact_name . "\nDeparture Date: " . date("D, d/m/Y", strtotime($booking->booking->date)) . "\nDeparture Time: " . $booking->trip->departure_time . "\nTotal Passengers: " . count($booking->passengers) . "\nSeats: " . $occupied_seats . "\nFinal Cost: NGN " . number_format($booking->booking->final_cost);
                Mail::send('emails.bookings.paid', $data, function ($message) use ($data) {
                    $message->subject('Bus.com.ng Successful!');
                    $message->from(env('APP_EMAIL'), env('APP_NAME'));
                    $message->to($data['booking']->booking->contact_email);
                });
            }
        } else {
            $total = number_format($booking->booking->final_cost + @$parent_booking->final_cost);
            if (!is_null($parent_booking_details)) {
                $msg = "Bus.com.ng on hold:\nBook Code: " . $parent_booking->booking_code . " \nfrom: "
                    . $parent_booking->trip->name . "\nDetails are: "
                    . $parent_booking->contact_name . "\n
                    Departure Date: " . date("D, d/m/Y", strtotime($parent_booking->date)) . "\n
                    Departure Time: " . $parent_booking->trip->departure_time . "\n
                    Total Passengers: " . $parent_booking->passenger_count . "\n
                    Seats: " . $p_occupied_seats . "\n
                    Trip Cost: NGN " . $parent_booking->final_cost . "\n
                    Return Trip\n
                    Trip Book Code: " . $booking->booking_code . " \n
                    from: " . $booking->trip->name . "\n
                    Details are: " . $booking->contact_name . "\n
                    Departure Date: " . date("D, d/m/Y", strtotime($booking->date)) . "\n
                    Departure Time: " . $booking->trip->departure_time . "\n
                    Total Passengers: " . $booking->passenger_count . "\n
                    Seats: " . $occupied_seats . "\n
                    Trip Cost: " . $booking->final_cost . "\n
                    Total Trip Cost: NGN " . $total;
                Mail::send('emails.send', $data, function ($message) use ($data) {
                    $message->from(env('APP_EMAIL'), env('APP_NAME'));
                    $message->to($data['booking']->contact_email);
                });
            } else {
                $msg = "Bus.com.ng on hold:\nBook Code: " . $booking->booking->booking_code . " \nfrom: " . $booking->trip->name . "\nDetails are: " . $booking->booking->contact_name . "\nDeparture Date: " . date("D, d/m/Y", strtotime($booking->booking->date)) . "\nDeparture Time: " . $booking->trip->departure_time . "\nTotal Passengers: " . count($booking->passengers) . "\nSeats: " . $occupied_seats . "\nFinal Cost: NGN " . number_format($booking->booking->final_cost);
                Mail::send('emails.bookings.pending', $data, function ($message) use ($data) {
                    $message->subject('Bus.com.ng on hold');
                    $message->from(env('APP_EMAIL'), env('APP_NAME'));
                    $message->to($data['booking']->booking->contact_email);
                });
                Mail::send('emails.bookings.pending', $data, function ($message) use ($data) {
                    $message->subject('Bus.com.ng - New Pending Booking');
                    $message->from(env('APP_EMAIL'), env('APP_NAME'));
                    $message->to('info@bus.com.ng');
                });
            }
            $msg .= "\n\nBank details for your Booking:\n";
            foreach ($banks as $bank) {
                $msg .= "\nBank: " . $bank->name . " \nAccnt Name: " . $bank->account_name . "\nAccnt Num" . $bank->acount_number . " \n";
            }
        }
        send_sms($booking->booking->contact_phone, $msg);
        $step = 4;


        $booktype = $request->booktype;

        return view('bookings.book_success', compact('trip', 'psg', 'booking', 'bus_fee', 'on_hold', 'fare', 'step',
            'occupied_seats', 'adults', 'parent_booking', 'infants', 'children', 'date', 'final_cost', 'booking_code', 'booktype'));
    }

    public function Transactions(Request $request)
    {

        $trans = Curl::to(config('api.online.save-transaction'))
            ->withData(array('booking_id' => $request->booking_id, 'booking_code' => $request->booking_code, 'status' => $request->status, 'response' => $request->response, 'payment_method' => $request->payment_method, 'cancelled' => $request->cancelled))
            ->post();
        $status = (json_decode($trans)->status);
        dd($status);

        if ($request->parent_booking_id != "") {
            $trans = Curl::to(config('api.online.save-transaction'))
                ->withData(array('booking_id' => $request->parent_booking_id, 'booking_code' => $request->parent_booking_code, 'status' => $request->status, 'response' => $request->response, 'payment_method' => $request->payment_method, 'cancelled' => $request->cancelled))
                ->post();
        }
        echo $status;
    }

    public function CharterSuccessPayment(Request $request)
    {

        $charter = Curl::to(config('api.online.save-charter'))
            ->withData(
                array(
                    'pickup' => $request->pickup,
                    'dropoff' => $request->dropoff,
                    'contact_name' => $request->contact_name,
                    'start' => $request->start,
                    'operator' => $request->operator_id,
                    'contact_phone' => $request->contact_phone,
                    'contact_email' => $request->contact_email,
                    'phone_kin' => $request->phone_kin,
                    'kin' => $request->kin,
                    'passenger' => $request->passenger,
                    'budget' => $request->budget,
                    'pick_up_address' => $request->pickup_address,
                    'drop_off_address' => $request->dropoff_address
                )
            )->post();

        $charter = (json_decode($charter));
        if ((bool)$charter) {
            $message = "New Charter request at bus.com.ng by "
                . $request->contact_name . "($request->contact_phone)
        				from $request->pickup to  $request->dropoff
        				";
            $numbers = config('custom.charter_alert_numbers');
            for ($i = 0; $i < count($numbers); $i++) {
                if (!empty($numbers[$i])) {
                    send_sms($numbers[$i], $message);
                }
            }
            $msg = 'Dear Admin,' . "\n";
            $msg .= 'A new charter request has been made on Bus.com.ng' . "\n";
            $msg .= 'Details Below:' . "\n";
            $msg .= 'Name: ' . $request->contact_name . "\n";
            $msg .= 'Pickup Point: ' . $request->pickup . "\n";
            $msg .= 'Drop off Point: ' . $request->dropoff . "\n";
            $msg .= 'Phone Number: ' . $request->contact_phone . "\n";
            $msg .= 'Number of Passengers: ' . $request->passenger . "\n";
            $msg .= 'Pickup Address: ' . $request->pickup_address . "\n";
            $msg .= 'Drop off Address: ' . $request->dropoff_address . "\n";
            $msg .= 'Budget: ' . $request->budget . "\n";

            $data = [
                'name' => $request->contact_name,
                'email' => $request->contact_email,
                'subject' => 'New Charter Request',
                'message' => $msg,
                'option' => 'Charter'
            ];
            Mail::send('emails.plain', compact('data'), function ($message) {
                $message->from(env('APP_EMAIL'), env('APP_NAME'));
                $message->subject('Bus.com.ng Charter Request');
                $message->to('ada@bus.com.ng');
            });
            //send sms to the client
            $client_msg = "Hi $request->contact_name, " . env("CHARTER_MSG");
            send_sms($request->contact_phone, $client_msg);

        }
        $trip = Curl::to(config('api.online.trip-from-charter-id'))->withData(array('id' => $charter->data[0]->id))->post();
        $trip = (json_decode($trip)->data);

        if (!empty($data)) {

            $msg = 1;
            return view('pages.charter', compact('msg'));
            // return redirect()->route('charter',compact('alert'));
        }
        // return view('bookings.charter_success', array('trip' => $trip[0]));
    }

    public function Sms(Request $request)
    {

        $trip = Curl::to(config('api.online.trip-from-booking-id'))->withData(array('booking_code' => $request->booking_code))->post();
        $booking = (json_decode($trip)->data);

        if (array_get($request, 'cashier_name')) {

            $msg = "Cash office details for your Booking:\n\nCashier: " . $request->cashier_name . " \nPhone: " . $request->cashier_phone . "\nAddress " . $request->cashier_address . " \nAmount: N" . number_format($booking->final_cost) . "\nBooking code: " . $booking->booking_code;

            $resp = send_sms($booking->contact_phone, $msg);
            return json_encode(trim($resp));

        }

        $msg = "Bank details for your Booking:\n\nBank: " . $request->bank . " \nAccnt Name: " . $request->acct_name . "\nAccnt Num" . $request->acct_number . " \nAmount: N" . number_format($booking->final_cost) . "\nBooking code: " . $booking->booking_code;

        $resp = send_sms($booking->contact_phone, $msg);
        return json_encode(trim($resp));

    }

    public function getInfantTicketPrice(Request $request)
    {
        $infant_price_arr = [];
        //get operator rule
        $operator_rule = Curl::to(config('api.online.get-operator-booking-rule'))
            ->withData(['operator_id' => $request->operator_id])->post();
        $rule = (json_decode($operator_rule)->data);

        if ($rule)
            $rules = unserialize($rule->rules);

        $discount = get_infant_discount($request->infant, $rules);

        if (isset($discount['discount_percentage'])) {
            $infant_price_arr['infant_price'] = $request->unit_cost - ($request->unit_cost * ($discount['discount_percentage'] / 100));

            $infant_price_arr['discount'] = (int)$discount['discount_percentage'];
        }

        return $infant_price_arr;
    }

    public function book_nysc(Request $request, $trip_id = null, $fare = null)
    {
        $request_type = $request->isMethod('post');
        $trips = DB::select("SELECT id, fare, source_park_id, dest_camp_id, operator_id, (SELECT name FROM parks WHERE id = source_park_id) as park,
                    (SELECT name FROM nysc_camps WHERE id = dest_camp_id) as camp,
                    (SELECT name FROM operators WHERE id = operator_id) as operator, 
                    (SELECT code FROM operators WHERE id = operator_id) as operator_code, 
                    (SELECT img FROM operators WHERE id = operator_id) as operator_logo
                    FROM nysc_trips WHERE id = '$trip_id'");
        switch ($request_type) {
            case true;
                //Save Passenger
                $passenger = new NyscPassenger();
                $passenger->name = $request->name;
                $passenger->phone = $request->phone;
                $passenger->email = $request->email;
                $passenger->gender = $request->gender;
                $passenger->next_of_kin = $request->kin;
                $passenger->next_of_kin_phone = $request->kin_phone;
                $passenger->save();
                //Save Booking
                $booking = new NyscBooking();
                $booking->trip_id = $trip_id;
                $booking->source_park_id = $request->source_park_id;
                $booking->dest_camp_id = $request->dest_camp_id;
                $booking->booking_code = 'NYSC-' . $trips[0]->operator_code . rand(19999, 99999);
                $booking->operator_id = $trips[0]->operator_id;
                $booking->total_fare = $trips[0]->fare;
                $booking->payment_mode = $request->payment_mode;
                $booking->payment_reference = $request->transaction_ref;
                $booking->payment_status = 'On Hold';
                $booking->passenger_id = $passenger->id;
                $booking->travel_date = $request->travel_date;
                $booking->save();
                $step = 4;
                $bookings = $booking;
                $trips = $trips[0];
                //Redirect to Payment
                if ($request->payment_mode == 'online') {
                    //return redirect(route('pay_online', [$booking->payment_reference]));
                    $payment = $this->paystack_verification($request->transaction_ref);

                    if ($payment) {
                        $status = 'Paid';
                        $code = 200;
                        NyscBooking::where('payment_reference', $request->transaction_ref)->update(['payment_status' => $status]);
                        $passenger = NyscPassenger::where('id', $bookings->passenger_id)->first();
                        $on_hold = false;
                        $step = 4;
                        return view('trips.nysc.payment', compact('bookings', 'passenger', 'trips', 'on_hold', 'step', 'status', 'code'));
                    } else {
                        $on_hold = true;
                        $payment_failure = 'Sorry, payment failed. Contact administrator';
                        return view('trips.nysc.payment', compact('bookings', 'passenger', 'trips', 'on_hold', 'step', 'payment_failure'));
                    }
                } else {
                    $on_hold = true;
                    return view('trips.nysc.payment', compact('bookings', 'passenger', 'trips', 'on_hold', 'step'));
                }

                break;
            default;
                $step = 2;
                $trips = $trips[0];
                $paystack_key = env('PAYSTACK_KEY');
                return view('trips.nysc.book', compact('trips', 'paystack_key', 'step'));
                break;
        }

    }

    public function paystack_verification($ref)
    {
        $result = array();
        $url = 'https://api.paystack.co/transaction/verify/' . $ref;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt(
            $ch, CURLOPT_HTTPHEADER, [
                'Authorization: Bearer sk_test_47df907b6798049ba4fd92802ba053e305914953']
        );
        $request = curl_exec($ch);
        curl_close($ch);

        if ($request) {
            $result = json_decode($request, true);
            return $result;

        } else {

            return false;
        }
    }

    public function pay_with_amplify($ref = null)
    {
        $booking = NyscBooking::where('payment_reference', $ref)->first();

        $passenger = NyscPassenger::where('id', $booking['passenger_id'])->first();
        $data = [
            'merchantId' => 'NDTQEJ8ZXECEACQJVH60HW',
            'apiKey' => '8eb3c0fe-a335-41d5-9f42-9465cd690cec',
            'transID' => $ref,
            'customerEmail' => $passenger->email,
            'customerName' => $passenger->name,
            'Amount' => $booking->total_fare,
            'redirectUrl' => route('nysc_payment_success'),
            'paymentDescription' => "Payment for Bus tickets on Bus.com.ng",
            'planID' => 0
        ];
        $data = json_encode($data);
        $url = 'https://api.amplifypay.com/merchant/transact';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            )
        );
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        //dd($result);
        NyscBooking::where('id', $booking->id)->update(['payment_reference' => $result['TransactionRef']]);
        return Redirect::to($result['PaymentUrl']);
    }

    public function success_payment(Request $request)
    {
        $ref = $request->tran_response;
        $status = self::verify_amplify($ref);
        $body = $status;
        $booking = NyscBooking::where('payment_reference', $ref)->first();
        $trip_id = $booking->trip_id;
        $trips = DB::select("SELECT id, fare, source_park_id, dest_camp_id, operator_id, (SELECT name FROM parks WHERE id = source_park_id) as park,
                    (SELECT name FROM nysc_camps WHERE id = dest_camp_id) as camp,
                    (SELECT name FROM operators WHERE id = operator_id) as operator, 
                    (SELECT code FROM operators WHERE id = operator_id) as operator_code, 
                    (SELECT img FROM operators WHERE id = operator_id) as operator_logo
                    FROM nysc_trips WHERE id = '$trip_id'");
        $trips = $trips[0];
        $status = $body['StatusDesc'];
        $code = $body['StatusCode'];
        NyscBooking::where('payment_reference', $ref)->update(['payment_status' => $status]);
        $bookings = $booking;
        $passenger = NyscPassenger::where('id', $bookings->passenger_id)->first();
        $on_hold = false;
        $step = 4;
        return view('trips.nysc.payment', compact('bookings', 'passenger', 'trips', 'on_hold', 'step', 'status', 'code'));
    }

    public function verify_amplify($ref = null)
    {
        $data = [
            'merchantId' => 'NDTQEJ8ZXECEACQJVH60HW',
            'apiKey' => '8eb3c0fe-a335-41d5-9f42-9465cd690cec',
            'transactionRef' => $ref
        ];
        $data = json_encode($data);
        $url = 'https://api.amplifypay.com/merchant/verify';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            )
        );
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }

    /*  public function paystack_verification($ref)
      {
          $result = array();
          $url = 'https://api.paystack.co/transaction/verify/'.$ref;

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt(
            $ch, CURLOPT_HTTPHEADER, [
              'Authorization: Bearer sk_test_47df907b6798049ba4fd92802ba053e305914953']
          );
          $request = curl_exec($ch);
          curl_close($ch);

          if ($request) {
            $result = json_decode($request, true);
            return $result;

          }else {

           return false;
          }
      }*/
}