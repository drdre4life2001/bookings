<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Curl;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Session;

class TripsController extends Controller{

	public function Homepage(Request $request){

        if(Session::has('round_trip_stage')){

            Session::remove("round_trip_stage");
            Session::remove("second_trip_to");
            Session::remove("second_trip_from");
            Session::remove("second_trip_departing");

            Session::remove('round_trip_passengers');
            Session::remove('round_trip_infants');
            Session::remove('parent_booking_code');
        }

        $parks = Curl::to(config('api.online.get-parks'))->post();
        $parks = (json_decode($parks)->data);

        $operators = Curl::to(config('api.online.get-operators'))->post();
        $operators = (json_decode($operators)->data);

        $operators_full = Curl::to(config('api.online.get-operators'))->withData(array('type'=>'all' ))->post();
        $operators_full = (json_decode($operators_full)->data);


   		if($request->isMethod('post')) {

    	}
        if(Auth::user()){
            $getBookings = Curl::to(config('api.online.get-customer-bookings'))
                           ->withData(['customer_id' => Auth::user()->id, 'limit' => 5])
                           ->post();

            $bookings = json_decode($getBookings)->data;
        }
		return view('trips.homepage', compact('parks', 'operators', 'operators_full', 'bookings'));
	}

    public function loadDests(Request  $request){

        $dests = Curl::to(config('api.online.get-dests'))->withData(array('source_park_id'=>$request->source_park_id ))->post();
        $dests = json_decode($dests)->data;

        return $dests;


    }


    public function Loading(Request $request){

        // dd($request->all());

        if(Session::has("parent_booking_code")){

            $trip = Curl::to(config('api.online.trip-from-booking-id'))->withData(['booking_code' =>Session::get('parent_booking_code')])->post();
            $parent_booking = (json_decode($trip)->data);
        }


        if(!empty($request->oneTo2)){

            Session::set("round_trip_stage", 1);
            Session::set("second_trip_to", $request->oneFrom2);
            Session::set("second_trip_from", $request->oneTo2);
            Session::set("second_trip_date", $request->end);


            if(empty($request->passengers2))
             Session::set('round_trip_passengers', $request->options2);
            else
                Session::set('round_trip_passengers', $request->passengers2);

            if(empty($request->infants2))
                Session::set('round_trip_infants', $request->infant_options2);
            else
                Session::set('round_trip_infants', $request->infant2);
        }



        if (!empty($request->oneTo))
            $to = $request->oneTo;
        else
            $to = $request->oneTo2;

        // if(Session::has('parent_booking_code') && Session::get('round_trip_stage') == 2){
        if(Session::get('round_trip_stage') == 2){

            $to = Session::get('second_trip_to');
            $inputs = [
                        'oneTo2' => Session::get('second_trip_to'),
                        'oneFrom2' => Session::get('second_trip_from'),
                        'passengers2' => Session::get('round_trip_passengers'),
                        'infants2' => Session::get('round_trip_infants'),
                        'oneTo' => '',
                        'oneFrom' => '',
                        'options' => '',
                        'infants' => '',
                        'passengers' => '',
                        'departing' => Session::get("second_trip_date")
                        ];
                        
        }

        $park = Curl::to(config('api.online.get-park'))->withData(array('park_id'=>$to ))->post();
        $park = (json_decode($park)->data);

        if ($request->isMethod('post')) {
            $inputs = $request->all();

        }



        return view('trips.loading', compact('inputs', 'park'));

    }

    public function Search(Request $request, $page = 1){

        // dd($request->all());

        $current_page = ($page != null)? $page: 1;
        $per_page = env("PER_PAGE");
        $url = $request->url();

        $sort_types = [
                'departure_time:asc'=>'Departure Time (earliest first)',
                'departure_time:desc'=>'Departure Time (earliest last)',
                'fare:asc'=>'Price (cheapest first)',
                'fare:desc'=>'Price (cheapest last)',
            ];
        $sort_type = 'Price (cheapest first)';
        $sort_by = 'fare:asc';
        $price_min = 1000;
        $price_max =  20000;
        $bus_type =  0;
        $operator = 0;
        $ac=$tv=$security=$passport=$insurance = 0;

        $parks = Curl::to(config('api.online.get-parks'))->post();
        $parks = (json_decode($parks)->data);

         // if ($request->isMethod('post') || $request->isMethod('get')) {


            /**
             * For One Trip
             */
            if (!empty($request->oneTo)) {
                $fromPark = $request->oneFrom;
                $toPark = $request->oneTo;
                $date = date('Y-m-d', strtotime($request->departing));

                if(empty($request->passengers))
                    $passengers = $request->options;
                else
                    $passengers = $request->passengers;
                $end = '';


                if(empty($request->children))
                    $children = ($request->children_options)? $request->children_options: 0 ;
                else
                    $children = $request->children;

                if(empty($request->infants))
                    $infants = ($request->infant_options)? $request->infant_options: 0 ;
                else
                    $infants = $request->infants;

            }else{
                $fromPark = $request->oneFrom2;
                $toPark = $request->oneTo2;
                $date = date('Y-m-d', strtotime($request->departing));
                $end = date(strtotime($request->end));

                if(empty($request->passengers2))
                    $passengers = $request->options2;
                else
                    $passengers = $request->passengers2;

                if(empty($request->infants2))
                    $infants = ($request->infant_options2)? $request->infant_options2: 0;
                else
                    $infants = $request->infant2;
            }

            //sort by
            if(!empty($request->sort_by)){
                $sort_by = $request->sort_by;
                $sort_type = $sort_types[$sort_by];
            }

            if(!empty($request->filter_price)){
                $filter_price = $request->filter_price;
                $fpa = explode(":", $filter_price);
                $price_min = $fpa[0];
                $price_max = $fpa[1];
            }

            if(!empty($request->bus_type)){
                $bus_type = $request->bus_type;
            }

            if(!empty($request->operator)){
                $operator = $request->operator;
            }

            if(!empty($request->ac))
                $ac = $request->ac;

            if(!empty($request->tv))
                $tv = $request->tv;

            if(!empty($request->passport))
                $passport = $request->passport;

            if(!empty($request->security))
                $security = $request->security;

            if(!empty($request->insurance))
                $insurance = $request->insurance;


            // dd(compact('fromPark', 'toPark', 'date', 'sort_by', 'filter_price','bus_type', 'operator', 'ac','tv', 'passport', 'security', 'insurance'));

             $resp = Curl::to(config('api.online.search'))->withData(compact('fromPark', 'toPark', 'date', 'sort_by', 'filter_price','bus_type', 'operator', 'ac','tv', 'passport', 'security', 'insurance', 'page', 'per_page'))->post();

             // dd(json_decode($resp));

             $trips = json_decode($resp)->data->trips;

             $operators = json_decode($resp)->data->operators;
             $bus_types = json_decode($resp)->data->busTypes;
             $trip_count = json_decode($resp)->data->trip_count;
             $fromParkObj = json_decode($resp)->data->fromPark;
             $toParkObj = json_decode($resp)->data->toPark;

             if(isset(json_decode($resp)->data->altTrips))
                $altTrips = json_decode($resp)->data->altTrips;



             // dump($altTrips);

         // }
         $search_paginate = generatePaginator($url, $current_page, $per_page, $trip_count, $_GET);

         $round_trip_stage = Session::get('round_trip_stage');

        $step = 1;
        return view('trips.search', compact('trips', 'fromPark', 'toPark', 'parks', 'passengers', 'date', 'end', 'sort_type', 'sort_types', 'operators', 'bus_types', 'price_min', 'price_max', 'bus_type', 'operator', 'ac', 'tv','security','passport', 'insurance', 'step', 'infants', 'children', 'search_paginate', 'trip_count', 'round_trip_stage', 'fromParkObj', 'toParkObj', 'altTrips' ));

    }

}