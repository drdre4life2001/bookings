<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Curl;
use Session;

class BookingActionsController extends Controller
{
    public function rebook(Request $request)
    {
    	$go_ahead = isset($request->go_ahead)? $request->go_ahead: false;
    	$inputs = $request->all();


        echo Session::get('date');
    	$booking_details = Curl::to(config("api.online.trip-from-booking-id"))
    							->withData(['booking_code' => $request->booking_code])
    							->post();

    	$booking = (json_decode($booking_details)->data);
    	$old_booking = $booking;
    	$old_booking_fee = $booking->final_cost;
    	$booking->final_cost = $booking->trip->fare * $booking->passenger_count;

    	$booking->status = "PENDING";


    	$infants = 0;
    	$infant_arr = [];

    	if(count($booking->passengers) > 0){


    		foreach ($booking->passengers as $psg ) {
    			if($psg->age < 10){
    				$discount = get_infant_discount(collect($psg)->toArray(), unserialize($booking->trip->operator->rule));

    				if(isset($discount['discount_percentage'])){
						$infant_price = $booking->trip->fare - ($booking->trip->fare * ( $discount['discount_percentage'] / 100));
						$booking->final_cost += $infant_price;
					}

    				$infant_arr[] = $psg;
    				$infants +=1;
    			}
    		}
    	}
    	$old_booking_fee = 10000;

    	if($go_ahead == false ){

    		return view('actions.confirm', compact('old_booking_fee', 'inputs', 'old_booking', 'booking'));
    	}
        else{

        	$save = Curl::to(config('api.online.save-booking'))
    					->withData(array(
    									'trip_id'=>$booking->trip_id,
    									'date'=> date("Y-m-d", strtotime($request->departing)),
    									'passenger_count'=> $booking->passenger_count,
    									'unit_cost'=>$booking->trip->fare,
    									'final_cost'=>($booking->final_cost),
    								    'contact_name'=>$booking->contact_name,
    								    'contact_email'=>$booking->contact_email,
    								    'contact_phone'=>$booking->contact_phone,
    								    'next_of_kin_name'=>$booking->next_of_kin,
    								    'next_of_kin_phone'=>$booking->next_of_kin_phone,
    								    'customer_id' => isset($booking->customer_id)? $booking->customer_id: ""
    								     ))->post();

    		$new_booking = (json_decode($save)->data);

    		if(count($booking->passengers) > 0){
    			foreach ($booking->passengers as $psg) {
    				# code...

    				$save_psg = Curl::to(config('api.online.save-psg'))
    					            ->withData([
    					            			'booking_id'=>$new_booking->id,
    					            			'name'=>$psg->name,
    					            			'gender' => $psg->gender,
    					            			'age' => isset($psg->age)? $psg->age: 0
    					            		])->post();
    			}
    		}
            if(Session::has('date'))
                Session::remove('date');

    		return redirect()->route('payment', [$new_booking->booking_code]);
        }
    }

    public function pay(Request $request)
    {
    	# code...
    }

}