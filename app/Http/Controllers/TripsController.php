<?php

namespace App\Http\Controllers;

use App\NyscPark;
use App\NyscTrip;
use App\Operator;
use App\Park;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Curl;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Session;

class TripsController extends Controller
{
//    public function __construct() {
//        $this->middleware('check_api');
//    }

    public function Homepage(Request $request)
    {
        if (Session::has('round_trip_stage')) {
            Session::remove("round_trip_stage");
            Session::remove("second_trip_to");
            Session::remove("second_trip_from");
            Session::remove("second_trip_departing");
            Session::remove('round_trip_passengers');
            Session::remove('round_trip_infants');
            Session::remove('parent_booking_code');
        }
        $parks = \Ixudra\Curl\Facades\Curl::to(config('api.online.get-parks'))->post();
        $parks = !is_null(json_decode($parks)->data) ? json_decode($parks)->data : [];
        $operators = Curl::to(config('api.online.get-operators'))->post();
        $operators = !is_null(json_decode($operators)->data) ? json_decode($operators)->data : [];
        $_feedbacks_ = Curl::to(config('api.online.get-feedbacks'))->post();
        $_feedbacks = !is_null($_feedbacks_) ? json_decode($_feedbacks_) : [];
        $feedbacks = !empty($_feedbacks) ? $_feedbacks->data : [];
        $operators_full = Curl::to(config('api.online.get-operators'))->withData(array('type' => 'all'))->post();
        $operators_full = !is_null(json_decode($operators_full)->data) ? json_decode($operators_full)->data : [];
        if (Auth::user()) {
            $getBookings = Curl::to(config('api.online.get-customer-bookings'))
                ->withData(['customer_id' => Auth::user()->id, 'limit' => 5])
                ->post();

            $bookings = json_decode($getBookings)->data;
            $msg = 0;
        }
        if (empty($parks)) {
            $errors = 'No Parks Aviailable';
        }

        $msg = '';
        return view('trips.homepage', compact('parks', 'newdests', 'errors', 'newparks', 'operators', 'operators_full', 'bookings', 'msg', 'feedbacks'));

    }

    public function nysc(Request $request)
    {
        $camps = NyscPark::all();
        $parks = Park::where('nysc', 1)->get();
        $operators = Operator::all();
        return view('trips.nysc.homepage', compact('parks', 'operators', 'lagos_parks', 'camps'));
    }

    public function nysc_search(Request $request)
    {
        $source = $request->oneFrom;
        $destination = $request->oneTo;
        $step = 1;
        $passengers = 1;
        $sort_types = [
            'departure_time:asc' => 'Departure Time (earliest first)',
            'departure_time:desc' => 'Departure Time (earliest last)',
            'fare:asc' => 'Price (cheapest first)',
            'fare:desc' => 'Price (cheapest last)',
        ];
        $trips = DB::select("SELECT id, fare, (SELECT name FROM parks WHERE id = source_park_id) as park,
                    (SELECT name FROM nysc_camps WHERE id = dest_camp_id) as camp,
                    (SELECT name FROM operators WHERE id = operator_id) as operator,
		    (SELECT address FROM nysc_camps WHERE id = dest_camp_id) as address, 
                    (SELECT img FROM operators WHERE id = operator_id) as operator_logo
                    FROM nysc_trips WHERE dest_camp_id = '$destination' AND source_park_id = '$source'");
        if (count($trips) == 0) {
            return back()->with('error', 'No Trips Available for the selected route');
        }
        $parks = Park::all();
        $fromPark = Park::where('id', $source)->first();
        $toPark = NyscPark::where('id', $destination)->first();
        $fromPark = $fromPark->name;
        $toPark = $toPark->name;
        $trip_count = count($trips);
        return view('trips.nysc.search', compact('result', 'step', 'trips', 'sort_type', 'sort_types', 'trip_count', 'parks', 'fromPark', 'toPark', 'passengers'));
    }


    public function loadDests(Request $request)
    {
        $dests = Curl::to(config('api.online.get-dests'))->withData(array('source_park_id' => $request->source_park_id))->post();
        $dests = json_decode($dests)->data;
        return $dests;
    }

    public function loadIntDests(Request $request)
    {
        $dests = Curl::to(config('api.online.get-int-dests'))->withData(array
        ('source_park_id' => $request->source_park_id))->post();
        $dests = json_decode($dests)->data;
        return $dests;
    }

    public function Loading(Request $request)
    {
        $parks = \Ixudra\Curl\Facades\Curl::to(config('api.online.get-parks'))->post();
        $parks = !is_null(json_decode($parks)->data) ? json_decode($parks)->data : [];
        $operators = Curl::to(config('api.online.get-operators'))->post();
        $operators = !is_null(json_decode($operators)->data) ? json_decode($operators)->data : [];
        $_feedbacks_ = Curl::to(config('api.online.get-feedbacks'))->post();
        $_feedbacks = !is_null($_feedbacks_) ? json_decode($_feedbacks_) : [];
        $feedbacks = !empty($_feedbacks) ? $_feedbacks->data : [];
        $operators_full = Curl::to(config('api.online.get-operators'))->withData(array('type' => 'all'))->post();
        $operators_full = !is_null(json_decode($operators_full)->data) ? json_decode($operators_full)->data : [];
        $date = $request->date;
        $date = date('Y-m-d',strtotime($date));
        $date = strtotime($date);
        $today = date('Y-m-d');
        $today = strtotime($today);
        $oneweek = strtotime("+7 day", $today);
        $tomorrow = strtotime("+1 day", $today);
        $tomorrow_date = date("Y-m-d", $tomorrow);
        if ($date == $tomorrow){
         if(date('H')>18){
         $msg = 'Bookings is closed for tomorrow';
            return view('trips.homepage', compact('parks', 'newdests', 'errors', 'newparks', 'operators', 'operators_full', 'bookings', 'msg', 'feedbacks'));
         }
        }
        if ($date<=$today){

            $msg = 'Date is not Valid';
            return view('trips.homepage', compact('parks', 'newdests', 'errors', 'newparks', 'operators', 'operators_full', 'bookings', 'msg', 'feedbacks'));

        }

        if ($date >$oneweek ){

            $msg = 'Date should not be more than one week ahead';
            return view('trips.homepage', compact('parks', 'newdests', 'errors', 'newparks', 'operators', 'operators_full', 'bookings', 'msg', 'feedbacks'));

        }
        $input = null;
        if (Session::has("parent_booking_code")) {
            $trip = Curl::to(config('api.online.trip-from-booking-id'))->withData(['booking_code' => Session::get('parent_booking_code')])->post();
            $parent_booking = (json_decode($trip)->data);
        }
        if (!empty($request->oneTo2)) {
            Session::set("round_trip_stage", 1);
            Session::set("second_trip_to", $request->oneFrom2);
            Session::set("second_trip_from", $request->oneTo2);
            Session::set("second_trip_date", $request->end);
            if (empty($request->passengers2))
                Session::set('round_trip_passengers', $request->options2);
            else
                Session::set('round_trip_passengers', $request->passengers2);

            if (empty($request->infants2))
                Session::set('round_trip_infants', $request->infant_options2);
            else
                Session::set('round_trip_infants', $request->infant2);
        }
        if (!empty($request->oneTo))
            $to = $request->oneTo;
        else
            $to = $request->oneTo2;
        if (Session::get('round_trip_stage') == 2) {
            $to = Session::get('second_trip_to');
            $inputs = [
                'oneTo2' => Session::get('second_trip_to'),
                'oneFrom2' => Session::get('second_trip_from'),
                'passengers2' => Session::get('round_trip_passengers'),
                'infants2' => Session::get('round_trip_infants'),
                'oneTo' => '',
                'oneFrom' => '',
                'options' => '',
                'infants' => '',
                'passengers' => '',
                'departing' => Session::get("second_trip_date")
            ];

        }
        $booktype = $request->booktype;

        if ($request->isMethod('post')) {
            $inputs = $request->all();

        }
        $park = Curl::to(config('api.online.get-park'))->withData(array('park_id' => $to))->post();
        $park = (json_decode($park)->data);
        return view('trips.loading', compact('inputs', 'park', 'book'));
    }

    public function Search(Request $request, $page = 1)
    {
        $current_page = ($page != null) ? $page : 1;
        $per_page = env("PER_PAGE");
        $bus_fee = env("BUS_FEE");
        $url = $request->url();
        $sort_types = [
            'departure_time:asc' => 'Departure Time (earliest first)',
            'departure_time:desc' => 'Departure Time (earliest last)',
            'fare:asc' => 'Price (cheapest first)',
            'fare:desc' => 'Price (cheapest last)',
        ];
        $sort_type = 'Price (cheapest first)';
        $sort_by = 'fare:asc';
        $price_min = 1000;
        $price_max = 20000;
        $bus_type = 0;
        $operator = 0;
        $ac = $tv = $security = $passport = $insurance = 0;
        $parks = Curl::to(config('api.online.get-parks'))->post();
        $parks = (json_decode($parks)->data);

        /**
         * For One Trip
         */

        if (!empty($request->oneTo)) {
            $fromPark = $request->oneFrom;
            $toPark = $request->oneTo;
            $date = date('Y-m-d', strtotime($request->start));
            if (empty($request->passengers))
                $passengers = $request->options;
            else
                $passengers = $request->passengers;
            $end = '';
            if (empty($request->children))
                $children = ($request->children_options) ? $request->children_options : 0;
            else
                $children = $request->children;
            if (empty($request->infants))
                $infants = ($request->infant_options) ? $request->infant_options : 0;
            else
                $infants = $request->infants;

        } else {
            $fromPark = $request->oneFrom2;
            $toPark = $request->oneTo2;
            $date = date('Y-m-d', strtotime($request->departing));
            $end = date(strtotime($request->end));

            if (empty($request->passengers2))
                $passengers = $request->options2;
            else
                $passengers = $request->passengers2;

            if (empty($request->infants2))
                $infants = ($request->infant_options2) ? $request->infant_options2 : 0;
            else
                $infants = $request->infant2;
        }
        //sort by
        if (!empty($request->sort_by)) {
            $sort_by = $request->sort_by;
            $sort_type = $sort_types[$sort_by];
        }
        if (!empty($request->filter_price)) {
            $filter_price = $request->filter_price;
            $fpa = explode(":", $filter_price);
            $price_min = $fpa[0];
            $price_max = $fpa[1];
        }
        if (!empty($request->bus_type)) {
            $bus_type = $request->bus_type;
        }
        if (!empty($request->operator)) {
            $operator = $request->operator;
        }
        if (!empty($request->ac))
            $ac = $request->ac;

        if (!empty($request->tv))
            $tv = $request->tv;

        if (!empty($request->passport))
            $passport = $request->passport;

        if (!empty($request->security))
            $security = $request->security;

        if (!empty($request->insurance))
            $insurance = $request->insurance;
        // dd(compact('fromPark', 'toPark', 'date', 'sort_by', 'filter_price','bus_type', 'operator', 'ac','tv', 'passport', 'security', 'insurance'));
        $resp = Curl::to(config('api.online.search'))->withData(compact('fromPark', 'toPark', 'date', 'sort_by', 'filter_price', 'bus_type', 'operator', 'ac', 'tv', 'passport', 'security', 'insurance', 'page', 'per_page'))->post();
        //dd($resp);

        if (json_decode($resp)->status == true) {
            $trips = json_decode($resp)->data->trips;
            $operators = json_decode($resp)->data->operators;
            $bus_types = json_decode($resp)->data->busTypes;
            $trip_count = json_decode($resp)->data->trip_count;
            $fromParkObj = json_decode($resp)->data->fromPark;
            $toParkObj = json_decode($resp)->data->toPark;
            if (isset(json_decode($resp)->data->altTrips))
                $altTrips = json_decode($resp)->data->altTrips;
            $search_paginate = generatePaginator($url, $current_page, $per_page, $trip_count, $_GET);
            $round_trip_stage = Session::get('round_trip_stage');
            $step = 1;
            //dd($trips[0]->trip->id);
            return view('trips.search', compact('trips', 'fromPark', 'toPark', 'parks', 'bus_fee', 'passengers',
                'date',
                'end',
                'sort_type', 'sort_types', 'operators', 'bus_types', 'price_min', 'price_max', 'bus_type', 'operator', 'ac', 'tv', 'security', 'passport', 'insurance', 'step', 'infants', 'children', 'search_paginate', 'trip_count', 'round_trip_stage', 'fromParkObj', 'toParkObj', 'altTrips'));
        } else {
            return back()->with('status', 'Sorry! There are no trips for the requested routes!');
        }
    }

    public function GetNYSCParks()
    {
        $parks = Park::where('nysc', 1)->get();
        $json_data = [
            'status' => true,
            'data' => $parks,
            'message' => 'NYSC Parks'
        ];
        return json_encode($json_data);
    }

}
