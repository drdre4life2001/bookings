<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Curl;
use Illuminate\Support\Facades\Redirect;
use Faker\Factory as Faker;
use Auth;
use Mail;
use Session;



class BookingsController extends Controller{

    public function Add(Request $request, $trip_id, $trip_fare, $passport ,$psg, $children, $infants, $date,
                        $end = null){
        //dd($request);
        if(Session::has('round_trip_stage') && Session::get('round_trip_stage') == 1){
            Session::set('round_trip_stage', 2);
            Session::set('first_trip_id', $trip_id);
            Session::set('first_trip_date', $date);
            return redirect()->route('loading');
        }
        $p_type = $passport !== 0 ? $passport:"";
        $user = Auth::user();
        $return_date = (date('Y-m-d', $end));
        if(Session::has('round_trip_stage') && Session::get('round_trip_stage') == 2){
            $trip = Curl::to(config('api.online.get-trip'))
                ->withData(['trip_id'=>Session::get('first_trip_id'), 'departure_date' => Session::get('first_trip_date')])
                ->post();
            $trip = (json_decode($trip)->data);
            $fare = $trip_fare !== 0 ? $trip_fare:$trip->fare;
            $return_trip = Curl::to(config('api.online.get-trip'))
                ->withData(['trip_id'=>$trip_id, 'departure_date' => $date])
                ->post();
            $return_trip = (json_decode($return_trip)->data);
            // dump($return_trip);
            $return_date = $date;
            $passport_option = $request->passport_type;
            $date =  Session::get('first_trip_date');
            $final_cost = $psg * $fare + ( (1 - ($trip->operator->local_children_discount/ 100)) * $trip->fare *
                    $children) ;
            $final_cost += $psg * $return_trip->fare + ( (1 - ($trip->operator->local_children_discount/ 100)) * $trip->fare * $children) ;
        }else{
            $trip = Curl::to(config('api.online.get-trip'))
                ->withData(['trip_id'=>$trip_id, 'departure_date' => $date])
                ->post();
            $trip = (json_decode($trip)->data);
            $fare = $trip_fare !== 0 ? $trip_fare:$trip->fare;
            $final_cost = $psg * $fare + ( (1 - ($trip->operator->local_children_discount/ 100)) *
                    $fare * $children);
        }
        //convenience fee
        $final_cost += config('custom.convenience_fee');
        // dd($trip);
        $occupied_seats = [];
        collect($trip->seats)->each(function($item, $key) use (&$occupied_seats){
            if( $item->seat_no != ""){
                $occupied_seats[] =  $item->seat_no;
            }
        });
        // dd($parent_booking);
        if ($request->isMethod('post')) {
            //dd($request);
            $basefare = $trip->international !== 0 ? $trip_fare:$trip->fare;
            //dd($trip_fare);
            if(Session::has('round_trip_stage') && Session::get('round_trip_stage') == 2)
                $basefare += $return_trip->fare;
            $infants = ($request->infant_count != "" || $request->infant_count != null )? $request->infant_count: $infants;
            $children = ($request->children_count != "" || $request->children_count != null )? $request->children_count: $children;
            // $psg = ($request->passenger_count != "" || $request->passenger_count != null)? intval($request->passenger_count+1): $psg;
            $infant_seats = 0;
            if($infants > 0){
                $infant = [];
                $counter = (int) $infants;
                for ($j=1; $j <= $counter; $j++) {
                    $infantsx = 'infants'.$j;
                    $i_age = 'i_age'.$j;
                    $i_gender = 'i_gender'.$j;
                    $infant[$j]['name'] = $request->$infantsx;
                    $infant[$j]['age'] = $request->$i_age;
                    $infant[$j]['gender'] = $request->$i_gender;
                    $infant_arr = get_infant_discount($infant[$j], unserialize($trip->operator->rule));
                    $infant_arr['discount_percentage'] = isset($infant_arr['discount_percentage'])?$infant_arr['discount_percentage']: 100;
                    $infant[$j]['price'] = $basefare - ($basefare * ( $infant_arr['discount_percentage']/100));
                    $infant[$j]['discount_percentage'] = $infant_arr['discount_percentage'];
                    $final_cost += $infant[$j]['price'];
                    if(isset($infant_arr['discount_percentage']) && $infant_arr['discount_percentage'] != 100)
                        $infant_seats += 1;
                }
            }
            if(array_get($request, 'password')){
                $this->validate($request, [
                    'contact_name' => 'required',
                    'contact_phone' => 'required',
                    'password' => 'required|confirmed',
                    'password_confirmation' => 'required'
                ]);

                $save_customer = Curl::to(config('api.online.save-customer'))
                    ->withData([
                        'name' => $request->input('contact_name'),
                        'email' => $request->input('contact_email'),
                        'phone' => $request->input('contact_phone'),
                        'password' => $request->input('password'),
                    ])
                    ->post();

                $customer = (json_decode($save_customer)->data);
            }

            if(!isset($customer)){
                $get_customer = Curl::to(config('api.online.get-customer-data'))
                    ->withData(['phone' => $request->contact_phone])
                    ->post();

                $customer = (json_decode($get_customer)->data);
            }
            $seats = explode(",", $request->selected_seats);
            $seat_index = 0;
            // dump($seats);
            if($request->international == 1){
                switch($request->passport_type){
                    case($request->passport_type == 'virgin_passport');
                        $passport_cost = $trip->virgin_passport;
                        $final_cost += $passport_cost  ;
                        $p_cost = $passport_cost;
						$is_intl = 1;
                        break;
                    case($request->passport_type == 'regular_passport');
                        $passport_cost = $trip->regular_passport;
                        $final_cost += $passport_cost  ;
                        $p_cost = $passport_cost;
						$is_intl = 1;
                        break;
                    case($request->passport_type == 'no_passport');
                        $passport_cost = $trip->no_passport;
                        $final_cost += $passport_cost  ;
                        $p_cost = $passport_cost;
						$is_intl = 1;
                        break;
                }
                $passport_expiry_date = $request->passport_expiry_date;
                $passport_issue_date = $request->passport_date_of_issue;
                $passport_issue_place = $request->passport_place_of_issue;
                $passport_no = $request->passport_number;
                $passport_type = $request->passport_type;
                $pss_cost = $fare;
            }else{
                $passport_expiry_date = null;
                $passport_issue_date = null;
                $passport_issue_place = null;
                $passport_no = null;
                $passport_type = null;
                $final_cost += 0  ;
                $pss_cost = 0;
				$is_intl = null;
            }
			//dd($is_intl);
            $save = Curl::to(config('api.online.save-booking'))
                ->withData(array(
                    'trip_id'=>$trip->id,
                    'date'=> $request->departure_date,
                    'passenger_count'=> intval($psg + $children),
                    'unit_cost'=>isset($trip->fare)?$trip->fare : $fare,
                    'final_cost'=>$final_cost,
                    'contact_name'=>$request->contact_name,
                    'contact_email'=>$request->contact_email,
                    'contact_phone'=>$request->contact_phone,
                    'next_of_kin_name'=>$request->kin_name,
                    'next_of_kin_phone'=>$request->kin_phone,
                    'customer_id' => isset($customer->id)? $customer->id: "",
                    'return_trip_id' => isset($return_trip)? $return_trip->id: null,
                    'return_date' => isset($return_trip)? $return_date: null,
                    'gender' => $request->gender,
					'is_intl_trip' => $is_intl,
                    'seat' => isset($seats[$seat_index])?$seats[$seat_index]: $seats[0],
                    'passport_type' => $passport_type,
                    'passport_number' => $passport_no,
                    'passport_date_of_issue' => $passport_issue_date,
                    'passport_place_of_issue' => $passport_issue_place,
                    'passport_expiry_date' => $passport_expiry_date,
                    'departure_date' => $request->departure_date,
                    'passport_cost' => $pss_cost
                ))
                ->post();
            //						dd($save); exit;
            $booking = (json_decode($save)->data);
            // dd($booking);
            $booking_code= $booking->booking_code;
            if ($psg > 1) {  //other passengers...

                for ($i=1; $i < $psg; $i++) {
                    $pp = 'passengers'.$i;
                    $p_gender = 'p_gender'.$i;
                    $seat_index++;
					$ptype = 'passport_type'.$i;
					$pno = 'passport_number'.$i;
					$pexp = 'passport_expiry_date'.$i;
					$ppoi = 'passport_place_of_issue'.$i;
					$pdoi = 'passport_date_of_issue'.$i;
					//dd($seats);
                    $save_psg = Curl::to(config('api.online.save-psg'))
                        ->withData(array(
                            'booking_id'=>$booking->id,
                            'name'=>$request->$pp,
                            'gender' => $request->$p_gender,
                            'seat'=>isset($seats[$seat_index])?$seats[$seat_index]: $seats[0],
                            'age_group' => 'Adult',
							'passport_no' => isset($request->$pno)?$request->$pno:"",
							'passport_type' => isset($request->$ptype)?$request->$ptype:"",
							'passport_expiry_date' => isset($request->$pexp)?$request->$pexp:"", 
							'passport_place_of_issue' => isset($request->$ppoi)?$request->$ppoi:"",
							'passport_date_of_issue' => isset($request->$pdoi)?$request->$pdoi:""
                        ))->post();
                }
            }

            if ($children > 0 ) { //children
                for ($j=1; $j <= $children; $j++) {
                    $infantx = 'child'. $j;
                    $i_age = 'child_age'.$j;
                    $i_gender = 'child_gender'.$j;
                    // $seat_index++;
                    //dd($seat_index); exit;

                    // $save_psg = Curl::to(config('api.online.save-psg'))
                    // ->withData(array('booking_id'=>$booking->id, 'name'=>$request->$infantx, 'gender'=> $request->$i_gender, 'age' => $request->$i_age))->post();
                    $save_psg = Curl::to(config('api.online.save-psg'))
                        ->withData(array(
                            'booking_id'=>$booking->id,
                            'name'=>$request->$infantx,
                            'gender' => $request->$i_gender,
                            'age' => $request->$i_age,
                            'seat'=>isset($seats[$seat_index])?$seats[$seat_index]: $seats[0],
                            'age_group' => 'Child',
                            'trip_id'=>$trip->id,
                        ))->post();
                }
            }

            if ($infants > 0 ) {

                // dump('looping infants');

                for($j= (1 + $children); $j <= ($infants + $children); $j++) {

                    $infantx = 'infants'. $j;
                    $i_age = 'i_age'.$j;
                    $i_gender = 'i_gender'.$j;

                    // dump($infantx.' '.$i_age.' '.$i_gender);

                    //     			$save_psg = Curl::to(config('api.online.save-psg'))
                    // ->withData(array('booking_id'=>$booking->id, 'name'=>$request->$infantx, 'gender'=> $request->$i_gender, 'age' => $request->$i_age))->post();

                    $save_psg = Curl::to(config('api.online.save-psg'))
                        ->withData(array(
                            'booking_id'=>$booking->id,
                            'name'=>$request->$infantx,
                            'gender' => $request->$i_gender,
                            'age' => $request->$i_age,
                            'age_group' => 'Infant',
                            'trip_id'=>$trip->id,

                        ))->post();

                    // dump($save_psg);    
                }
            }

            //save booked seats

            if($request->selected_seats != ''){
                $seats = explode(",", $request->selected_seats);
                for ($i=0; $i < count($seats); $i++) {
                    $save_seat = Curl::to(config('api.online.save-booked-seat'))
                        ->withData([
                            'trip_id' => $booking->trip_id,
                            'seat_no' => $seats[$i],
                            'booking_id' => $booking->id,
                            'departure_date' => $booking->date
                        ])->post();
                }

            }

            // if(Session::has('round_trip_stage') && Session::get('round_trip_stage') == 1){
            //     Session::set('round_trip_stage', 2);
            //     Session::set('parent_booking_code', $booking_code);

            //     return redirect()->route('loading');

            // }
            // else
            return redirect()->route('payment', [$booking_code, $fare]);
        }
        $round_trip = ($end != null)? 1: 0;
        $step = 2;
        $getCustomerDataUrl =  config('api.online.get-customer-data');
        $intl = $trip->international;
        return view('bookings.add', compact('trip', 'date', 'psg', 'intl','return_date', 'step', 'children',
            'infants','p_type','fare',
            'getCustomerDataUrl', 'final_cost', 'user', 'occupied_seats', 'round_trip', 'return_trip', 'return_date'));
    }

    public function Payment(Request $request, $booking_code, $fare = null){


        if(Session::has('round_trip_stage')){
            Session::remove("round_trip_stage");
            Session::remove("second_trip_to");
            Session::remove("second_trip_from");
            Session::remove("second_trip_departing");
            Session::remove('first_search');
            Session::remove('second_search');
            Session::remove('round_trip_passengers');
            Session::remove('round_trip_infants');
            Session::remove('first_trip_id');
            Session::remove('first_trip_date');

        }

        $trip = Curl::to(config('api.online.trip-from-booking-id'))->withData(array('booking_code' =>$booking_code))->post();
		
        $booking = (json_decode($trip)->data);
		
       // dd($booking->is_intl_trip); 
		$intl = $booking->is_intl_trip;
        if(isset($booking->parent_booking_id)){
            $parent_trip = Curl::to(config('api.online.get-customer-booking-by-id'))->withData(['booking_id' => $booking->parent_booking_id])->post();
            $parent_booking = (json_decode($parent_trip)->data);
            // dd($parent_booking);
        }

        $infants = $children = 0;
        collect($booking->passengers)->each(function ($item, $key) use (&$infants) {
            if ($item->age_group == 'Infant') {
                $infants +=1;
            }
        });

        collect($booking->passengers)->each(function ($item, $key) use (&$children) {
            if ($item->age_group == 'Child') {
                $children +=1;
            }
        });


        $data = Curl::to(config('api.online.banks'))->get();
        $data = (json_decode($data)->data);
        $banks = $data->banks;
        $cash_offices = $data->cash_offices;
        $trip = $booking->trip;
        $psg = $booking->passenger_count;
        $date = $booking->date;
        $today = date('Y-m-d');
        $date_timestamp = strtotime($date);
        $today_timestamp = strtotime($today);
        if($today_timestamp > $date_timestamp){
            return redirect()->intended('/')->with('error', 'Sorry! You cant pay for this ticket anymore!');
        }
        $final_cost = $booking->final_cost;
        if(Session::remove("parent_booking_code"))
            Session::remove("parent_booking_code");
        $step = 3;
        return view('bookings.payment', compact('trip', 'psg', 'booking', 'intl','booking_code', 'banks', 'fare','cash_offices','step', 'infants', 'parent_booking', 'children', 'date', 'final_cost'));
    }

    public function SuccessPayment(Request $request, $booking_code, $on_hold = FALSE){

        $trip = Curl::to(config('api.online.trip-from-booking-id'))
            ->withData(array('booking_code' =>$booking_code))
            ->post();
        $booking = (json_decode($trip)->data);
        $data = Curl::to(config('api.online.banks'))->get();
        $data = (json_decode($data)->data);
        $banks = $data->banks;
        $cash_offices = $data->cash_offices;
        $trip = $booking->trip;
        $psg = $booking->passenger_count;
        $p_occupied_seats = [];
        $date = $booking->date;
        $final_cost = $booking->final_cost;
        $booking_code = $booking->booking_code;
        if(isset($booking->parent_booking_id)){
            $parent_trip = Curl::to(config('api.online.get-customer-booking-by-id'))->withData(['booking_id' => $booking->parent_booking_id])->post();
            $parent_booking = (json_decode($parent_trip)->data);
            collect($parent_booking->seats)->each(function($itemx, $keyx) use (&$p_occupied_seats){
                if( $itemx->seat_no != ""){
                    $p_occupied_seats[] =  $itemx->seat_no;
                }
            });
            $p_occupied_seats = implode(",", $p_occupied_seats);
        }

        $occupied_seats = [];
        collect($booking->seats)->each(function($item, $key) use (&$occupied_seats){
            if( $item->seat_no != ""){
                $occupied_seats[] =  $item->seat_no;
            }
        });
        $occupied_seats = implode(",", $occupied_seats);

        $infants = $children = 0;
        collect($booking->passengers)->each(function ($item, $key) use (&$infants) {
            if ($item->age_group == 'Infant') {
                $infants +=1;
            }
        });

        collect($booking->passengers)->each(function ($item, $key) use (&$children) {
            if ($item->age_group == 'Child') {
                $children +=1;
            }
        });
        $data = compact('trip', 'psg', 'occupied_seats', 'booking', 'p_occupied_seats', 'parent_booking', 'banks', 'cash_offices');
        if($on_hold == FALSE){
            if(isset($parent_booking)){
                $total = number_format($booking->final_cost + $parent_booking->final_cost);
                $msg = "Bus.com.ng purchase notice:\nBook Code: ".$parent_booking->booking_code." \nfrom: ".$parent_booking->trip->name."\nDetails are: ".$parent_booking->contact_name."\nDeparture Date: ".date("D, d/m/Y", strtotime($parent_booking->date))."\nDeparture Time: ".$parent_booking->trip->departure_time."\nTotal Passengers: ". $parent_booking->passenger_count."\nSeats: ".$p_occupied_seats."\n Trip Cost: NGN ". $parent_booking->final_cost."\nReturn Trip\nTrip Book Code: ".$booking->booking_code." \nfrom: ".$booking->trip->name."\nDetails are: ".$booking->contact_name."\nDeparture Date: ".date("D, d/m/Y", strtotime($booking->date))."\nDeparture Time: ".$booking->trip->departure_time."\nTotal Passengers: ". $booking->passenger_count."\nSeats: ".$occupied_seats."\n Trip Cost: ".$booking->final_cost."\nTotal Trip Cost: NGN ". $total;
                Mail::send('emails.send',  $data, function ($message) use ($data){
                    $message->subject("Bus.com.ng purchase notice : Ticket Confirmation");
                    $message->from(env('APP_EMAIL'), env('APP_NAME'));
                    $message->to($data['booking']->contact_email);
                });

            }
            else{
                $msg = "Bus.com.ng purchase notice:\nBook Code: ".$booking->booking_code." \nfrom: ".$booking->trip->name."\nDetails are: ".$booking->contact_name."\nDeparture Date: ".date("D, d/m/Y", strtotime($booking->date))."\nDeparture Time: ".$booking->trip->departure_time."\nTotal Passengers: ". $booking->passenger_count."\nSeats: ".$occupied_seats."\nFinal Cost: NGN ". number_format($booking->final_cost);
                Mail::send('emails..bookings.paid',  $data, function ($message) use ($data){
                    $message->from(env('APP_EMAIL'), env('APP_NAME'));
                    $message->to($data['booking']->contact_email);
                });
            }
        }
        else{
            $total = number_format($booking->final_cost + @$parent_booking->final_cost);
            if(isset($parent_booking)){
                $msg = "Bus.com.ng on hold:\nBook Code: ".$parent_booking->booking_code." \nfrom: "
                    .$parent_booking->trip->name."\nDetails are: "
                    .$parent_booking->contact_name."\n
                    Departure Date: ".date("D, d/m/Y", strtotime($parent_booking->date))."\n
                    Departure Time: ".$parent_booking->trip->departure_time."\n
                    Total Passengers: ". $parent_booking->passenger_count."\n
                    Seats: ".$p_occupied_seats."\n
                    Trip Cost: NGN ". $parent_booking->final_cost."\n
                    Return Trip\n
                    Trip Book Code: ".$booking->booking_code." \n
                    from: ".$booking->trip->name."\n
                    Details are: ".$booking->contact_name."\n
                    Departure Date: ".date("D, d/m/Y", strtotime($booking->date))."\n
                    Departure Time: ".$booking->trip->departure_time."\n
                    Total Passengers: ". $booking->passenger_count."\n
                    Seats: ".$occupied_seats."\n
                    Trip Cost: ".$booking->final_cost."\n
                    Total Trip Cost: NGN ". $total;
                Mail::send('emails.send',  $data, function ($message) use ($data){
                    $message->from(env('APP_EMAIL'), env('APP_NAME'));
                    $message->to($data['booking']->contact_email);
                });
            }
            else{
                $msg = "Bus.com.ng on hold:\nBook Code: ".$booking->booking_code." \nfrom: ".$booking->trip->name."\nDetails are: ".$booking->contact_name."\nDeparture Date: ".date("D, d/m/Y", strtotime($booking->date))."\nDeparture Time: ".$booking->trip->departure_time."\nTotal Passengers: ". $booking->passenger_count."\nSeats: ".$occupied_seats."\nFinal Cost: NGN ". number_format($booking->final_cost);
                Mail::send('emails.bookings.pending',  $data, function ($message) use ($data){
                    $message->subject('Bus.com.ng on hold');
                    $message->from(env('APP_EMAIL'), env('APP_NAME'));
                    $message->to($data['booking']->contact_email);
                });
                Mail::send('emails.bookings.pending',  $data, function ($message) use ($data){
                    $message->subject('Bus.com.ng - New Pending Booking');
                    $message->from(env('APP_EMAIL'), env('APP_NAME'));
                    $message->to('info@bus.com.ng');
                });
            }
            $msg .= "\n\nBank details for your Booking:\n";
            foreach ($banks as $bank) {
                $msg .= "\nBank: ".$bank->name." \nAccnt Name: ".$bank->account_name."\nAccnt Num".$bank->acount_number." \n";
            }
        }
        send_sms($booking->contact_phone, $msg);
        $step = 4;
        return view('bookings.success', compact('trip', 'psg', 'booking', 'on_hold', 'step', 'occupied_seats', 'parent_booking', 'infants', 'children', 'date', 'final_cost', 'booking_code'));
    }

    public function Transactions(Request $request){

        $trans = Curl::to(config('api.online.save-transaction'))
            ->withData(array('booking_id' =>$request->booking_id, 'booking_code' => $request->booking_code, 'status' => $request->status, 'response'=>$request->response, 'payment_method'=>$request->payment_method, 'cancelled' => $request->cancelled))
            ->post();
        $status = (json_decode($trans)->status);
        if($request->parent_booking_id != ""){
            $trans = Curl::to(config('api.online.save-transaction'))
                ->withData(array('booking_id' =>$request->parent_booking_id, 'booking_code' => $request->parent_booking_code, 'status' => $request->status, 'response'=>$request->response, 'payment_method'=>$request->payment_method, 'cancelled' => $request->cancelled))
                ->post();
        }
        echo $status;
    }

    public function CharterSuccessPayment(Request $request){

        $charter = Curl::to(config('api.online.save-charter'))
            ->withData(
                array(
                    'pickup' =>$request->pickup,
                    'dropoff'=>$request->dropoff,
                    'contact_name'=>$request->contact_name,
                    'start'=>$request->start,
                    'operator'=>$request->operator_id,
                    'contact_phone'=>$request->contact_phone,
                    'phone_kin'=>$request->phone_kin,
                    'kin' => $request->kin,
                    'passenger'=>$request->passenger,
                    'budget' => $request->budget,
                    'pick_up_address' => $request->pickup_address,
                    'drop_off_address' => $request->dropoff_address
                )
            )->post();

        $charter = (json_decode($charter)->data);

        if((bool) $charter){

            $message = "New Charter request at bus.com.ng by "
                .$request->contact_name."($request->contact_phone)
        				from $request->pickup to  $request->dropoff
        				";
            $numbers = config('custom.charter_alert_numbers');

            for ($i=0; $i < count($numbers) ; $i++) {
                if(!empty($numbers[$i])){
                    send_sms($numbers[$i], $message);
                }
            }
            //send sms to the client
            $client_msg = "Hi $request->contact_name, ".env("CHARTER_MSG");

            send_sms($request->contact_phone, $client_msg);
        }


        $trip = Curl::to(config('api.online.trip-from-charter-id'))->withData(array('charter_booking_id' =>$charter->id))->post();
        $trip = (json_decode($trip)->data);

        return view('bookings.charter_success', compact('trip'));
    }

    public function Sms(Request $request){

        $trip = Curl::to(config('api.online.trip-from-booking-id'))->withData(array('booking_code' =>$request->booking_code))->post();
        $booking = (json_decode($trip)->data);

        if(array_get($request, 'cashier_name')){

            $msg = "Cash office details for your Booking:\n\nCashier: ".$request->cashier_name." \nPhone: ".$request->cashier_phone."\nAddress ".$request->cashier_address." \nAmount: N".number_format($booking->final_cost)."\nBooking code: ".$booking->booking_code;

            $resp = send_sms($booking->contact_phone, $msg);
            return json_encode(trim($resp));

        }


        $msg = "Bank details for your Booking:\n\nBank: ".$request->bank." \nAccnt Name: ".$request->acct_name."\nAccnt Num".$request->acct_number." \nAmount: N".number_format($booking->final_cost)."\nBooking code: ".$booking->booking_code;

        $resp = send_sms($booking->contact_phone, $msg);
        return json_encode(trim($resp));

    }

    public function getInfantTicketPrice(Request $request)
    {
        $infant_price_arr = [];
        //get operator rule
        $operator_rule = Curl::to(config('api.online.get-operator-booking-rule'))
            ->withData(['operator_id' => $request->operator_id])->post();
        $rule = (json_decode($operator_rule)->data);

        if($rule)
            $rules = unserialize($rule->rules);

        $discount = get_infant_discount($request->infant, $rules);

        if(isset($discount['discount_percentage'])){
            $infant_price_arr['infant_price'] = $request->unit_cost - ($request->unit_cost * ( $discount['discount_percentage'] / 100));

            $infant_price_arr['discount'] = (int) $discount['discount_percentage'];
        }

        return $infant_price_arr;
    }

    function SendBankDetails(Request $request){

        dd($request->all());


    }

}