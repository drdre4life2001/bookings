<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Curl;
use Illuminate\Support\Facades\Redirect;
use Mail;


class PagesController extends Controller{

    public function getAbout(){
        return view('pages/about');
    }

    public function getHowItWorks(){
        return view('pages/benefits');
    }

    public function getBenefits(){
        return view('pages.benefits');
    }

    public function getTerms(){
        return view('pages.terms');
    }

    public function getContact(){

        return view('pages/contact');
    }

    public function getFaq()
    {
        return view('pages/faq');
    }

    public function charter()
    {
        return view('pages/charter');
    }


    public function getPartnership()
    {
        return view('pages.partnership');
    }
    public function getWhatsnew()
    {
        $articles = Curl::to(config('api.online.get-articles'))->post();
        if(!empty($articles)){
            $articles = (json_decode($articles)->data);
            $articles = array_reverse($articles);
        }else{
            $articles = [];
        }
        return view('pages.new',compact('articles'));
    }

    public function blog()
    {
        $articles = Curl::to(config('api.online.get-sample-article'))->post();
        //dd($articles);
        if(!empty($articles)){
            $articles = (json_decode($articles)->data);

        }else{
            $articles = [];
        }
        return view('pages.new',compact('articles'));
    }

    public function getArticle(Request $request){
        $article = Curl::to(config('api.online.get-article').'/'.$request->slug)->post();
        $articles = Curl::to(config('api.online.get-articles'))->post();
        $articles = json_decode($articles)->data;
        if(!empty($article)){
            $article = (json_decode($article)->data);
        }else{
            $article = [];
        }
        return view('pages.article',compact('article','articles'));

    }

    public function send(Request $request)
    {
        if($request->isMethod('post')){
            //dd($request->all());
            if($request->type == 'inquiry'){
                Mail::send('emails.plain', ['data' => $request->all()], function ($msg) use ($request) {
                    $msg->from('info@bus.com.ng', 'Bus.com.ng Enquiries');

                    $msg->to(env('APP_EMAIL'), env('APP_NAME'))->subject($request->subject);
                });
                return back()->with('status', 'Message sent!');
            }else{
                Mail::send('emails.partner', ['data' => $request->all()], function ($msg) use ($request) {
                    $msg->from('info@bus.com.ng', 'Bus.com.ng Partnership Enquiry - '.$request->company);
                    $msg->to(env('APP_EMAIL'), env('APP_NAME'))->subject('Interested in our solution.');
                });
                Mail::send('emails.partner_sender', ['data' => $request->all()], function ($msg) use ($request) {
                    $msg->from('info@bus.com.ng', 'Bus.com.ng Partnership Enquiry - '.$request->company);
                    $msg->to($request->email, env('APP_NAME'))->subject('Interested in our solution.');
                });
                return back()->with('status', 'Message sent!');
            }
            return back();
        }
    }
}
