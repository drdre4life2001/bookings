<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Curl;
use Redirect;

class FeedbackController extends Controller
{
	public function add(Request $request, $booking_id = "")
	{
		$booking = Curl::to(config("api.online.trip-from-booking-id"))
    							->withData(['booking_code' => $booking_id])
    							->post();
    	$booking = (json_decode($booking)->data);
		if($request->isMethod('post')){
			$data = $request->except('_token');

			if(!isset($data['operator_id']))
				$data['operator_id'] = isset($booking->trip->operator->id)? $booking->trip->operator->id: "";

			if(isset($booking->id))
				$data['booking_id'] = $booking->id;

			// dd($data);
			$save_feedback = Curl::to(config('api.online.save-feedback'))
									->withData($data)
									->post();

			$feedback = json_decode($save_feedback)->data;
			return back()->with('success', 'Feedback added!');
		}

		return view('users.addfeedback');
	}


public function getAdd(){
	return view('feedback.add');
}

public function postAdd(Request $request){
	$this->validate($request, array(
		'name'=>'required',
		'email'=>'required',
		'feedback'=>'required',
	));
if ($request->isMethod('post')) {
	$feedback = Curl::to(config('api.online.save-feedback'))
					->withData(['booking_id'=>1, 'rating'=>1, 'operator'=>$request->operator, 'name'=>$request->name, 'email' => $request->email, 'feedback'=>$request->feedback])
					->post();

/*
if($feedback){
	dd($feedback);
}else{
	echo 'failed';
} */
}
return Redirect::to('/feedback/getAdd')->withMessage('Your feedback has been sent successfully');
}


}
