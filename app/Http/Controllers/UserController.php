<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Curl;
use Auth;

class UserController extends Controller
{

    public function dashBoard(Request $request, $page = 1)
    {
        $customer_id = Auth::user()->id;//get this from session
        $customer_email = Auth::user()->email;

        $current_page = ($page != null) ? $page : 1;
        $per_page = env("PER_PAGE");
        $url = $request->url();


        //Get Customer Bookings
        $getBookings = Curl::to(config('api.online.get-customer-bookings'))
            ->withData(['customer_id' => $customer_id, 'per_page' => $per_page, 'page' => $current_page])
            ->post();
        $getPendingBookings = Curl::to(config('api.online.get-customer-bookings'))
            ->withData(['customer_id' => $customer_id, 'per_page' => $per_page, 'page' => $current_page, 'status' => 'PENDING'])
            ->post();

        $getPaidBookings = Curl::to(config('api.online.get-customer-bookings'))
            ->withData(['customer_id' => $customer_id, 'per_page' => $per_page, 'page' => $current_page, 'status' => 'PAID'])
            ->post();

        $getCancelledBookings = Curl::to(config('api.online.get-customer-bookings'))
            ->withData(['customer_id' => $customer_id, 'per_page' => $per_page, 'page' => $current_page, 'status' => 'CANCELLED'])
            ->post();

        $customerStats = Curl::to(config('api.online.get-customer-stats'))
            ->withData(['customer_id' => $customer_id])
            ->post();

        $bookings = json_decode($getBookings)->data;
        $all_paginate = generatePaginator($url, $current_page, $per_page, $bookings->count);

        $pending_bookings = json_decode($getPendingBookings)->data;
        $pending_paginate = generatePaginator($url, $current_page, $per_page, $pending_bookings->count);


        $paid_bookings = json_decode($getPaidBookings)->data;
        $paid_paginate = generatePaginator($url, $current_page, $per_page, $paid_bookings->count);

        $cancelled_bookings = json_decode($getCancelledBookings)->data;
        $cancelled_paginate = generatePaginator($url, $current_page, $per_page, $cancelled_bookings->count);


        $stats = json_decode($customerStats)->data;

        return view('users.dashboard', compact('bookings', 'stats', 'pending_bookings', 'paid_bookings', 'cancelled_bookings', 'all_paginate', 'pending_paginate', 'paid_paginate', 'cancelled_paginate', 'customer_email'));
    }

    public function signUp(Request $request)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                    'name' => 'required',
                    'phone' => 'required',
                    'password' => 'required'
                ]
            );
            /*$saveCustomer = Curl::to(config('api.online.save-customer'))
                ->withData($request->all())
                ->post();
            //$customer = json_decode($saveCustomer)->data;*/
            $duplicate = User::where('phone',$request->phone)->first();
            if(is_null($duplicate)){
                $new_user = new User();
                $new_user->name = $request->name;
                $new_user->email = $request->email;
                $new_user->phone = $request->phone;
                $new_user->password = bcrypt($request->password);
                $new_user->save();
                if (Auth::attempt(['phone' => $request->phone, 'password' => $request->password])) {
                    return redirect()->intended('/')->with('success','Sign up successful!');
                }
            }else{
                 return redirect()->back()->with('error','This phone number is already in use');
            }
        }
        return view('users.register');
    }

    public function signIn(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::attempt(['phone' => $request->phone, 'password' => $request->password])) {

                return redirect()->route('/');
            }

            $request->session()->flash('error', 'Wrong Username/password');
        }
        return view('users.login');
    }


    public function signOut()
    {
        Auth::logout();
        return redirect('sign-in');
    }
}
