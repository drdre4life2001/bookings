<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class Messaging extends Controller
{
    public function sendEmail(Request $request)
    {
    	$data = $request->all();

    	Mail::queue('emails.send',  $data, function ($message) use ($data){

            $message->from(env('APP_EMAIL'), env('APP_NAME'));

            $message->to($data['to']);

        });

        return response()->json(['message' => 'Request completed']);
    }
}
