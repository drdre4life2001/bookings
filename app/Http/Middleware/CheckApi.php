<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Ixudra\Curl\Facades\Curl;

class CheckApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $host = config('api');
        $api = Curl::to($host['api-host'])->get();
        $api = $api ? $api : \GuzzleHttp\json_encode(['status' => 'notOk']);
        $api =  \GuzzleHttp\json_decode($api);
        if($api->status == 'Ok') {
            return $next($request);
        }else{
            return view('errors.503');
        }

    }
}
