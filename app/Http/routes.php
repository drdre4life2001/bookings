<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::match(['get', 'post'], 'infant-price', ['uses' => 'BookingsController@getInfantTicketPrice', 'as' => 'infant-price']);
Route::match(['get', 'post'], 'save-transactions', ['uses' => 'BookingsController@Transactions', 'as' => 'transactions']);
Route::match(['get', 'post'], 'test-sms', ['uses' => 'BookingsController@Sms', 'as' => 'sms']);




// Route::match(['get', 'post'], 'trips/loading', ['uses' => 'TripsController@Loading', 'as' => 'loading']);
// Route::match(['get', 'post'], 'trips/search', ['uses' => 'TripsController@Search', 'as' => 'search']);
// // Route::match(['get'], 'nextclass/{module_id}/{c_week_id}/{course_id}', ['uses' => 'CoursesController@NextClass', 'as' => 'next-class']);
// Route::match(['get', 'post'], 'booking/{tripid}/{id}/{ids}/{return?}', ['uses' => 'BookingsController@Add', 'as' => 'new-booking']);
// Route::match(['get', 'post'], 'payment/{bk_id}', ['uses' => 'BookingsController@Payment', 'as' => 'payment']);
// Route::match(['get', 'post'], 'payment_success/{bk_id}/{on_hold?}', ['uses' => 'BookingsController@SuccessPayment', 'as' => 'payment_success']);
// Route::match(['get', 'post'], 'charter_payment_success', ['uses' => 'BookingsController@CharterSuccessPayment', 'as' => 'charter_payment_success']);

// Route::match(['get', 'post'], 'save-transactions', ['uses' => 'BookingsController@Transactions', 'as' => 'transactions']);
// Route::match(['get', 'post'], 'test-sms', ['uses' => 'BookingsController@Sms', 'as' => 'sms']);
// Route::match(['get', 'post'], 'save-subscribe', ['uses' => 'BookingsController@Subscribe', 'as' => 'save-subscribe']);
// Route::match(['get', 'post'], 'infant-price', ['uses' => 'BookingsController@getInfantTicketPrice', 'as' => 'infant-price']);

// // Route::controller('pages', 'PagesController');
// 	// Route::resource('pages', 'PagesController');
// Route::controller('pages', 'PagesController');

// Route::get('test', function(){
// 	return view('test');
// });

// // Route::get('dashboard', 'UserController@dashBoard');
// Route::post('save_customer_feedback', 'FeedbackController@add');
// Route::match(['GET', 'POST'],'feedback/{booking_id}', 'FeedbackController@add');

// // Route::match(['GET', 'POST'],'sign-up', 'UserController@signUp');
// // Route::post('sign-in', 'UserController@signIn');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------

|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::match(['get', 'post'], 'trips/loading', ['uses' => 'TripsController@Loading', 'as' => 'loading']);

Route::match(['get', 'post'], 'charter_payment_success', ['uses' => 'BookingsController@CharterSuccessPayment', 'as' => 'charter_payment_success']);

//Route::group(['middleware' => ['web']], function () {

//feedback
Route::match(['get'], 'feedback/getAdd', ['uses' => 'FeedbackController@getAdd']);
Route::match(['post'], 'feedback/postAdd', ['uses' => 'FeedbackController@postAdd']);

Route::match(['get', 'post'], 'rebook', ['uses' => 'BookingActionsController@rebook', 'as' => 'rebook']);

Route::match(['get', 'post'], 'trips/get-dests', ['uses' => 'TripsController@loadDests', 'as' => 'get-dests']);
// Route::match(['get', 'post'], 'trips/loading', ['uses' => 'TripsController@Loading', 'as' => 'loading']);
Route::match(['get', 'post'], 'trips/search/{page?}', ['uses' => 'TripsController@Search', 'as' => 'search']);
// Route::match(['get'], 'nextclass/{module_id}/{c_week_id}/{course_id}', ['uses' => 'CoursesController@NextClass', 'as' => 'next-class']);
Route::group(['middleware' => ['web']], function () {

    Route::match(['get', 'post'], '/', ['uses' => 'TripsController@Homepage', 'as' => 'homepage']);

    Route::match(['get', 'post'], 'booking/{tripid}/{fare}/{passport}/{adult}/{children}/{infants}/{date}/{end?}', ['uses' => 'BookingsController@Add', 'as' => 'new-booking']);

});

Route::match(['get', 'post'], 'book/{tripid}/{fare}', ['uses' => 'BookingsController@book_nysc', 'as' => 'new-nysc-booking']);

Route::match(['get', 'post'], 'payment/{bk_id}', ['uses' => 'BookingsController@Payment', 'as' => 'payment']);
Route::match(['get', 'post'], 'payment_success/{bk_id}/{on_hold?}', ['uses' => 'BookingsController@SuccessPayment', 'as' => 'payment_success']);
Route::match(['get', 'post'], 'nysc_payment_success', ['uses' => 'BookingsController@success_payment', 'as' => 'nysc_payment_success']);
// Route::match(['get', 'post'], 'charter_payment_success', ['uses' => 'BookingsController@CharterSuccessPayment', 'as' => 'charter_payment_success']);
Route::match(['get', 'post'], 'pay_online/{ref}', ['uses' => 'BookingsController@pay_with_amplify', 'as' => 'pay_online']);

Route::match(['get', 'post'], 'save-subscribe', ['uses' => 'BookingsController@Subscribe', 'as' => 'save-subscribe']);

Route::match(['get', 'post'], 'pages/privacy', function(){
	return view('pages.privacy');
});
Route::match(['get', 'post'], 'pages/whatsnew', ['uses' => 'PagesController@getWhatsnew', 'as' => 'whatsnew']);
Route::match(['get', 'post'], 'pages/view-article/{slug}', ['uses' => 'PagesController@getArticle',
    'as' => 'article']);
Route::match(['get', 'post'], '/pages/terms', function(){
		return view('pages.terms');
	});

Route::match(['get', 'post'], 'send-bank-details', ['uses' => 'BookingsController@SendBankDetails', 'as' => 'send-bank-details']);

// Route::controller('pages', 'PagesController');
	// Route::resource('pages', 'PagesController');




Route::get('test', function(){
	return view('test');
});

Route::post('sendemail', 'PagesController@send');

Route::group(['middleware' => ['web']], function () {


    Route::controller('pages', 'PagesController');
    Route::get('pages/about', 'PagesController@getAbout');
    Route::get('pages/faq', 'PagesController@getFaq');
    Route::get('pages/contact', 'PagesController@getContact');
    Route::get('pages/benefits', 'PagesController@getBenefits');
    Route::get('pages/charter', 'PagesController@getBenefits');

});


// Route::get('dashboard', 'UserController@dashBoard');
Route::post('save_customer_feedback', 'FeedbackController@add');
Route::match(['GET', 'POST'],'feedback/{booking_id}', 'FeedbackController@add');

// Route::match(['GET', 'POST'],'sign-up', 'UserController@signUp');
// Route::post('sign-in', 'UserController@signIn');

    Route::match(['get', 'post'], 'parks', ['uses' => 'TripsController@GetNYSCParks', 'as' => 'parks']);
    Route::match(['get', 'post'], 'nysc', ['uses' => 'TripsController@nysc', 'as' => 'nysc']);
    Route::match(['get', 'post'], 'nysc_search', ['uses' => 'TripsController@nysc_search', 'as' => 'nysc_search']);
    Route::match(['GET', 'POST'],'sign-up', 'UserController@signUp');
	Route::get('dashboard/{page?}', 'UserController@dashBoard')->middleware('auth');
	Route::match(['GET', 'POST'],'sign-in', 'UserController@signIn');
	Route::match(['GET', 'POST'],'login', 'UserController@signIn');
	Route::get('sign-out', 'UserController@signOut');
	Route::get('social/{provider}', 'SocialAuthController@redirectToProvider');
	Route::get('social/{provider}/callback', 'SocialAuthController@handleProviderCallback');

//});
