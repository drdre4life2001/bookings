<?php

// use Curl;


	function send_sms($to, $msg)
    {

        $cid = "";
        $user = "info@bus.com.ng";
        // $senderArray = 'Buscomng';
        $senderId = 'Bus.com.ng';
        $to = $to;
        $pass = "buscomng";
        $ch = curl_init();
       // dd($msg);
        $postdata = 'username=' . $user . '&password=' . $pass . '&message=' . $msg . '&sender=' . $senderId . '&mobiles=' . $to; //initialize the request variable
        // echo $postdata;
          $nmsg = urlencode($msg);

//       $url = 'http://portal.nigerianbulksms.com/api/?'; //this is the url of the gateway's interface
//        $ch = curl_init(); //initialize curl handle
//        curl_setopt($ch, CURLOPT_URL, $url); //set the url
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //return as a variable
//        curl_setopt($ch, CURLOPT_POST, 1); //set POST method
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata); //set the POST variables
//        $response = curl_exec($ch); // grab URL and pass it to the browser. Run the whole process and return the response
//        curl_close($ch); //close the curl handle
//
//        $strHeader = get_headers($url)[0];
//
//        $statusCode = substr($strHeader, 9, 3 );
//        dd($statusCode);
//       return $statusCode;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://portal.nigerianbulksms.com/api/?username=info%40bus.com.ng&password=buscomng&message='$nmsg'&sender=$senderId&mobiles=$to",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "postman-token: 15b0c421-0369-2444-44ee-f18874567e92"
            ),
        ));

        $response = curl_exec($curl);
        return $response;
    

    }

	function get_infant_discount($passenger = [], $rules = [])
	{
		//check to see if the passenger falls into the category of an infant
		$discount_arr = [];
		if(!empty($rules)){
			if($passenger['age'] <= $rules['infant']['max_age']){
				if(isset($rules['discount'])){
					foreach($rules['discount'] as $discount){
						if($passenger['age'] >= $discount['min_age'] and $passenger['age'] <= $discount['max_age'])
							$discount_arr = $discount;
					}
				}
			}
		}
		return $discount_arr;
	}

	function generatePaginator($url, $page, $per_page, $total, $params = null)
	{
            $current_page = $page;

            $pages_details_view = [];

            $page_arr = explode("/", $url);
            $page_val = $current_page;
            if($current_page > 1){
            	$page_val =  (int) (array_pop($page_arr));
            	$url = implode("/", $page_arr);
            }
            $url_params = "?";
            if(!is_null($params)){

            	foreach ($params as $key => $value) {

	        		if(!is_array($value)){
	        		    $url_params.= isset($key)? "$key=$value&":"";
	        		}
	          		else{
	          			foreach ($value as $keym => $val) {
	          				$url_params.= isset($keym)? "$key"."[".$keym."]=$val&":"";
	          			}
	          		}
	        	}
            }

            $page_depth = ceil($total / $per_page);

            for($i = 1; $i <= $page_depth; $i++)
            {
                if($i > $current_page - 4 && $i < $current_page + 4)
                {
                    $item = [];
                    $item['page'] = $i;
                    $item['url'] = $url.'/'.$i.$url_params;

                    if($current_page == $i)
                        $item['active'] = true;

                    $pages_details_view['pages'][] = $item;
                }
            }

            if($current_page > 1){
                $pages_details_view['prev_page_link'] = $url.'/'.($page_val-1).$url_params;
            }
            if($current_page < $page_depth){
                $pages_details_view['next_page_link'] =  $url.'/'.($page_val+1).$url_params;
            }


		return $pages_details_view;
	}

?>


