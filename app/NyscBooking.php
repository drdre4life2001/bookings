<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class NyscBooking extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "nysc_bookings";

    public function operators(){
        return $this->hasMany('App\Models\Operator');
    }

    public function parks(){
        return $this->hasMany('App\Models\Park');
    }

}
