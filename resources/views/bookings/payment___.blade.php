@extends('layouts.app')

@section('content')


    @include('bookings.includes.steps')

    <div class="gap"></div>

    <div id="main-content">
        <div id="icon-box">
            <img src="{{ URL::asset('img/booking-banner.jpg') }}" width="100%">
        </div>

        <div id="searchdetails-box"></div>
    </div>

    <h3>Make Payment</h3>
    <hr/>
    <div class="row row-wrap">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="bs-example">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Pay With Your
                                        ATM Card</a>
                                </h3>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <h3>Pay with your Card</h3>
                                    <!-- <p>Important: You will be redirected to PayPal's website to securely complete your payment.</p> -->
                                    <a onclick="pay()" class="btn btn-primary1 btn-large">
                                        <img class="pp-img"
                                             src="<?php echo asset('img/click-here-to-pay-online.jpg') ?>"
                                             alt="Pay with your Card" title="Pay with your Card"/>
                                        <!--Checkout via PayStack -->
                                    </a>
                                    <div class="gap"></div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Book On
                                            Hold</a>
                                    </h3>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <h3>Book on Hold</h3>
                                        <p>Booking on hold means your booking is reserved for 5 hours while we await
                                            your payment. If
                                            payment is not received by then, you will forfiet your booking and your
                                            booking code will be invalid.</p>
                                        <p></p>
                                        <p>See the list of banks you can pay to below:</p>
                                        <table class="table table-bordered table-striped table-booking-history">
                                            <thead>
                                            <tr>
                                                <th>Bank</th>
                                                <th>Account number</th>
                                                <th>Account Name</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($banks as $bank)
                                                <tr>

                                                    <td>{{ $bank->name }}</td>
                                                    <td>{{ $bank->acount_number }}</td>
                                                    <td class="text-center">{{ $bank->account_name }}
                                                    </td>
                                                    <td class="text-center"><a
                                                                class="btn btn-default btn-sm sendAcctDetails"
                                                                title="SMS this bank details to me."
                                                                data-payload="{{ $bank->name }},{{$bank->acount_number}},{{ $bank->account_name }}"
                                                                href="#"><i class="fa fa-mobile"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                        <p></p>

                                        @if(!empty($cash_offices))
                                            <p>You can also pay into any of our cash offices listed below:</p>
                                            <table class="table table-bordered table-striped table-booking-history">
                                                <thead>
                                                <tr>
                                                    <th>Contact Name</th>
                                                    <th>Phone</th>
                                                    <th>Address</th>
                                                    <th></th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($cash_offices as $co)
                                                    <tr>

                                                        <td>{{ $co->contact_name }}</td>
                                                        <td>{{ $co->phone }}</td>
                                                        <td class="text-center">{{ $co->address }}
                                                        </td>
                                                        <td class="text-center"><a
                                                                    class="btn btn-default btn-sm sendCashOffice"
                                                                    title="SMS this Cash Office details to me."
                                                                    data-payload="{{$co->contact_name}}|{{$co->phone}}|{{$co->address}}"
                                                                    href="#"><i class="fa fa-mobile"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                        @endif
                                        <a class="btn btn-primary btn-large"
                                           href="{{ route('payment_success', [$booking_code, TRUE]) }}">Book on Hold</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-md-offset-2">
                @include('bookings.includes.summary')
            </div>
            <div class="gap"></div>
        </div>
    </div>
    </div>

    <script src="https://js.paystack.co/v1/inline.js"></script>
    <script>
            <?php
            $uuid = uniqid(true);
            $parent_booking_cost = isset($parent_booking->final_cost) ? $parent_booking->final_cost * 100 : 0;
            $parent_booking_id = isset($parent_booking->id) ? $parent_booking->id : "";
            $parent_booking_code = isset($parent_booking->booking_code) ? $parent_booking->booking_code : "";
            ?>
        var ajax_image = "<img src='<?php echo asset('img/loading.gif') ?>' alt='Loading...' />";

        function pay() {
            var old_content = $("#sub_btn").html();

            $("#sub_btn").html(ajax_image);

            var url = "{{ route('transactions') }}";
            $.ajax
            ({
                type: "POST",
                url: url,
                data: ({
                    booking_id: "{{$booking->booking->id}}",
                    booking_code: "{{$booking->booking->booking_code}}",
                    status: 'Failed',
                    response: 'Transaction Not Found',
                    parent_booking_id: "{{$parent_booking_id}}",
                    parent_booking_code: "{{$parent_booking_code}}"
                }),
                success: function (response) {
                    console.log(response)
                    loadPaystack(old_content);
                }
            });

        }

        function loadPaystack(content) {
            var handler = PaystackPop.setup({
                key: 'pk_live_8a25bdcd5e037076abb8e1d3656b727106192967',
                email: "{{$booking->booking->contact_email}}",
                amount: {{$booking->booking->final_cost * 100}} + <?php echo((env("CONVENIENCE_FEE") * 100) + $parent_booking_cost);?>,
                ref: '<?php echo $uuid; ?>',
                callback: paystackcallback,

                onClose: function () {
                    var url = "{{ route('transactions') }}";
                    var data = ({
                        booking_id: "{{$booking->booking->id}}",
                        booking_code: "{{$booking->booking->booking_code}}",
                        status: 'Cancelled',
                        response: 'Transaction cancelled by user',
                        cancelled: 'yes',
                        parent_booking_id: "{{$parent_booking_id}}",
                        parent_booking_code: "{{$parent_booking_code}}"
                    });
                    $.post(url, data, function (response) {
                    });
                    $("#sub_btn").html(content);
                    //$.post(url, data, function(response){});
                }
            })
            handler.openIframe()
        }

        function paystackcallback(response) {
            var url = "https://paystack.ng/charge/verification";
            $.ajax
            ({
                type: "POST",
                url: url,
                data: ({trxref: response.trxref, merchantid: 'pk_live_8a25bdcd5e037076abb8e1d3656b727106192967'}),
                success: function (response) {
                    var pars = (JSON.parse(response))
                    var resp = (pars.status)
                    if (resp == 'success') {
                        var oldurl = "{{ route('transactions') }}";
                        $.ajax
                        ({
                            type: "POST",
                            url: oldurl,
                            data: ({
                                booking_id: "{{$booking->booking->id}}",
                                booking_code: "{{$booking->booking->booking_code}}",
                                status: 'Success',
                                response: 'Transaction Successful',
                                payment_method: 'PayStack',
                                parent_booking_id: "{{$parent_booking_id}}",
                                parent_booking_code: "{{$parent_booking_code}}"
                            }),
                            success: function (response) {
                                if (response == 1) {
                                    window.location.href = '{{route("payment_success", [$booking->booking->booking_code])}}'; //using a named route
                                }
                                ;
                            }
                        });
                    }
                }
            });
        }


        // {"status":"success","message":"Successful",
        // "transaction":{"amount":"5000","transaction_date":"2016-03-06 10:00:40","status":"success",
        // "authorization":{"authorization_code":"AUTH_8uonx9dy","email":"info@bus.com.ng","description":"visa ending with 1381"},
        // "trxref":"156dbef4f6317c","first_name":"","last_name":"","email":"info@bus.com.ng","gateway_response":"Success","plan":"0","paystack_reference":"PSTKg285q"}}


        $(".sendAcctDetails").each(function (index, element) {
            var phone = $(element).attr('data-booking-phone');
            var amount = $(element).attr('data-booking-amount');
            var payload = $(element).attr('data-payload');
            // $( element ).html( '<span style="font-style:italic;font-size:80%;"><img width="15px" src="{{ asset("desktop/img/ajax-loader-fb.gif") }}">');
            $.ajax
            (
                {
                    type: "POST",
                    url: "{{ url('send-bank-details') }}/" + phone + '/' + amount + '/' + payload,
                    data: ({rnd: Math.random() * 100000, _token: '{{ csrf_token() }}'}),
                    success: function (html) {
                        $(element).html(html);
                        $(element).removeClass('duplicate-job');

                    }
                });

        });

    </script>

@stop
