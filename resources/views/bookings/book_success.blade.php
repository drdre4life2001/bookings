@extends('layouts.app2')

<header>
        <div class="row">
          <div class="col-lg-4 col-lg-offset-4">
            @if(Session::has('error'))
                <div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Oops!</strong> {{ session::get('error') }}.
                </div>
            @endif


            @if(Session::has('success'))
                <div class="alert alert-success fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Success!</strong> {{ session::get('success') }}.
            </div>

            @endif
        </div>
        <div class="container">
          <?php
                $parent_booking_cost = isset($parent_booking->final_cost)? $parent_booking->final_cost * 100: 0;
                $parent_booking_id = isset($parent_booking->id)? $parent_booking->id: "";
                $parent_booking_code = isset($parent_booking->booking_code)? $parent_booking->booking_code: "";
            ?>
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <a href="{{ url('/') }}"><img src="{{ URL::asset('img/bus-logo.png') }}" width="25%" class="logo"></a>
              </div>
        
        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
         <div id="h-side" class="col-lg-8 col-md-8 hidden-sm hidden-xs">
        	<ul>
        	<li><img src="{{ URL::asset('img/mobile-app-development.png') }}" style="margin-top:-3%"></li>
        	<li>
        	
        		<i class="fa fa-facebook-square" aria-hidden="true" style="color:#2c54c7; font-size: 30px"></i>
        		<i class="fa fa-twitter-square" aria-hidden="true" style="color:#19add8; font-size: 30px"></i>
        		<i class="fa fa-google-plus-square" aria-hidden="true" style="color:#e92c2c; font-size: 30px"></i>

        	</li>

        		<li><img src="{{ URL::asset('img/phone-512.png') }}" width="8.5%" style="float: left; padding-right: 2%">
        	<span class="n-num"> Call: 08082729131, 07092912321 </span></li>
        		
        	</ul>
   <!-- .cd-3d-nav -->

    </header>

  <div id="main-content" style="margin-bottom:0%">
  	
  	<div id="searchdetails-box" style="padding:2%;">
  		 <div class="container">
  		 <h2>Payment & Travelling Details </h2>
  		 </div>
  	</div>
  	<div id="paydetails-box" style="">
  				 <div class="container">
  				  <h2 align="center">
  		 		<img src="{{ URL::asset('img/progress.png') }}" width="50%">

  		</h2>
  		<hr/>
  				<div class="row">
                <div class="col-md-8 col-md-offset-2">
                    @if($booktype == "onhold")
                    <div  class="round box-icon-large box-icon-center box-icon-success mb30">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="alert alert-success" >
                      <h2 class="text-center">Your Booking was successful and it is on Hold for 5 hours!</h2>
                      <h5 class="text-center mb30">Booking details has been sent to your phone number [{{ $booking->booking->contact_phone }}] and email address [{{ $booking->booking->contact_email }}]</h5>
                      <ul class="order-payment-list list mb30">
                          <li>
                              <div class="row">
                                  <p>Please note that Booking on hold means your booking is reserved for 5 hours while we await your payment. If payment is not received by then, you will forfiet your booking and your booking code will be invalid.</p>
                              </div>
                          </li>

                      </ul>
                    </div>
                    @else

                    <div class="round box-icon-large box-icon-center box-icon-success mb30">
                        <i class="fa fa-check"></i>
                    </div>
                    
                    <div class="alert alert-success" >
                        <h2 class="text-center">Your payment was successful!</h2>
                        <h5 class="text-center mb30">Booking details has been sent to your phone number [{{ $booking->booking->contact_phone }}] and email address [{{ $booking->booking->contact_email }}]</h5>

                        <ul class="order-payment-list list mb30">
                            <li>
                                <div class="row text-center">
                                    <p>Safe Journey!</p>
                                </div>
                            </li>

                        </ul>
                    </div>
                    @endif

                    
                    {{-- @include('bookings.includes.summary') --}}

                </div>
  				 	</div>
  				 	
  				 </div>
  	</div>
  	<div id="payment-d">
  			<div class="container">

  			<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
  				<div class="p-details">
  					<h2><i class="fa fa-users"></i> Travellers Details </h2>
  					{{-- <h3>Please enter name as they appear on identification document</h3> --}}
  					<div class="row">
  						<form style="margin-top: 2%">
	  						<div class="col-md-6">
								<div class="form-group">
									<label>Contact Name:</label>
									<input type="text" disabled="" value="{{ $booking->booking->contact_name }}" name="">
									{{-- <label name=""> {{ $booking->sourcepark->name }} </label> --}}
								</div>

                  <div class="form-group">
                    <label>Leaving From:</label>
                    <input type="text" disabled="" value="{{ $booking->sourcepark->name }}" name="">
                    {{-- <label name=""> {{ $booking->sourcepark->name }} </label> --}}
                  </div>

	  							<div class="form-group">
                    <label>Booking Code:</label>
                    <input type="text" disabled="" value="{{ $booking->booking->booking_code }}" name="">

                  </div>
                  
                 
                      
	  						</div>
	  						<div class="col-md-6">
	  								
	  							{{-- <div class="form-group">
			  						<label>Gender</label><br/>
		  							<select>
		  								<option>Male</option>
		  								<option>Female</option>
		  							</select>
	  							</div> --}}


                  <div class="form-group">
                    <label>Departure Date:</label>
                    <input type="text" disabled="" value="<?php echo date('D, M dS', strtotime($date)); ?>" name="">
                  </div>

                  <div class="form-group">
                    <label>Departure Time:</label>
                    <input type="text" disabled="" value="{{ date('H:iA', strtotime($trip->departure_time)) }}" name="">
                  </div>
								<div class="form-group">
									<label>Next of Kin:</label>
									<input type="text" disabled="" value="{{ $booking->booking->next_of_kin }}" name="">
									{{-- <label name=""> {{ $booking->sourcepark->name }} </label> --}}
								</div>

                  @if(!empty($return_trip))
                    <div class="form-group">
                      <label>Returning From:</label>
                      <input type="text" disabled="" value="{{ $return_trip->sourcepark->name }}" name="">
                    </div>

                    <div class="form-group">
                      <label>Arrivint At:</label>
                      <input type="text" disabled="" value="{{ $return_trip->destpark->name }}" name="">
                    </div>

                    <div class="form-group">
                      <label>Rturning Date:</label>
                      <input type="text" disabled="" value="<?php echo date('D, M dS', strtotime($return_date)); ?>" name="">
                    </div>
                  @endif
                  
	  						</div>

	  						{{--<p style="padding:2%; color:#999; font-size: 13px;">By clicking book, it mean you accept bus.com.ng terms and conditions--}}
 {{--and OKEYSON INVESTMENT's terms.</p>--}}
  						</form>
  					</div>
  				</div>

  				
  			</div>

  			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  				<div class="p-details" style="padding:6%; margin-top: 10%">
  				<h2><i class="fa fa-credit-card-alt" aria-hidden="true"></i> Booking Summary </h2>
  				    {{-- @if($trip->ac)
                <span<i class="fa fa-check-square-o font-green-jungle"></i>  AC</span> 
              @else
                <span><i class="fa fa-times-circle font-red-thunderbird"></i>  AC</span>  
              @endif
              
              &nbsp;
              
              <hr>
              @if($trip->security)
                  <span><i class="fa fa-check-square-o font-green-jungle"></i>  Security</span> 
              @else
                  <span ><i class="fa fa-times-circle font-red-thunderbird"></i>  Security</span> 
              @endif

              &nbsp;&middot;&nbsp;

              <hr>
              @if($trip->insurance)
                  <span ><i class="fa fa-check-square-o font-green-jungle"></i>  Insurance</span> 
              @else
                  <span><i class="fa fa-times-circle font-red-thunderbird"></i>  Insurance</span> 
              @endif

              &nbsp;&middot;&nbsp;

              <hr>

              <br/>
              @if($trip->tv)
                  <span><i class="fa fa-check-square-o font-green-jungle"></i>  TV</span> 
              @else
                  <span><i class="fa fa-times-circle font-red-thunderbird"></i>  TV</span> 
              @endif

              &nbsp;&middot;&nbsp;

              <hr>

              @if($trip->passport)
                  <span><i class="fa fa-check-square-o font-green-jungle"></i>  Passport</span> <br> <br>
                  <hr>
              @else
                  <span><i class="fa fa-times-circle font-red-thunderbird"></i>  Passport</span> <br> <br>
              @endif --}}

                  {{--{{ $ad = isset($adults) ? $adults: ''}}--}}
                                {{--<h5>({{ $ad }} @if($ad == 1) Adult @else Adults @endif, {{$children}}@if($children == 1) Child @else Children @endif, {{$infants}}@if($infants == 1) Infant @else Infants @endif)</h5>--}}
                                <ul class="booking-item-payment-price">
                                    <li>
                                        <p class="booking-item-payment-price-title"> {{ $booking->booking->passenger_count }}  Passenger(s)</p>
                                       <?php
                                            $international = isset($intl)?1:0; 
                                        ?>
                                        @if($international !== 1)
                                        <p class="booking-item-payment-price-amount">&#x20A6; <?php echo
                                            number_format( $booking->booking->unit_cost + $bus_fee ); ?><small>/per passenger</small></p>
                                        @else
                                        <p class="booking-item-payment-price-amount">&#x20A6; <?php echo
                                            number_format( $booking->booking->unit_cost + $bus_fee); ?><small>/per passenger</small></p>
                                        @endif
                                    </li>
                                    @if($children > 0)
                                    <li>
                                        <p class="booking-item-payment-price-title"> {{ $children }} @if($children ==
                                            1) Child @else Children @endif</p>
                                        <?php
                                            $international = isset($intl)?1:0; 
                                        ?>
                                        @if($international !== 1)
                                        <p class="booking-item-payment-price-amount">&#x20A6; <?php echo
                                            number_format( ( (1 - ($booking->operator->local_children_discount/ 100)) * $trip->fare) + $bus_fee  ); ?><small>/per child</small>
                                        </p>
                                        @else
                                        <p class="booking-item-payment-price-amount">&#x20A6; <?php echo
                                            number_format( ( (1 - ($booking->operator->local_children_discount/ 100)) *
                                                $booking->booking->unit_cost) + $bus_fee ); ?><small>/per child</small></p>
                                            @endif
                                    </li>
                                    @endif
                                    @if($infants > 0)
                                    <li>
                                        <p class="booking-item-payment-price-title"> {{ $infants }} @if($infants == 1) Infant @else Infants @endif</p>
                                        <p class="booking-item-payment-price-amount">&#x20A6; <?php echo number_format( 0 ); ?><small>/per infant</small>
                                        </p>
                                        
                                    </li>
                                    @endif
                                    <li>
                                        <p class="booking-item-payment-price-title"> Convenience fee</p>
                                        <p class="booking-item-payment-price-amount">&#x20A6; <?php echo number_format( config('custom.convenience_fee') ); ?>
                                        </p>
                                        
                                    </li>
                                   

                                </ul>
                            </li>
                        </ul> <hr>
                        <p class="booking-item-payment-total">Total Cost: <span>&#x20A6; {{ number_format($final_cost, 2) }} </span>
                        </p>

  				</div>
  			</div>
	  	
	  		</div>
  	</div>
  
   

   </div>
    			
    	





{{-- <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Trigger modal</a> --}}

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
