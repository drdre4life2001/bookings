@extends('layouts.app')

@section('content')

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/seatselector.css') }}">

    <div id="main-content">
        <div id="icon-box">
            <img src="{{ URL::asset('img/booking-banner.jpg') }}" width="100%">
        </div>

        <div id="searchdetails-box">
            <div class="container">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding:2% 0%">
                    <h1><i class="fa fa-bus"></i> Trip Summary</h1> <br>
                    <h2>{{ $trip->sourcepark->name }} to {{ $trip->destpark->name }} - {{ date('gA', strtotime($trip->trip->departure_time)) }} </h2>
                    <p> Selected Operator: {{ $trip->operator->name }}</p> <br>
                    <p> <i class="fa fa-users" aria-hidden="true"></i> <span style="color:#ffca4a"> {{ $psg + $children }} passenger(s) </span>   </span> </p>

                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
                    <a type="button" href="{{ url('/') }}" class="btn btn-large btn-warning"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> <b>Modify Search</b></a>
                </div>
            </div>
        </div>
        <div id="box-aval">
            <div class="container">
                <i class="fa fa-credit-card" aria-hidden="true"></i> Passenger Details
            </div>
        </div>

        <div id="payment-d">
            <div class="container">

                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">

                    <div class="modal fade" tabindex="-1" role="dialog" id="chooseSeats">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content modal-lg">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Select {{ $psg + $children }} seat(s)</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="seat-main">
                                        <div class="seats">

                                        </div>
                                        <!--enable submit button when a seat is selected -->

                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                                $('.seats').click(function(e){
                                                    $('#submitButtonBookNow').removeAttr("disabled", true); //disable the submit button by defaul
                                                    $('#holdButtonBookNow').removeAttr("disabled", true); //disable the submit button by default
                                                    $('#warningText').hide();
                                                });
                                            });


                                        </script>


                                        <div class="seat-legend-main">
                                            <ul class="seat-legend">
                                                <li class="seat-legend-item"><span>Avalailable seat</span> <img src="{{ asset('img/available_seat_img.gif') }}"></li>
                                                <li class="seat-legend-item"><span>Booked seat</span> <img src="{{ asset('img/booked_seat_img.gif') }}"></li>
                                                <li class="seat-legend-item"><span>Selected seat</span> <img src="{{ asset('img/selected_seat_img.gif') }}"></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Save Seat</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->

                    <div class="p-details col-sm-12">
                        <div class="row">
                            <h2 class="col-sm-8"><i class="fa fa-users"></i> Customer Details</h2>
                            <div class="col-sm-4">
                                <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#chooseSeats">
                                    Select Seats
                                </button>
                            </div>
                            <h3 class="col-sm-12">Please enter name as they appear on identification document</h3>
                           <p></p>

                        </div>

                        <div class="row">
                            @if(count( $errors ) > 0)
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-error" style="font-size: 14px; color: red;">{{ $error }}</div>
                                @endforeach
                            @endif


                            <form action="" method="post"data-toggle="validator"name="form" role="form" id="submitter">
                                {!! csrf_field() !!}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputName" class="control-label">Full Name</label>
                                        <input class="form-control" required value="@if($user){{$user->name}}@endif"  id="contact_name" type="text" placeholder="Cina Saffary" name="contact_name" />
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail" class="control-label">Email</label>
                                        <input type="email" class="form-control" id="contact_email"  value="@if($user){{$user->email}}@endif" placeholder="Email" name="contact_email" data-error="Bruh, that email address is invalid" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPone" class="control-label">Telephone</label>
                                        <input class="form-control"    value="@if($user){{$user->phone}}@endif" type="number" maxlength="11" minlength="11" id="contact_phone" name="contact_phone" required />
                                        <div class="help-block with-errors"></div>
                                    </div>



                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label>Gender</label><br/>
                                        @if($user)
                                            @if($user->contact_gender == 'Male')
                                                <select name="gender" class="form-control" required id="contact_gender" required>
                                                    <option value="">Select Your Gender</option>
                                                    <option value="Female">Female</option>
                                                    <option value="Male" selected="selected">Male</option>
                                                </select>
                                            @else
                                                <select name="gender" class="form-control" required id="contact_gender" >
                                                    <option value="">Select Your Gender</option>
                                                    <option value="Female" selected="selected">Female</option>
                                                    <option value="Male">Male</option>
                                                </select>
                                            @endif
                                        @else
                                            <select name="gender" class="form-control" required id="contact_gender">
                                                <option value="">Select Your Gender</option>
                                                <option value="Female">Female</option>
                                                <option value="Male">Male</option>
                                            </select>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Next of Kin</label>
                                        <input class="form-control" required type="text" name="kin_name"  id ="kin_name"/>
                                    </div>

                                    <div class="form-group">
                                        <label>Next of Kin Phone</label>
                                        <input class="form-control" required type="text" id="kin_phone" name="kin_phone" />
                                    </div>

                                </div>
                                <div class="col-sm-12">
                                    @if($intl == 1)
                                        @include('bookings.includes.passport')
                                    @endif
                                </div>
                                <div class="col-sm-12">
                                    @if($psg > 1)
                                        <h3>Other Passengers</h3>
                                        <ul class="list booking-item-passengers" id="passengers-section">
                                            @if($psg > 1)

                                                <?php for ($i=1; $i < $psg; $i++):?>
                                                <li class="other-passenger">
                                                    <div class="">
                                                        <div class="col-md-2">
                                                            <label><i class="fa fa-close remove-icon"></i> <span>Passenger</span> <?php echo $i + 1; ?></label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Full Name</label>
                                                                <input name="passengers<?php echo $i; ?>" class="form-control" type="text" required="" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>Gender</label>
                                                                <select name="p_gender<?php echo $i; ?>" class="form-control" required>
                                                                    <option value="">Select Gender</option>
                                                                    <option value="Female">Female</option>
                                                                    <option value="Male">Male</option>
                                                                </select>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                @if($intl == 1)
                                                    @include('bookings.includes.passport')
                                                @endif
                                                <?php endfor ?>
                                            @endif


                                        </ul>
                                    @endif
                                </div>

                                <div class="col-sm-12">
                                    @if($children > 0)
                                        <h3>Children</h3>
                                        <ul class="list booking-item-passengers" id="infants-section">

                                            <?php for ($i=1; $i <= $children; $i++):?>

                                            <li>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label><i class="fa fa-close remove-infant" id="remove-infant<?php echo $i; ?>"></i><span> Child <?php echo $i; ?></span></label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Full Name</label>
                                                            <input id="infantname<?php echo $i;?>" name="child<?php echo $i; ?>" class="form-control" type="text" required="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label>Gender</label>
                                                            <select name="child_gender<?php echo $i; ?>" class="form-control" required>
                                                                <option value="">-Choose-</option>
                                                                <option value="Female">Female</option>
                                                                <option value="Male">Male</option>
                                                            </select>

                                                        </div>

                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label>Age</label>
                                                        <!-- <input id="<?php echo $i;?>" class="form-control infantHandler" name="i_child<?php echo $i; ?>" type="text"  required="" min="4" max="11" /> -->
                                                            <select  id="<?php echo $i;?>"  name="child_age<?php echo $i; ?>" class="form-control" required>
                                                                <option value="">-Choose-</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            @if($intl == 1)
                                                @include('bookings.includes.passport')
                                            @endif
                                            <?php endfor ?>

                                        </ul>
                                    @endif
                                </div>

                                <div class="col-sm-12">
                                    @if($infants > 0)
                                        <h3>Infant(s)</h3>
                                        <ul class="list booking-item-passengers" id="infants-section">

                                            <?php for ($j= $i; $j < $infants + $i; $j++):?>

                                            <li>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label><i class="fa fa-close remove-infant" id="remove-infant<?php echo $j; ?>"></i><span> Infant <?php echo $j - $i; ?></span></label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Full Name</label>
                                                            <input id="infantname<?php echo $j;?>" name="infants<?php echo $j; ?>" class="form-control" type="text" required="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label>Gender</label>
                                                            <select name="i_gender<?php echo $j; ?>" class="form-control" required>
                                                                <option value="">-Choose-</option>
                                                                <option value="Female">Female</option>
                                                                <option value="Male">Male</option>
                                                            </select>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label>Age</label>
                                                        <!-- <input id="<?php echo $j; ?>" class="form-control infantHandler" name="i_age<?php echo $j; ?>" type="text" required="" min="1" max="4" /> -->
                                                            <select  id="<?php echo $j;?>"  name="i_age<?php echo $j; ?>" class="form-control" required>
                                                                <option value="">-Choose-</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            @if($intl == 1)
                                                @include('bookings.includes.passport')
                                            @endif
                                            <?php endfor ?>


                                        </ul>
                                    @endif
                                </div>

                                <p style="font-size: 10px; font-family: Helvetica, Arial, sans-serif" class="col-md-12"> <!-- style="padding:2%; color:#999; font-size: 13px;" -->By clicking book, it means you accept bus.com.ng terms and conditions and {{ $trip->operator->name }}'s terms.</p>
                                <input type="hidden" name="departure_date" value="{{$date}}">
                                <input type="hidden" name="return_date" value="{{$return_date}}">
                                <input type="hidden" name="infant_count" id="i_count" value="{{ $infants }}">
                                <input type="hidden" name="children_count" id="c_count" value="{{ $children }}">
                                <input type="hidden" name="final_cost" id="final_cost" value="{{$final_cost }}">
                                <input type="hidden" name="passenger_count" id="p_count" value="">
                                <input type="hidden" name="transaction_ref" id="transaction_ref" value="$_COOKIE['transaction_ref']">
                                <input type="hidden" name="selected_seats" id="seat_selected" value="">
                                 <input type="hidden" name="booktype" id="booktype" value="paystack">
                                <input type="hidden" name="payment_method" id="payment_method" value="PayStack">

                                <!-- <div id="warningText" style="color: red;">
                                  <p>You haven't selected a seat. <br/>Please scroll up and select a seat so as to activate the button below. </p></div> -->
                                <div class="col-sm-12 full-center">


                                    <br>
                                    <a onclick="payWithPaystack()" data-toggle="modal" id="submitButtonBookNow" disabled  class="btn btn-large btn-danger" style="margin-left:25%">
                                        <h1> Pay With Your ATM Card!</h1>
                                    </a>
                                    <a href="#modal-book" data-toggle="modal" id="holdButtonBookNow" disabled  class="btn btn-large btn-danger" style="margin-top:2%">
                                        <h1> Book on Hold!</h1>
                                    </a>
                                </div>
                            </form>

                        </div>
                    </div>


                </div>


                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="p-details  col-sm-12" style="padding:6%; margin-top: 10%">
                        <div class="row">


                            <div class="col-md-12">


                                <h2>Trip Details</h2>
                                <div class="well" style="background:white; border-radius: 10px; font-weight: bold; font-size:30px;">
                                    <label>Bus Type: {{ $trip->busType->name }} seats({{ $trip->busType->no_of_seats }}) </label> <br>
                                    <hr>
                                    <label>Source Park: {{ $trip->sourcepark->name }} </label> <br>
                                    <hr>
                                    <label>Trip Destination: {{ $trip->destpark->name }} </label>
                                    <hr>
                                    <label>Trip Code:{{ $trip->trip->trip_code }} </label><br>
                                    <hr>
                                    <label>Unit Cost: &#x20A6;{{ $fare }} </label>
                                    <hr>
                                    <label>Convenience Fee: &#x20A6; <?php echo number_format( config('custom.convenience_fee') ) ?> </label>


                                    <label class="btn btn-success" style="font-weight: bold;">Total Amount:&#x20A6; {{ number_format($final_cost, 2) }} </label> <br>

                                </div>
                            </div>
                        </div>

                    </div>


                </div>

            </div>
        </div>



    </div>

@stop


<div class="modal fade" id="modal-book">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center>
                    <h2  style="color: red;" class="modal-title">Book on Hold</h2>
                </center>
            </div>
            <div style="" class="modal-body">
        <span style="color:#222;">
          <p>Booking on hold means your booking is reserved for 5 hours while we await
                                            your payment. If
                                            payment is not received by then, you will forfiet your booking and your
                                            booking code will be invalid.</p>
                                        <p></p>
                                        <p>See the list of banks you can pay to below:</p> <br> <br>
                                        <table style="color: #7c7e7f" class="table table-bordered table-striped table-booking-history">
                                            <thead>
                                            <tr>
                                                <th>Bank</th>
                                                <th>Account number</th>
                                                <th>Account Name</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($banks as $bank)
                                                <tr>

                                                    <td>{{ $bank->name }}</td>
                                                    <td>{{ $bank->acount_number }}</td>
                                                    <td class="text-center">{{ $bank->account_name }}
                                                    </td>
                                                    <td class="text-center"><a
                                                                class="btn btn-default btn-sm sendAcctDetails"
                                                                title="SMS this bank details to me."
                                                                data-payload="{{ $bank->name }},{{$bank->acount_number}},{{ $bank->account_name }}"
                                                                href="#"><i class="fa fa-mobile"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                        <p></p>

            {{--@if(!empty($cash_offices))--}}
                {{--<p>You can also pay into any of our cash offices listed below:</p>--}}
                {{--<table style="color: #7c7e7f" class="table table-bordered table-striped table-booking-history">--}}
                                                {{--<thead>--}}
                                                {{--<tr>--}}
                                                    {{--<th>Contact Name</th>--}}
                                                    {{--<th>Phone</th>--}}
                                                    {{--<th>Address</th>--}}
                                                    {{--<th></th>--}}

                                                {{--</tr>--}}
                                                {{--</thead>--}}
                                                {{--<tbody>--}}
                                                {{--@foreach($cash_offices as $co)--}}
                                                    {{--<tr>--}}

                                                        {{--<td>{{ $co->contact_name }}</td>--}}
                                                        {{--<td>{{ $co->phone }}</td>--}}
                                                        {{--<td class="text-center">{{ $co->address }}--}}
                                                        {{--</td>--}}
                                                        {{--<td class="text-center"><a--}}
                                                                    {{--class="btn  btn-default btn-sm sendCashOffice"--}}
                                                                    {{--title="SMS this Cash Office details to me."--}}
                                                                    {{--data-payload="{{$co->contact_name}}|{{$co->phone}}|{{$co->address}}"--}}
                                                                    {{--href="#"><i class="fa fa-mobile"></i></a>--}}
                                                        {{--</td>--}}
                                                    {{--</tr>--}}
                                                {{--@endforeach--}}

                                                {{--</tbody>--}}
                                            {{--</table>--}}
            {{--@endif--}}

        </span>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger btn-large" data-toggle="modal" onclick="hold()"  href="">Book on Hold Now!</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

{{--key: 'pk_test_27de415452e0decc11cca9344a9e29c02b9c09d7'--}}

       @section('scripts')
<script src="https://js.paystack.co/v1/inline.js"></script>

<script>

    function hold() {
        document.getElementById("booktype").value = "onhold";
        document.getElementById("submitter").submit();
    }



    function payWithPaystack(){


        var x = document.forms["form"]["contact_name"].value;
        if (x == "") {
            alert("Name must be filled out");
            return false;
        }


        var x = document.forms["form"]["gender"].value;
        if (x == "") {
            alert("You must pick your gender");
            return false;
        }

        var x = document.forms["form"]["kin_name"].value;
        if (x == "") {
            alert("Next of kin name must be filled out");
            return false;
        }
        var x = document.forms["form"]["contact_phone"].value;
        var isnum = /^\d+$/.test(x);
        var n = x.length;
        if (n != 11 ||isnum == false){

            alert("not a valid phone number")
            return false;
        }
        var x = document.forms["form"]["kin_phone"].value;
        var isnum = /^\d+$/.test(x);
        var n = x.length;
        if (n != 11 ||isnum == false){

            alert("not a valid phone number")
            return false;
        }


        var raw_string_number =  $('#final_cost').val();
        var clean_string_number = raw_string_number.replace(',','');
        var clean_number =  parseInt(clean_string_number);
        var url = "https://paystack.ng/charge/verification";

        console.log(clean_number);
        var handler = PaystackPop.setup({
            key: '{{$paystack_key}}',
            email: $('#contact_email').val(),
            amount: (clean_number * 100),
            ref: ''+Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
            metadata: {
                custom_fields: [
                    {
                        display_name:$('#contact_name').val(),
                        phone: $('#contact_phone').val(),

                    }
                ]
            },
            callback: function(response){
                console.log(response);
                $('#transaction_ref').val(response.reference);
                $('#status').val(response.status);

                document.cookie = "status = " + status;
                $("#paystack_message").show();
                $("#hold_message").hide();
                document.getElementById("submitter").submit();

                //window.location = "http://localhost:8000/payment_success/" + response.reference;


            }
        });
        handler.openIframe();
    }
</script>
   @endsection('scripts');

