@extends('layouts.app')

@section('content')


@include('bookings.includes.steps')


    <div class="gap"></div>

    <div class="container">
            <div class="row">
                <div class="col-md-8">

                    <form action="" method="post" id="submitter">
                        {{ csrf_field() }}


                    <h4>Choose {{ $psg + $children }} seat(s)</h4>
                    <ul class="list booking-item-passengers">
                        <li>
                            <div class="seat-main">
                                <div class="seats">

                                </div>
                                <!--enable submit button when a seat is selected -->
                                <script type="text/javascript">
                                      $('.seats').click(function(e){
                                        $('#submitButtonBookNow').removeAttr("disabled", true); //disable the submit button by default
                                        $('#warningText').hide();
                                      });
                                </script>


                                <div class="seat-legend-main">
                                    <ul class="seat-legend">
                                        <li class="seat-legend-item"><span>Avalailable seat</span> <img src="{{ asset('img/available_seat_img.gif') }}"></li>
                                        <li class="seat-legend-item"><span>Booked seat</span> <img src="{{ asset('img/booked_seat_img.gif') }}"></li>
                                        <li class="seat-legend-item"><span>Selected seat</span> <img src="{{ asset('img/selected_seat_img.gif') }}"></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <div class="gap gap-small"></div>


                    <h4>Customer Details</h4>
                        <div class="row">
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input class="form-control" required value="@if($user){{$user->phone}}@endif" type="text" id="contact_phone" name="contact_phone" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> Your Full Name</label>
                                    <input class="form-control" required value="@if($user){{$user->name}}@endif" id="contact_name" type="text" name="contact_name" />
                                </div>
                            </div>
                           <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" value="@if($user){{$user->email}}@endif" id="contact_email" type="text" name="contact_email" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                        <label>Gender</label>
                                        <select name="gender" class="form-control" required id="contact_gender">
                                            <option value="">Select Your Gender</option>
                                            <option value="Female">Female</option>
                                            <option value="Male">Male</option>
                                        </select>

                                    </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Next of kin</label>
                                    <input class="form-control" required type="text" name="kin_name"  id ="kin_name"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Next of kin's Phone</label>
                                    <input class="form-control" required type="text" id="kin_phone" name="kin_phone" />
                                </div>
                            </div>

                        </div>
                        @if(!Auth::check())
                        <div class="row hide-if-reg">
                            <div class="col-md-12">
                                 <div class="checkbox">
                                <label>
                                     <input  id="loyalty-check" type="checkbox" name="loyalty" />Join Loyalty Program
                                </label>
                                </div>
                            </div>
                        </div>
                        <div class="row hide-if-reg">
                            <div class="col-md-6" >
                                <div class="form-group" id="loyaltyx"></div>
                            </div>
                            <div class="col-md-6" >
                                <div class="form-group" id="confirmpasswordx"></div>
                            </div>
                        </div>
                        @else
                        <span class="label label-info">Welcome, {{$user->name}}</span>
                        @endif

                    <!-- </form> -->
                    <div class="gap gap-small"></div>

                    @if($psg > 1)
                    <h3>Other Passengers</h3>
                    <ul class="list booking-item-passengers" id="passengers-section">
                        @if($psg > 1)

                        <?php for ($i=1; $i < $psg; $i++):?>
                        <li class="other-passenger">
                            <div class="row">
                                <div class="col-md-3">
                                   <label><i class="fa fa-close remove-icon"></i> <span>Passenger</span> <?php echo $i + 1; ?></label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input name="passengers<?php echo $i; ?>" class="form-control" type="text" required="" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <select name="p_gender<?php echo $i; ?>" class="form-control" required>
                                            <option value="">Select Your Gender</option>
                                            <option value="Female">Female</option>
                                            <option value="Male">Male</option>
                                        </select>

                                    </div>
                                </div>
                            </div>
                        </li>

                    	<?php endfor ?>
                    @endif


                    </ul>
                    @endif

                    <!-- <a class="btn btn-danger btn-xs" id="addPassenger" href="#" ><i class="fa fa-plus"></i>Add Other Passengers</a> -->
                    <div class="gap gap-small"></div>

                    @if($children > 0)
                    <h3>Children</h3>
                    <ul class="list booking-item-passengers" id="infants-section">

                        <?php for ($i=1; $i <= $children; $i++):?>

                        <li>
                            <div class="row">
                                <div class="col-md-2">
                                   <label><i class="fa fa-close remove-infant" id="remove-infant<?php echo $i; ?>"></i><span> Child <?php echo $i; ?></span></label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input id="infantname<?php echo $i;?>" name="child<?php echo $i; ?>" class="form-control" type="text" required="" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <select name="child_gender<?php echo $i; ?>" class="form-control" required>
                                            <option value="">-Choose-</option>
                                            <option value="Female">Female</option>
                                            <option value="Male">Male</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Age</label>
                                        <!-- <input id="<?php echo $i;?>" class="form-control infantHandler" name="i_child<?php echo $i; ?>" type="text"  required="" min="4" max="11" /> -->
                                         <select  id="<?php echo $i;?>"  name="child_age<?php echo $i; ?>" class="form-control" required>
                                             <option value="">-Choose-</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <?php endfor ?>

                    </ul>
                    @endif

                     @if($infants > 0)
                    <h3>Infant(s)</h3>
                    <ul class="list booking-item-passengers" id="infants-section">

                        <?php for ($j= $i; $j < $infants + $i; $j++):?>

                        <li>
                            <div class="row">
                                <div class="col-md-2">
                                   <label><i class="fa fa-close remove-infant" id="remove-infant<?php echo $j; ?>"></i><span> Infant <?php echo $j - $i; ?></span></label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input id="infantname<?php echo $j;?>" name="infants<?php echo $j; ?>" class="form-control" type="text" required="" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <select name="i_gender<?php echo $j; ?>" class="form-control" required>
                                            <option value="">-Choose-</option>
                                            <option value="Female">Female</option>
                                            <option value="Male">Male</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Age</label>
                                        <!-- <input id="<?php echo $j; ?>" class="form-control infantHandler" name="i_age<?php echo $j; ?>" type="text" required="" min="1" max="4" /> -->
                                         <select  id="<?php echo $j;?>"  name="i_age<?php echo $j; ?>" class="form-control" required>
                                           <option value="">-Choose-</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <?php endfor ?>


                    </ul>
                    @endif

                    <!-- <a class="btn btn-danger btn-xs" id="addInfant" href="#">
                        <i class="fa fa-plus"></i>Add Other Children
                    </a> -->




                    <div class="gap gap-small"></div>
                    <hr />

                    <div class="checkbox checkbox-small">
                        <label class="">
                            <div class="i-check checked"><input class="i-check" checked="" disabled="" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>By clicking book, it mean you accept bus.com.ng <a href="#" target="_blank">terms and conditions</a> and {{ $trip->operator->name }}'s terms (showing on the right) .</label>
                    </div>

                    <div id="hidden-f">
                        <input type="hidden" name="departure_date" value="{{$date}}">
                        <input type="hidden" name="return_date" value="{{$return_date}}">
                        <input type="hidden" name="infant_count" id="i_count" value="{{ $infants }}">
                        <input type="hidden" name="children_count" id="i_count" value="{{ $children }}">
                        <input type="hidden" name="passenger_count" id="p_count" value="{{ $psg }}">
                        <div id="warningText" class="warning" style="color: red;">You have not selected a seat above. </div>
                        <input class="btn btn-primary" type="submit" value="Book now" disabled id="submitButtonBookNow" />
                    </div>

                 </form>
                 <div class="gap gap-small"></div>


                    <!-- <div class="row">
                        <div class="col-md-6">
                            <img class="pp-img" src="img/paypal.png" alt="Image Alternative text" title="Image Title" />
                            <p>Important: You will be redirected to PayPal's website to securely complete your payment.</p><a class="btn btn-primary">Checkout via Paypal</a>
                        </div>
                        <div class="col-md-6">
                                <div class="clearfix">
                                    <div class="form-group form-group-cc-number">
                                        <label>Card Number</label>
                                        <input class="form-control" placeholder="xxxx xxxx xxxx xxxx" type="text" /><span class="cc-card-icon"></span>
                                    </div>
                                    <div class="form-group form-group-cc-cvc">
                                        <label>CVC</label>
                                        <input class="form-control" placeholder="xxxx" type="text" />
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="form-group form-group-cc-name">
                                        <label>Cardholder Name</label>
                                        <input class="form-control" type="text" />
                                    </div>
                                    <div class="form-group form-group-cc-date">
                                        <label>Valid Thru</label>
                                        <input class="form-control" placeholder="mm/yy" type="text" />
                                    </div>
                                </div>
                                <div class="checkbox checkbox-small">
                                    <label>
                                        <input class="i-check" type="checkbox" checked/>Add to My Cards</label>
                                </div>

                        </div>
                    </div> -->
                </div>
                <div class="col-md-4">
                    @include('bookings.includes.summary')

                    <br/>
                    <hr/>
                    <br/>

                    <div class="booking-item-payment">
                        <header class="clearfix">
                            <h5 class="mb0">{{ $trip->operator->name }}'s Terms  </h5>
                        </header>
                        <ul class="booking-item-payment-details">
                            {!! $trip->operator->local_terms !!}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="gap"></div>
        </div>


@stop
