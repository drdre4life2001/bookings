@extends('layouts.app')

@section('content')
        @include('bookings.includes.steps')
        <div class="gap"></div>
<div class="container">
            <?php

                $parent_booking_cost = isset($parent_booking->final_cost)? $parent_booking->final_cost * 100: 0;
                $parent_booking_id = isset($parent_booking->id)? $parent_booking->id: "";
                $parent_booking_code = isset($parent_booking->booking_code)? $parent_booking->booking_code: "";
            ?>
                <script type="text/javascript">
                    window.onbeforeunload = function() {
                        return "Dude, are you sure you want to leave? Think of the kittens!";
                    }
                </script>


            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    @if($request->booktype == "onhold")

                        <div class="round box-icon-large box-icon-center box-icon-success mb30">
                            <i class="fa fa-check"></i>
                        </div>
                        <h2 class="text-center">Your Booking was successful and it is on Hold for 5 hours!</h2>
                        <h5 class="text-center mb30">Booking details has been sent to your phone number [{{ $booking->booking->contact_phone }}] and email address [{{ $booking->booking->contact_email }}]</h5>
                        <ul class="order-payment-list list mb30">
                            <li>
                                <div class="row">
                                    <p>Please note that Booking on hold means your booking is reserved for 5 hours while we await your payment. If payment is not received by then, you will forfiet your booking and your booking code will be invalid.</p>
                                </div>
                            </li>

                        </ul>
                    @else
                        <div class="round box-icon-large box-icon-center box-icon-success mb30">
                            <i class="fa fa-check"></i>
                        </div>
                        <h2 class="text-center">Your payment was successful!</h2>
                        <h5 class="text-center mb30">Booking details has been sent to your phone number [{{ $booking->booking->contact_name }}] and email address [{{ $booking->booking->contact_email }}]</h5>

                        <ul class="order-payment-list list mb30">
                            <li>
                                <div class="row text-center">
                                    <p>Safe Journey!</p>
                                </div>
                            </li>

                        </ul>

                    @endif

                    
                    @include('bookings.includes.summary')



                </div>
            </div>
            <div class="gap"></div>
        </div>


@stop