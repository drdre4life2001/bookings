<h4 style="color: red">Passport Details</h4>
    <div class="col-md-6">
        <div class="form-group">
            <label>Passport Type</label>
            <input type="text" value="{{ $p_type }}" name="passport_type<?php echo !empty($i)?$i:'';?>"
                   readonly="readonly"
                   class="form-control"
                   required="required">
        </div>
        <div class="col-md-6">
            <div class="form-group">
            <label>Passport Number</label>
            <input type="text" name="passport_number<?php echo !empty($i)?$i:'';?>" class="form-control"
                   required="required">
        </div>
            </div>
        <div class="col-md-8">
            <div class="form-group">
            <label>Date of Issue</label>
            <input type="date" name="passport_date_of_issue<?php echo !empty($i)?$i:'';?>" class="form-control"
                   required="required">
        </div></div>
        <div class="col-md-8">
            <div class="form-group">
            <label>Expiry Date</label>
            <input type="date" name="passport_expiry_date<?php echo !empty($i)?$i:'';?>" class="form-control"
                   required="required">
        </div></div>
        <div class="col-md-6">
            <div class="form-group">
            <label>Place of Issue</label>
            <input type="text" name="passport_place_of_issue<?php echo !empty($i)?$i:'';?>" class="form-control"
                   required="required">
        </div></div>
    </div>
