<style type="text/css">

.mt-element-step .step-line .mt-step-col {
  padding-top: 30px;
  padding-bottom: 30px;
  text-align: center; }

.mt-element-step .step-line .mt-step-number {
  font-size: 26px;
  border-radius: 50% !important;
  display: inline-block;
  margin: auto;
  padding: 9px;
  margin-bottom: 5px;
  border: 3px solid;
  border-color: #e5e5e5;
  position: relative;
  z-index: 5;
  height: 60px;
  width: 60px;
  text-align: center; }
  .mt-element-step .step-line .mt-step-number > i {
    position: relative;
    top: 50%;
    transform: translateY(-120%); }

.mt-element-step .step-line .mt-step-title {
  font-size: 20px;
  font-weight: 400; }
  .mt-element-step .step-line .mt-step-title:after {
    content: '';
    height: 3px;
    width: 50%;
    position: absolute;
    background-color: #e5e5e5;
    top: 53px;
    left: 50%;
    z-index: 4; }
  .mt-element-step .step-line .mt-step-title:before {
    content: '';
    height: 3px;
    width: 50%;
    position: absolute;
    background-color: #e5e5e5;
    top: 53px;
    right: 50%;
    z-index: 4; }

.mt-element-step .step-line .first .mt-step-title:before {
  content: none; }

.mt-element-step .step-line .last .mt-step-title:after {
  content: none; }

.mt-element-step .step-line .active .mt-step-number {
  color: #32c5d2 !important;
  border-color: #32c5d2 !important; }

.mt-element-step .step-line .active .mt-step-title,
.mt-element-step .step-line .active .mt-step-content {
  color: #32c5d2 !important; }

.mt-element-step .step-line .active .mt-step-title:after,
.mt-element-step .step-line .active .mt-step-title:before {
  background-color: #32c5d2; }

.mt-element-step .step-line .done .mt-step-number {
  color: #26C281 !important;
  border-color: #26C281 !important; }

.mt-element-step .step-line .done .mt-step-title,
.mt-element-step .step-line .done .mt-step-content {
  color: #26C281 !important; }

.mt-element-step .step-line .done .mt-step-title:after,
.mt-element-step .step-line .done .mt-step-title:before {
  background-color: #26C281; }

.mt-element-step .step-line .error .mt-step-number {
  color: #E7505A !important;
  border-color: #E7505A !important; }

.mt-element-step .step-line .error .mt-step-title,
.mt-element-step .step-line .error .mt-step-content {
  color: #E7505A !important; }

.mt-element-step .step-line .error .mt-step-title:after,
.mt-element-step .step-line .error .mt-step-title:before {
  background-color: #E7505A; }


  .bg-hover-white:hover, .bg-white {
    background: #fff none repeat scroll 0 0 !important;
}

.mt-element-step .step-line .mt-step-number > i {
    position: relative;
    top: 50%;
    transform: translateY(-120%);
}
[class*=" fa-"]:not(.fa-stack), [class*=" glyphicon-"], [class*=" icon-"], [class^="fa-"]:not(.fa-stack), [class^="glyphicon-"], [class^="icon-"] {
    display: inline-block;
    line-height: 14px;
}


</style>

	 <div class="container">
<div class="mt-element-step">
     <div class="row step-line">
        <div class="col-md-3 mt-step-col col-sm-3 hidden-xs first @if($step > 1) done @endif @if($step == 1) error @endif">
          @if($step > 1)
          <a href="#" onclick="window.history.go({{ 1 - $step }});">
          @endif
            <div class="mt-step-number bg-white">
                <i class="fa fa-car"></i>
            </div>

          @if($step > 1)
          </a>
          @endif

            <div class="mt-step-title uppercase font-grey-cascade">Bus Selection</div>
            <!-- <div class="mt-step-content font-grey-cascade">Select your Trip from the results</div> -->
        </div>
        <div class="col-md-3 mt-step-col col-sm-3 hidden-xs @if($step > 2) done @endif @if($step == 2) error @endif">

          @if($step > 2)
          <a href="#" onclick="window.history.go({{ 2 - $step }});">
          @endif

            <div class="mt-step-number bg-white">
                <i class="fa fa-user"></i>
            </div>

          @if($step > 2)
          </a>
          @endif

            <div class="mt-step-title uppercase font-grey-cascade">Traveler's Information</div>
            <!-- <div class="mt-step-content font-grey-cascade">Fill in your booking details</div> -->
        </div>
        <div class="col-md-3 mt-step-col col-sm-3 hidden-xs @if($step > 3) done @endif @if($step == 3) error @endif">

          @if($step > 3)
          <a href="#" onclick="window.history.go({{ 2 - $step }});">
          @endif
            <div class="mt-step-number bg-white">
                <i class="fa fa-credit-card"></i>
            </div>

          @if($step > 3)
          </a>
          @endif
            <div class="mt-step-title uppercase font-grey-cascade">Payment</div>
            <!-- <div class="mt-step-content font-grey-cascade">Pay for the booking</div> -->
        </div>
        <div class="col-md-3 mt-step-col col-sm-3 hidden-xs last @if($step > 4) done @endif @if($step == 4) error @endif">
            <div class="mt-step-number bg-white">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="mt-step-title uppercase font-grey-cascade">Confirmation</div>
            <!-- <div class="mt-step-content font-grey-cascade">Your booking confirmation</div> -->
        </div>
    </div>
</div>


</div>