<div class="booking-item-payment">
                        <header class="clearfix">
                            @if(isset($booking_code))
                                <a class="booking-item-payment-img">
                                    <img title="hotel 1" alt="Image Alternative text" style="width:100px;"  src="{{ config('api.api-host').'logos/'.$booking->trip->operator->img }}">
                                </a>
                                <h5 class="booking-item-payment-title">Booking Code: </h5>
                                <h4><strong>{{ $booking->booking_code }}</strong></h4>
                            @else
                                <h5 class="mb0">{{ $trip->sourcepark->name }} <i class="fa fa-arrow-right booking-item-payment-date-separator"></i> {{ $trip->destpark->name }}</h5>

                            @endif    
                            
                        </header>
                        <ul class="booking-item-payment-details">
                            <li>
                                <h5>Travel Details</h5>
                                <div class="booking-item-payment-flight">
                                    <div class="row">
                                       <div class="col-md-12">
                                            <div class="booking-item-flight-details">
                                                <div class="booking-item-departure"><i class="fa fa-bus"></i>
                                                    <h5>Leaving from</h5>
                                                    <p class="booking-item-destination">{{ $trip->sourcepark->name }}</p>
                                                </div>
                                                <div class="booking-item-arrival"><i class="fa fa-bus"></i>
                                                    <h5>Going to</h5>
                                                    <p class="booking-item-destination">{{ $trip->destpark->name }}</p>
                                                </div>
                                            </div>
                                        </div>
                                      	 <div class="col-md-12">
                                            <div class="booking-item-flight-duration">
                                                
                                                <p>Departure Date:
                                                <b><?php echo date('D, M dS', strtotime($date)); ?></b>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="booking-item-flight-duration">
                                                <p>Departure time:
                                                <b>{{ date('H:iA', strtotime($trip->departure_time)) }}</b></p>
                                            </div>
                                        </div>

                                        <br style="clear:both" />
                                        <hr/>
                                        

                                        @if(!empty($return_trip))

                                        <div class="col-md-12">
                                            <div class="booking-item-flight-details">
                                                <div class="booking-item-departure"><i class="fa fa-bus"></i>
                                                    <h5>Returning from</h5>
                                                    <p class="booking-item-destination">{{ $return_trip->sourcepark->name }}</p>
                                                </div>
                                                <div class="booking-item-arrival"><i class="fa fa-bus"></i>
                                                    <h5>Arriving at</h5>
                                                    <p class="booking-item-destination">{{ $return_trip->destpark->name }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <br><div class="col-md-12">
                                            <div class="booking-item-flight-duration">
                                               <p>Return date:
                                                <b><?php echo date('D, M dS', strtotime($return_date)); ?></b>
                                            </div>
                                        </div>
                                        @endif


                                         <p>
                                        <span>&nbsp;&nbsp;&nbsp;&nbsp;Bus type: <b><?php echo $trip->bus_type->name ?></b> - {{ $trip->bus_type->no_of_seats }} seater</span>
                                        <br>&nbsp;&nbsp;&nbsp;&nbsp;
                                        @if($trip->ac)
                                                    <span<i class="fa fa-check-square-o font-green-jungle"></i>  AC</span>
                                                    @else
                                                    <span><i class="fa fa-times-circle font-red-thunderbird"></i>  AC</span>
                                                    @endif

                                                    &nbsp;&middot;&nbsp;

                                                    @if($trip->security)
                                                        <span><i class="fa fa-check-square-o font-green-jungle"></i>  Security</span>
                                                    @else
                                                    <span ><i class="fa fa-times-circle font-red-thunderbird"></i>  Security</span>
                                                    @endif

                                                    <!-- &nbsp;&middot;&nbsp;

                                                    @if($trip->insurance)
                                                    <span ><i class="fa fa-check-square-o font-green-jungle"></i>  Insurance</span>
                                                    @else
                                                    <span><i class="fa fa-times-circle font-red-thunderbird"></i>  Insurance</span>
                                                    @endif

                                                    &nbsp;&middot;&nbsp;

                                                    <br/>
                                                    @if($trip->tv)
                                                    <span><i class="fa fa-check-square-o font-green-jungle"></i>  TV</span>
                                                    @else
                                                    <span><i class="fa fa-times-circle font-red-thunderbird"></i>  TV</span>
                                                    @endif

                                                    &nbsp;&middot;&nbsp;

                                                    @if($trip->passport)
                                                    <span><i class="fa fa-check-square-o font-green-jungle"></i>  Passport</span>
                                                    @else
                                                    <span><i class="fa fa-times-circle font-red-thunderbird"></i>  Passport</span>
                                                    @endif -->



                                </div>
                            </li>
                            <li>
                                <h5>({{ $psg }} @if($psg == 1) Adult @else Adults @endif, {{$children}}@if($children == 1) Child @else Children @endif, {{$infants}}@if($infants == 1) Infant @else Infants @endif)</h5>
                                <ul class="booking-item-payment-price">
                                    <li>
                                        <p class="booking-item-payment-price-title"> {{ $psg }} @if($psg == 1) Adult @else Adults @endif</p>
                                        <p class="booking-item-payment-price-amount">&#x20A6; <?php echo number_format( $trip->fare ) ?><small>/per adult</small>
                                        </p>
                                        
                                    </li>
                                    @if($children > 0)
                                    <li>
                                        <p class="booking-item-payment-price-title"> {{ $children }} @if($children == 1) Child @else Child @endif</p>
                                        <p class="booking-item-payment-price-amount">&#x20A6; <?php echo number_format( ( (1 - ($trip->operator->local_children_discount/ 100)) * $trip->fare)  ) ?><small>/per child</small>
                                        </p>
                                        
                                    </li>
                                    @endif
                                    @if($infants > 0)
                                    <li>
                                        <p class="booking-item-payment-price-title"> {{ $infants }} @if($infants == 1) Infant @else Infants @endif</p>
                                        <p class="booking-item-payment-price-amount">&#x20A6; <?php echo number_format( 0 ) ?><small>/per infant</small>
                                        </p>
                                        
                                    </li>
                                    @endif
                                    <li>
                                        <p class="booking-item-payment-price-title"> Convenience fee</p>
                                        <p class="booking-item-payment-price-amount">&#x20A6; <?php echo number_format( config('custom.convenience_fee') ) ?>
                                        </p>
                                        
                                    </li>
                                   

                                </ul>
                            </li>
                        </ul>
                        <p class="booking-item-payment-total">Total trip: <span>&#x20A6; <?php echo number_format
                                ($final_cost); ?></span>
                        </p>
                    </div>