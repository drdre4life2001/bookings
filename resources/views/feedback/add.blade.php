@extends('layouts.app')

@section('content')
<div class="gap"></div>

<div class="container">

@if(Session::has('message'))
  <div class="alert alert-success" role="alert">
      {{Session::get('message')}}
  </div>
@endif
    <div class="row col-md-6 col-lg-6">
        <div class="col-md-12">
          <h2> Send us a feedback </h2>
            <form method="post" action="{{ url('feedback/postAdd') }}">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                          <label for="email">Email </label>
                            <input name="email" required="required" class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                          <label for="name">Full name </label>
                            <input name="name" required="required" class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                          <label for="operator">Operator </label>
                            <input name="operator" required="required" class="form-control"/>
                        </div>
                    </div>
                </div>

              <!--  <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Rating</label>
                            <div class="containerx">
                                <input type="radio" name="rating" class="rating" value="1" />
                                <input type="radio" name="rating" class="rating" value="2" />
                                <input type="radio" name="rating" class="rating" value="3" />
                                <input type="radio" name="rating" class="rating" value="4" />
                                <input type="radio" name="rating" class="rating" value="5" />
                            </div>

                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                          <label for="feedback">Message </label>
                            <textarea name="feedback" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Submit Feedback"/>
                </div>
              </div>
            </div>
            </form>
        </div>
        </div>
    </div>
@stop
