@extends('layouts.app')

@section('content')

<div class="gap"></div>


        <div class="container">
            @if (Session::has('success'))
                <div class="alert alert-success">{{ Session::get('success') }}</div>
            @elseif (Session::has('error'))
                <div class="alert alert-warning">{{ Session::get('error') }}</div>
            @endif
            <div class="row" data-gutter="60">
                <div class="col-md-6">
                    <h3>Welcome to Bus.com.ng</h3>

                    <h4 class="tag-line">Book smarter, earn rewards.</h4>
                </div>
                <div class="col-md-6">
                    <h3>Login</h3>
                    <form method="post" action="sign-in">
                        {{ csrf_field() }}
                        <div class="form-group form-group-icon-left"><i class="fa fa-phone input-icon input-icon-show"></i>
                            <label>Phone No.</label>
                            <input class="form-control" name="phone" placeholder="e.g. 08123456789" type="text" required="required"/>
                        </div>
                        <div class="form-group form-group-icon-left"><i class="fa fa-lock input-icon input-icon-show"></i>
                            <label>Password</label>
                            <input class="form-control" name="password" type="password" placeholder="my secret password" required="required" />
                        </div>
                        <input class="btn btn-primary" type="submit" value="Sign in" />

                    </form>

                    <div class="row">
                      <!--  <div class="col-md-12">
                            <a href="/social/facebook" class="btn btn-sm btn-social btn-facebook"><span class="fa fa-facebook"></span> Login with Facebook</a>
                            <a href="/social/google" class="btn btn-sm btn-social btn-google"><span class="fa fa-google"></span> Login with Google</a>

                            {{-- <a href="/social/twitter" class="btn btn-sm btn-social btn-twitter"><span class="fa fa-twitter"></span> Login with Twitter</a> --}}
                        </div> -->

                    </div>
                </div>


            </div>

        </div>
@stop
