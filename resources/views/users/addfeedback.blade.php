@extends('layouts.app')

@section('content')
<div class="gap"></div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form method="post">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Rating</label>
                            <div class="containerx">
                                <input type="radio" name="rating" class="rating" value="1" />
                                <input type="radio" name="rating" class="rating" value="2" />
                                <input type="radio" name="rating" class="rating" value="3" />
                                <input type="radio" name="rating" class="rating" value="4" />
                                <input type="radio" name="rating" class="rating" value="5" />
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <textarea name="feedback" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Submit Feedback"/>
                </div>
              </div>
            </div>
            </form>
        </div>
        </div>
    </div>
@stop