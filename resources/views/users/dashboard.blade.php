@extends('layouts.app')

@section('content')
<div class="gap"></div>

<div class="container">
    <div class="row">

        <div class="col-md-12">
            <ul class="list list-inline user-profile-statictics mb30 hidden-md hidden-sm hidden-xs">
                <li><i class="fa fa-ticket user-profile-statictics-icon"></i>
                    <h5>{{$stats->paid}}</h5>
                    <p>Successful Bookings</p>
                </li>
                <li><i class="fa fa-ticket user-profile-statictics-icon"></i>
                    <h5>{{$stats->pending}}</h5>
                    <p>Pending Bookings</p>
                </li>
                <li><i class="fa fa-ticket user-profile-statictics-icon"></i>
                    <h5>{{$stats->cancelled}}</h5>
                    <p>Cancelled Bookings</p>
                </li>
                <li><i class="fa fa-bus user-profile-statictics-icon"></i>
                    <h5>{{$stats->total}}</h5>
                    <p>Total Trips</p>
                </li>

            </ul>
            <div class="row visible-xs visible-sm hidden-lg">
                <div class="col-sm-6 col-xs-6 col-md-6">
                    <p class="text-center"><i class="fa fa-ticket user-profile-statictics-icon"></i></p>
                    <h5 class="text-center">{{$stats->paid}}</h5>
                    <p class="text-center">Successful Bookings</p>
                </div>
                <div class="col-sm-6 col-xs-6 col-md-6">
                    <p class="text-center"><i class="fa fa-ticket user-profile-statictics-icon"></i></p>
                    <h5 class="text-center">{{$stats->pending}}</h5>
                    <p class="text-center">Pending Bookings</p>
                </div>
                <div class="col-sm-6 col-xs-6 col-md-6">
                    <p class="text-center"><i class="fa fa-ticket user-profile-statictics-icon"></i></p>
                    <h5 class="text-center">{{$stats->cancelled}}</h5>
                    <p class="text-center">Cancelled Bookings</p>
                </div>
                <div class="col-sm-6 col-xs-6 col-md-6">
                    <p class="text-center"><i class="fa fa-bus user-profile-statictics-icon"></i></p>
                    <h5 class="text-center">{{$stats->total}}</h5>
                    <p class="text-center">Total Trips</p>
                </div>
            </div>
            <div class="tabbable">
                <ul id="bookingTabs"class="nav nav-tabs nav-justified">
                    <li class="active"><a href = "#all" data-toggle = "tab">All Bookings</a></li>
                    <li><a href="#paid" data-toggle = "tab">Paid Bookings</a></li>
                    <li><a href="#onhold" data-toggle = "tab">Bookings on Hold</a></li>
                    <li><a href="#cancelled" data-toggle = "tab">Cancelled Bookings</a></li>
                </ul>
                <div id = "bookingTabsContent" class = "tab-content">
                    <div class = "tab-pane fade in active" id = "all">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-booking-history">
                                <thead>
                                    <tr>
                                        <th>Trip</th>
                                        <th>Booking Code</th>
                                        {{-- <th>Status</th> --}}
                                        <th> Date</th>
                                        <th>Total Cost</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(isset($bookings))
                                @foreach ($bookings->results as $booking)
                                    <tr>
                                        <td class="booking-history-type">
                                            <i class="fa fa-bus"></i>
                                            {{$booking->trip->name}}
                                        </td>
                                        <td class="booking-history-title">{{$booking->booking_code}}</td>
                                        <td>{{$booking->status}}</td>
                                        <td>{{$booking->date}}</td>
                                        <td>{{$booking->final_cost}}</td>

                                        <td class="text-center">
                                            <a class="btn btn-success btn-xs" href="#" data-toggle="modal" data-target="#feedbackModal{{$booking->id}}"><i class="fa fa-volume-up"></i>Give Feedback</a>
                                            <a class="btn btn-info btn-xs" href="#" data-toggle="modal" data-target="#bookingModal{{$booking->id}}"><i class="fa fa-eye"></i>View</a>

                                            <?php
                                                $booking_date = \Carbon\Carbon::createFromFormat('Y-m-d', $booking->date);
                                                $now = \Carbon\Carbon::now();

                                                $diff = $now->diffInHours($booking_date, false);

                                            ?>
                                            @if($booking->status != "PAID" && $diff > 24)
                                            <a class="btn btn-danger btn-xs pay" href="#" data-toggle="modal" data-payload="{{$booking->id}},{{$booking->booking_code}},{{$booking->final_cost}}"><i class="fa fa-money"></i> Pay</a>
                                            @endif

                                            <a class="btn btn-warning btn-xs" href="#" data-toggle="modal" data-target="#rebookModal{{$booking->id}}"><i class="fa fa-rotate-right"></i>Rebook </a>
                                        </td>
                                    </tr>
                                    <!-- Modal -->
                                    <div id="feedbackModal{{$booking->id}}" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Give Feedback</h4>
                                          </div>

                                          <div class="modal-body">
                                            <form method="post" action="save_customer_feedback">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="operator_id" value="{{$booking->trip->operator_id}}">
                                                <input type="hidden" name="booking_id" value="{{$booking->id}}">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Rating</label>

                                                            <div class="containerx">
                                                                <input type="radio" name="rating" class="rating" value="1" />
                                                                <input type="radio" name="rating" class="rating" value="2" />
                                                                <input type="radio" name="rating" class="rating" value="3" />
                                                                <input type="radio" name="rating" class="rating" value="4" />
                                                                <input type="radio" name="rating" class="rating" value="5" />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <textarea name="feedback" class="form-control" rows="5"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" class="btn btn-success" value="Submit Feedback"/>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            </form>
                                        </div>

                                      </div>
                                    </div>

                                    <div id="bookingModal{{$booking->id}}" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Booking Details</h4>
                                          </div>

                                          <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6"><b>Trip Name</b></div>
                                                    <div class="col-md-6">{{$booking->trip->name}}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6"><b>Booking Code</b></div>
                                                    <div class="col-md-6">
                                                        {{$booking->booking_code}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6"><b>Passenger</b></div>
                                                    <div class="col-md-6">
                                                        {{$booking->contact_name}}
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>

                                      </div>
                                    </div>

                                    <div id="rebookModal{{$booking->id}}" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Re-book trip</h4>
                                        </div>

                                        <div class="modal-body">
                                            <form class="form-inline" method="post" action="/rebook">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="booking_code" value="{{$booking->booking_code}}">

                                                <div class="form-group form-group-lg form-group-icon-left form-group-filled"><i class="fa fa-calendar input-icon input-icon-highlight" aria-hidden="true"></i>
                                                    <label>Departure date</label>
                                                    <input class="date-pick form-control" data-date-format="M d, D" type="text" name="departing">
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" value="Rebook">
                                                </div>
                                            </form>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>

                                      </div>
                                    </div>

                                @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="pagination">
                                @if(isset($all_paginate['pages']))
                                    @foreach($all_paginate['pages'] as $page)
                                        <li <?php if(isset($page['active']) && $page['active'] == true):?> class="active"<?php endif; ?>>
                                            <a href="{{$page['url']}}">{{$page['page']}}</a>
                                        </li>
                                    @endforeach
                                @endif
                                    <?php if(isset($all_paginate['next_page_link'])):?>
                                        <li class="next">
                                            <a href="{{$all_paginate['next_page_link']}}">Next Page</a>
                                        </li>
                                    <?php endif;?>
                                </ul>
                            </div>
                        </div>
                        <div class="gap"></div>
                    </div>
                    <div class = "tab-pane fade" id = "paid">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-booking-history">
                                <thead>
                                    <tr>
                                        <th>Trip</th>
                                        <th>Booking Code</th>
                                        <th>Status</th>
                                        <th> Date</th>
                                        <th>Total Cost</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(isset($paid_bookings))
                                @foreach ($paid_bookings->results as $booking)
                                    <tr>
                                        <td class="booking-history-type">
                                            <i class="fa fa-bus"></i>
                                            {{$booking->trip->name}}
                                        </td>
                                        <td class="booking-history-title">{{$booking->booking_code}}</td>
                                        <td>{{$booking->status}}</td>
                                        <td>{{$booking->date}}</td>
                                        <td>{{$booking->final_cost}}</td>
                                        <td class="text-center">
                                            <a class="btn btn-success btn-xs" href="#" data-toggle="modal" data-target="#paidfeedbackModal{{$booking->id}}"><i class="fa fa-volume-up"></i>Give Feedback</a>
                                            <a class="btn btn-info btn-xs" href="#" data-toggle="modal" data-target="#bookingModal{{$booking->id}}"><i class="fa fa-eye"></i>View</a>
                                            @if($booking->status != "PAID")
                                            <a class="btn btn-danger btn-xs pay" href="#" data-toggle="modal" data-payload="{{$booking->id}},{{$booking->booking_code}},{{$booking->final_cost}}"><i class="fa fa-money"></i>Pay</a>
                                            @endif
                                            <a class="btn btn-warning btn-xs" href="#" data-toggle="modal" data-target="#rebookModal{{$booking->id}}"><i class="fa fa-rotate-right"></i>Rebook </a>
                                        </td>
                                    </tr>
                                    <!-- Modal -->
                                    <div id="paidfeedbackModal{{$booking->id}}" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Give Feedback</h4>
                                          </div>

                                          <div class="modal-body">
                                            <form method="post" action="save_customer_feedback">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="operator_id" value="">
                                                <input type="hidden" name="booking_id" value="{{$booking->id}}">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Rating</label>

                                                            <div class="containerx">
                                                                <input type="radio" name="rating" class="rating" value="1" />
                                                                <input type="radio" name="rating" class="rating" value="2" />
                                                                <input type="radio" name="rating" class="rating" value="3" />
                                                                <input type="radio" name="rating" class="rating" value="4" />
                                                                <input type="radio" name="rating" class="rating" value="5" />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <textarea name="feedback" class="form-control" rows="5"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" class="btn btn-success" value="Submit Feedback"/>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            </form>
                                        </div>

                                      </div>
                                    </div>

                                     <div id="bookingModal{{$booking->id}}" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Booking Details</h4>
                                          </div>

                                          <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6"><b>Trip Name</b></div>
                                                    <div class="col-md-6">{{$booking->trip->name}}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6"><b>Booking Code</b></div>
                                                    <div class="col-md-6">
                                                        {{$booking->booking_code}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6"><b>Passenger</b></div>
                                                    <div class="col-md-6">
                                                        {{$booking->contact_name}}
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>

                                      </div>
                                    </div>

                                @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="pagination">
                                @if(isset($paid_paginate['pages']))
                                    @foreach($paid_paginate['pages'] as $page)
                                        <li <?php if(isset($page['active']) && $page['active'] == true):?> class="active"<?php endif; ?>>
                                            <a href="{{$page['url']}}">{{$page['page']}}</a>
                                        </li>
                                    @endforeach
                                @endif
                                    <?php if(isset($paid_paginate['next_page_link'])):?>
                                        <li class="next">
                                            <a href="{{$paid_paginate['next_page_link']}}">Next Page</a>
                                        </li>
                                    <?php endif;?>
                                </ul>
                            </div>

                        </div>
                        {{-- <div class="gap"></div> --}}
                    </div>
                    <div class = "tab-pane fade" id = "onhold">
                        <div class="table-responsive">
                        <table class="table table-bordered table-striped table-booking-history">
                            <thead>
                                <tr>
                                    <th>Trip</th>
                                    <th>Booking Code</th>
                                    <th>Status</th>
                                    <th> Date</th>
                                    <th>Total Cost</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($pending_bookings))
                            @foreach ($pending_bookings->results as $booking)
                                <tr>
                                    <td class="booking-history-type">
                                        <i class="fa fa-bus"></i>
                                        {{$booking->trip->name}}
                                    </td>
                                    <td class="booking-history-title">{{$booking->booking_code}}</td>
                                    <td>{{$booking->status}}</td>
                                    <td>{{$booking->date}}</td>
                                    <td>{{$booking->final_cost}}</td>
                                    <td class="text-center">
                                        <a class="btn btn-success btn-xs" href="#" data-toggle="modal" data-target="#pending_feedbackModal{{$booking->id}}"><i class="fa fa-volume-up"></i>Give Feedback</a>
                                        <a class="btn btn-info btn-xs" href="#" data-toggle="modal" data-target="#bookingModal{{$booking->id}}"><i class="fa fa-eye"></i>View</a>
                                        @if($booking->status != "PAID")
                                        <a class="btn btn-danger btn-xs pay" href="#"  data-toggle="modal" data-payload="{{$booking->id}},{{$booking->booking_code}},{{$booking->final_cost}}"><i class="fa fa-money"></i>Pay</a>
                                        @endif
                                        <a class="btn btn-warning btn-xs" href="#" data-toggle="modal" data-target="#rebookModal{{$booking->id}}"><i class="fa fa-rotate-right"></i>Rebook </a>
                                    </td>
                                </tr>
                                <!-- Modal -->
                                <div id="pending_feedbackModal{{$booking->id}}" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Give Feedback</h4>
                                      </div>

                                      <div class="modal-body">
                                        <form method="post" action="save_customer_feedback">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="operator_id" value="">
                                            <input type="hidden" name="booking_id" value="{{$booking->id}}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Rating</label>

                                                        <div class="containerx">
                                                            <input type="radio" name="rating" class="rating" value="1" />
                                                            <input type="radio" name="rating" class="rating" value="2" />
                                                            <input type="radio" name="rating" class="rating" value="3" />
                                                            <input type="radio" name="rating" class="rating" value="4" />
                                                            <input type="radio" name="rating" class="rating" value="5" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="feedback" class="form-control" rows="5"></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <input type="submit" class="btn btn-success" value="Submit Feedback"/>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                        </form>
                                    </div>

                                  </div>
                                </div>

                                 <div id="bookingModal{{$booking->id}}" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Booking Details</h4>
                                      </div>

                                      <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-6"><b>Trip Name</b></div>
                                                <div class="col-md-6">{{$booking->trip->name}}</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6"><b>Booking Code</b></div>
                                                <div class="col-md-6">
                                                    {{$booking->booking_code}}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6"><b>Passenger</b></div>
                                                <div class="col-md-6">
                                                    {{$booking->contact_name}}
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                  </div>
                                </div>

                            @endforeach
                            @endif
                            </tbody>
                        </table>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="pagination">
                                    @if(isset($pending_paginate['pages']))
                                    @foreach($pending_paginate['pages'] as $page)
                                        <li <?php if(isset($page['active']) && $page['active'] == true):?> class="active"<?php endif; ?>>
                                            <a href="{{$page['url']}}">{{$page['page']}}</a>
                                        </li>
                                    @endforeach
                                    @endif
                                    <?php if(isset($pending_paginate['next_page_link'])):?>
                                        <li class="next">
                                            <a href="{{$pending_paginate['next_page_link']}}">Next Page</a>
                                        </li>
                                    <?php endif;?>
                                </ul>
                            </div>

                        </div>
                        <div class="gap"></div>
                    </div>
                    <div class = "tab-pane fade" id = "cancelled">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-booking-history">
                                <thead>
                                    <tr>
                                        <th>Trip</th>
                                        <th>Booking Code</th>
                                        <th>Status</th>
                                        <th> Date</th>
                                        <th>Total Cost</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(isset($cancelled_bookings))
                                    @foreach ($cancelled_bookings->results as $booking)
                                        <tr>
                                            <td class="booking-history-type">
                                                <i class="fa fa-bus"></i>
                                                {{$booking->trip->name}}
                                            </td>
                                            <td class="booking-history-title">{{$booking->booking_code}}</td>
                                            <td>{{$booking->status}}</td>
                                            <td>{{$booking->date}}</td>
                                            <td>{{$booking->final_cost}}</td>
                                            <td class="text-center">
                                                <a class="btn btn-success btn-xs" href="#" data-toggle="modal" data-target="#cfeedbackModal{{$booking->id}}"><i class="fa fa-volume-up"></i>Give Feedback</a>
                                                <a class="btn btn-info btn-xs" href="#" data-toggle="modal" data-target="#bookingModal{{$booking->id}}"><i class="fa fa-eye"></i>View</a>
                                                @if($booking->status != "PAID")
                                                <a class="btn btn-danger btn-xs pay"  data-toggle="modal" data-payload="{{$booking->id}},{{$booking->booking_code}},{{$booking->final_cost}}" href="#"><i class="fa fa-money"></i>Pay</a>
                                                @endif
                                                <a class="btn btn-warning btn-xs" href="#" data-toggle="modal" data-target="#rebookModal{{$booking->id}}"><i class="fa fa-rotate-right"></i>Rebook </a>
                                            </td>
                                        </tr>
                                        <!-- Modal -->
                                        <div id="cfeedbackModal{{$booking->id}}" class="modal fade" role="dialog">
                                          <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Give Feedback</h4>
                                              </div>

                                              <div class="modal-body">
                                                <form method="post" action="save_customer_feedback">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="operator_id" value="">
                                                    <input type="hidden" name="booking_id" value="{{$booking->id}}">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Rating</label>

                                                                <div class="containerx">
                                                                    <input type="radio" name="rating" class="rating" value="1" />
                                                                    <input type="radio" name="rating" class="rating" value="2" />
                                                                    <input type="radio" name="rating" class="rating" value="3" />
                                                                    <input type="radio" name="rating" class="rating" value="4" />
                                                                    <input type="radio" name="rating" class="rating" value="5" />
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <textarea name="feedback" class="form-control" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <input type="submit" class="btn btn-success" value="Submit Feedback"/>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                                </form>
                                            </div>

                                          </div>
                                        </div>

                                         <div id="bookingModal{{$booking->id}}" class="modal fade" role="dialog">
                                          <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Booking Details</h4>
                                              </div>

                                              <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6"><b>Trip Name</b></div>
                                                        <div class="col-md-6">{{$booking->trip->name}}</div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6"><b>Booking Code</b></div>
                                                        <div class="col-md-6">
                                                            {{$booking->booking_code}}
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6"><b>Passenger</b></div>
                                                        <div class="col-md-6">
                                                            {{$booking->contact_name}}
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                          </div>
                                        </div>

                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="pagination">
                                @if(isset($cancelled_paginate['pages']))
                                    @foreach($cancelled_paginate['pages'] as $page)
                                        <li @if(isset($page['active']) && $page['active'] == true) class="active"@endif>
                                            <a href="{{$page['url']}}">{{$page['page']}}</a>
                                        </li>
                                    @endforeach
                                @endif
                                    <?php if(isset($cancelled_paginate['next_page_link'])):?>
                                        <li class="next">
                                            <a href="{{$cancelled_paginate['next_page_link']}}">Next Page</a>
                                        </li>
                                    <?php endif;?>
                                </ul>
                            </div>

                        </div>
                        <div class="gap"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ URL::asset('/js/jquery.js') }}" > </script>

<script src="https://js.paystack.co/v1/inline.js"></script>
<script type="text/javascript">
    $(".pay").click(function(){
        var payload = $(this).data("payload").split(",");
        var trn_url = "{{ route('transactions') }}";
        console.log(payload);
        var data = ({booking_id: payload[0], booking_code: payload[1], status:'Failed', response:'Transaction Not Found', 'payment_method':'PayStack'});
        $.post(trn_url, data, loadPaystack(payload));

    function loadPaystack(payload){
            var handler = PaystackPop.setup({
                key: 'pk_test_7d7271a9f0ca45ac76d8ca1569ea47948e1bb5f5',
                email: '{{$customer_email}}',//check if this actually passed use logged in user email
                amount: (payload[2] * 100 + <?php echo env("CONVENIENCE_FEE") * 100;?>),
                ref: '<?php echo uniqid(true); ?>',
                callback: paystackcallback,

                onClose: function(){
                    var url = "{{ route('transactions') }}";
                    var data = ({booking_id: payload[0], booking_code: payload[1], status:'Cancelled', response:'Transaction cancelled by user', 'cancelled':'yes'});
                    $.post(url, data, function(response){});

                  }
            })
            handler.openIframe()
                }
     function paystackcallback(response){
            var url = "https://paystack.ng/charge/verification";
            $.ajax
              ({
                  type: "POST",
                  url: url,
                  data: ({trxref:response.trxref, merchantid:'pk_test_7d7271a9f0ca45ac76d8ca1569ea47948e1bb5f5'}),
                  success: function(response){
                    var pars = (JSON.parse(response))
                    var resp =(pars.status)

                    if(resp == 'success'){
                         var oldurl = "{{ route('transactions') }}";

                         $.ajax
                          ({
                              type: "POST",
                              url: oldurl,
                              data: ({booking_id: payload[0], booking_code: payload[1], status:'Success', response:'Transaction Successful', 'payment_method':'PayStack'}),
                              success: function(response){
                                <?php //$booking_code = "<script>document.write(payload[1])</script>";?>

                                @if(isset($booking))
                                if (response == 1) {
                                     window.location.href = "<?php echo env('SITE_URL').'payment/';?>"+payload[1]; //using a named route
                                };
                                @endif
                              }
                          });
                    }
                  }
              });
        }
    });
</script>
@stop