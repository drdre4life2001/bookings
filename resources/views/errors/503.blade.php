<!DOCTYPE HTML>
<html class="full">

<head>
    <title>Bus.com.ng - Coming soon</title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/mystyles.css">
    <script src="js/modernizr.js"></script>


</head>

<body class="full">

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">

        <div class="full-page text-center">
            <div class="bg-holder full">
                <div class="bg-mask-darken"></div>
                <div class="bg-img" style="background-image:url({{ asset('img/img16.jpg') }});"></div>
                <div class="bg-holder-content full text-white">
                    <a class="logo-holder" href="index.html">
                        <img src="{{ asset('img/logo-big.png') }}" alt="Image Alternative text" title="Image Title" />
                    </a>
                    <div class="full-center">
                        <div class="container">
                            <h2>Oooops!!! Something Went Wrong!</h2>
                            <h3>We Will Back Soon!</h3>
                            <div id="fillForm">
                            <div class="gap"></div>
                            <p>Enter your name, email and phone number to earn discounts, cash-back rewards and receive more about bus.com.ng.</p><Br/>
                            <form>
                                <div class="row">
                                    <div class="col-md-4 col-md-offset-4">
                                        
                                            <div class="form-group form-group-ghost form-group-lg">
                                                <input id="sub_name" class="form-control" placeholder="Type your name" required type="text" />
                                            </div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-md-offset-4">
                                        
                                            <div class="form-group form-group-ghost form-group-lg">
                                                <input id="sub_email" class="form-control" placeholder="Type your email address" required type="email" />
                                            </div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-md-offset-4">
                                        
                                            <div class="form-group form-group-ghost form-group-lg">
                                                <input id="sub_phone" class="form-control" placeholder="Type your phone" required type="text" />
                                            </div>
                                        
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-2 col-md-offset-5">
                                        
                                            <button type="submit" class="btn btn-primary" onclick="subscribe(); return false;" id="saveSub" >Get in touch with us now!</button>
                                        
                                    </div>
                                </div>
                                    
                            </form>  
                            </div>
                            <div id="submitted" class="hide">
                                <p>We've received your details and we will be in touch.</p>
                            </div>  
                               
                            
                        </div>
                    </div>
                    <!-- <ul class="list footer-social">
                        <li>
                            <a class="fa fa-facebook round box-icon-normal animate-icon-bounce" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-twitter round box-icon-normal animate-icon-bounce" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-dribbble round box-icon-normal animate-icon-bounce" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-linkedin round box-icon-normal animate-icon-bounce" href="#"></a>
                        </li>
                        <li>
                            <a class="fa fa-pinterest round box-icon-normal animate-icon-bounce" href="#"></a>
                        </li>
                    </ul> -->
                </div>
            </div>
        </div>



        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <!-- <script src="js/custom.js"></script> -->


        <script type="text/javascript">

    function  subscribe(){

        var email = $("#sub_email").val();
        var phone = $("#sub_phone").val();
        var name = $("#sub_name").val();

        if (typeof name === 'undefined' || !name || typeof email === 'undefined' || !email || typeof phone === 'undefined' || !phone){

            alert('Please enter your name, email and phone number');
            return false;

        }

        $('#saveSub').html('<img src="{{ asset('img/facebook.gif') }}"  alt="Sending....." style="height:30px" >');

        // console.log(bank+' '+acct_name+' number '+acct_number)
        $.ajax({
            url: "http://bustickets.ng/online/save-online-user",
            type: "post",
            data: {email:email, phone:phone, name:name},
            success: function (response) {
               // you will get response from your php page (what you echo or print)  
               // console.log(response); 
               $("#sub_email").val('');
               $("#sub_phone").val('');
               $("#sub_name").val('');
               $('#saveSub').html('Get the Travel Points');
               $("#submitted").removeClass('hide');
               $("#fillForm").addClass('hide');


            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
               $("#subscribe").modal('hide');
            }
        });

        return false;

    }


    // Countdown
$('.countdown').each(function() {
    var count = $(this);
    $(this).countdown({
        zeroCallback: function(options) {
            var newDate = new Date("November 1, 2017 12:13:00");
            // newDate = newDate.setMonth(10);
            // newDate = newDate.setHours(1);
                newDate = newDate.setHours(newDate.getHours() + 1);

            $(count).attr("data-countdown", newDate);
            $(count).countdown({
                unixFormat: true
            });
        }
    });
});


    </script>
    </div>

    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83001036-2', 'auto');
  ga('send', 'pageview');

</script>

</body>

</html>


