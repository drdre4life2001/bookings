@extends('layouts.app')

@section('content')

    <div class="gap"></div>

    <div class="container">

    <div class="row">
    	<div class="col-md-8 col-md-offset-2">
    		<h5>Please Confirm Trip Details</h5>
    		<table class="table table-stripe">

	    		<tbody>
	    			<tr>
	    				<td>Trip</td>
	    				<td>{{$booking->trip->name}}</td>
	    			</tr>
	    			<tr>
	    				<td>Departure Time</td>
	    				<td>{{date('h:i A', strtotime($booking->trip->departure_time))}}</td>
	    			</tr>
	    			<tr>
	    				<td>Fare</td>
	    				<td>{{number_format($old_booking->trip->fare)}}</td>
	    			</tr>
	    			<tr>
	    				<td>Passenger Count</td>
	    				<td>{{number_format($booking->passenger_count)}}</td>
	    			</tr>
	    			<tr>
	    				<td>Previous Booking Total Cost</td>
	    				<td>{{number_format($old_booking->final_cost)}}</td>
	    			</tr>
	    			<tr>
	    				<td>New Booking Total Cost</td>
	    				<td>{{number_format($booking->final_cost)}}</td>
	    			</tr>
	    		</tbody>
    		</table>
    		<form method="post">
    			{{csrf_field()}}
				<input type="hidden" name="booking_code" value="{{$inputs['booking_code']}}">
				<input type="hidden" name="departing" value="{{$inputs['departing']}}">
				<input type="hidden" name="go_ahead" value="true">
				<input type="submit" class="btn btn-success" value="Continue Booking">
				<a class="btn btn-danger" href="/dashboard">Go back</a>
			</form>
    	</div>
    </div>

    </div>
 @stop