<footer id="main-footer">
  <div class="container hidden-xs hidden-sm">
      <div class="row row-wrap">
          <div class="col-md-3">
              <a class="logo" href="index.html">
                  <img src="{{ asset('img/logo-black.jpg') }}" alt="Image Alternative text" width="100px" height="100px" style="border-radius: 500px;"  title="Image Title" />
              </a>
              <p class="mb20">Search through the largest bus travel inventory in Nigeria.!</p>
              <ul class="list list-horizontal list-space">
                  <li>
                      <a  href="https://facebook.com/bus.com.ng" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                  </li>
                  <li>
                      <a href="http://twitter.com/BusComNg" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                  </li>
                  <li>
                      <a href="https://instagram.com/bus.com.ng"  style="width: 20px; height: 20px;"  target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                  </li>

              </ul>
          </div>

          <div class="col-md-3">
              <!-- <h4>Newsletter</h4>
              <form>
                  <label>Enter your E-mail Address</label>
                  <input type="text" class="form-control">
                  <p class="mt5"><small>*We Never Send Spam</small>
                  </p>
                  <input type="submit" class="btn btn-primary" value="Subscribe">
              </form> -->
          </div>
          <div class="col-md-2">
              <ul class="list list-footer">
                  <li><a href="{{ url('pages/about') }}">About us</a>
                  </li>
                  <li><a href="{{ url('pages/whatsnew') }}">Press Centre</a>
                  </li>
                  <li><a href="{{ url('pages/whatsnew') }}">Jobs</a>
                  </li>
                  <li><a href="{{ url('/pages/privacy') }}">Privacy Policy</a>
                  </li>
                  <li><a href="{{ url('/pages/terms') }}">Terms of Use</a>
                  </li>
                  <li><a href="{{ url('/pages/contact') }}">Feedback</a>
                  </li>
              </ul>
          </div>
          <div class="col-md-4">
              <h4>Have Questions?</h4>
              <h4 style="color: grey;">080 30 500 600</h4>
              <h4><a href="#" style="color: grey;"> info@bus.com.ng </a></h4>
              <p style="color: grey;">24/7 Dedicated Customer Support</p>
          </div>

      </div>
  </div>
  <div class="container visible-sm visible-xs">
      <div class="row row-wrap">
          <div class="col-xs-12">
              <h4 class="text-center">Have Questions?</h4>
              <h4 class="text-center text-color">080 30 500 600</h4>
              <h5 class="text-center"><a href="#" class="text-color"> info@bus.com.ng</a></h5>
          </div>
          <p class="text-center"> Copyright &copy; <?php echo date('Y');?> bus.com.ng </p>

      </div>
  </div>

</footer>

<script src="{{ URL::asset('/js/jquery.js') }}" > </script>
<script src="{{ URL::asset('/js/bootstrap.js') }}" > </script>
<script src="{{ URL::asset('/js/slimmenu.js') }}" > </script>
<script src="{{ URL::asset('/js/dropit.js') }}" > </script>
<script src="{{ URL::asset('/js/bootstrap-datepicker.js') }}" > </script>
<script src="{{ URL::asset('/js/bootstrap-timepicker.js') }}" > </script>
<script src="{{ URL::asset('/js/nicescroll.js') }}" > </script>
<script src="{{ URL::asset('/js/ionrangeslider.js') }}" > </script>

<script src="{{ URL::asset('/select2/select2.min.js') }}" > </script>
<script src="{{ URL::asset('/js/icheck.js') }}" > </script>
<script src="{{ URL::asset('/js/fotorama.js') }}" > </script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script src="{{ URL::asset('/js/typeahead.js') }}" > </script>
<script src="{{ URL::asset('/js/card-payment.js') }}" > </script>
<script src="{{ URL::asset('/js/magnific.js') }}" > </script>
<script src="{{ URL::asset('/js/owl-carousel.js') }}" > </script>
<script src="{{ URL::asset('/js/fitvids.js') }}" > </script>
<script src="{{ URL::asset('/js/tweet.js') }}" > </script>
<script src="{{ URL::asset('/js/gridrotator.js') }}" > </script>
<script src="{{ URL::asset('/js/countdown.js') }}" > </script>
<script src="{{ URL::asset('/js/custom.js') }}" > </script>
<script src="{{ URL::asset('/js/jquery.query-object.js') }}" > </script>
<script src="{{ URL::asset('/js/rating.js') }}" > </script>


<script type="text/javascript">
$('.select2').select2();

function sendBookingSMS(bank, acct_name, acct_number, booking_id){
  // var ajaxloader = ("{{ public_path() }}/img/facebook.gif");

  $('#smsbtn'+bank).html('<img src="{{ asset('img/facebook2.gif') }}"  alt="Sending....." style="height:30px" >');

  // console.log(bank+' '+acct_name+' number '+acct_number)
  $.ajax({
      url: "{{ route('sms') }}",
      type: "post",
      data: {bank:bank, acct_name:acct_name, acct_number:acct_number, booking_id:booking_id},
      success: function (response) {
         // you will get response from your php page (what you echo or print)
         console.log(response);
         $('#smsbtn'+bank).html('Sms to me');

      },
      error: function(jqXHR, textStatus, errorThrown) {
         console.log(textStatus, errorThrown);
      }
  });
}

$('.reset-form').click(function(){
  $("#homepageSearch").find('select, textarea').val('');
});

$('#loyalty-check').click(function () {
  //confirmpasswordx
  var label = document.createElement('label');
  $(label).text("Create password");
  $(label).appendTo('#loyaltyx');

  var pwdBox = document.createElement('input');
  $(pwdBox).attr('type', 'password');
  $(pwdBox).attr('class', 'form-control');
  $(pwdBox).attr('name', 'password');

  var label1 = document.createElement('label');
  $(label1).text("Confirm password");
  $(label1).appendTo('#confirmpasswordx');

  var pwdBox1 = document.createElement('input');
  $(pwdBox1).attr('type', 'password');
  $(pwdBox1).attr('class', 'form-control');
  $(pwdBox1).attr('name', 'password_confirmation');



  $('#loyaltyx').empty();
  $('#confirmpasswordx').empty();

  if($(this).is(':checked')){
      $(label).appendTo('#loyaltyx');
      $(pwdBox).appendTo('#loyaltyx');

      $(label1).appendTo('#confirmpasswordx');
      $(pwdBox1).appendTo('#confirmpasswordx');
  }

});

function getJson(url, data, async = false) {
  return JSON.parse($.ajax({
              type: 'POST',
              url: url,
              data: data,
              dataType: 'json',
              global: false,
              async: async,
              success: function (data) {
                  return data;
              }
          }).responseText);
      }


$("#contact_phone").blur(function (e) {
  <?php if(isset($getCustomerDataUrl)):?>
      customerData = getJson("{{$getCustomerDataUrl}}",{'phone': $("#contact_phone").val()});
  <?php endif ?>

  if(customerData.data != null){
      $("#contact_name").val(customerData.data.contact_name);
      $("#kin_name").val(customerData.data.next_of_kin);
      $("#kin_phone").val(customerData.data.next_of_kin_phone);
      $("#contact_email").val(customerData.data.contact_email);
      $(".hide-if-reg").hide();
  }
  else{
      $(".hide-if-reg").show();
  }

});
<?php if(isset($booking)):?>
 $(".sendAcctDetails").click(function (){
      var acct = $(this).data("payload").split(',');
      var data = {};
      var sender = "Bus.com.ng";
      data.acct_number = acct[1];
      data.acct_name =  acct[2];
      data.bank = acct[0];
      data.booking_code = "{{$booking->booking_code}}";
      url = "{{route("sms")}}";

      response = getJson(url, data).split(" ");
      if(response[0] == "OK"){
          alert("Bank account sent to your phone number");
      }
  });

 $(".sendCashOffice").click(function () {
     var office_details = $(this).data("payload").split('|');
     // alert(office_details);

      var data = {};
      data.cashier_name = office_details[0];
      data.cashier_phone = office_details[1];
      data.cashier_address = office_details[2];
      data.booking_code = "{{$booking->booking_code}}";
      url = "{{route("sms")}}";
      response = getJson(url, data).split(" ");

      if(response[0] == "OK"){
          alert("Cash office details sent to your phone number");
      }
 });
<?php endif ?>
<?php if(isset($trip) && isset($psg)): ?>

if($(".infantHandler")){
  $(document.body).on('blur', '.infantHandler', function(){
      data = {};
      data.infant ={'age': $(this).val()};
      data.operator_id = {{$trip->operator->id}};
      data.unit_cost = {{ $trip->fare + ((!empty($return_trip))?$return_trip->fare:0) }};
      id = $(this).attr('id');
      initial_total_cost = {{$psg * ( $trip->fare + ((!empty($return_trip))?$return_trip->fare:0)) }};
      // this.total = initial_total_cost;


      url = "{{route('infant-price')}}";
      response = getJson(url, data);
      if(response != "" || response != null){
          if($('#inf'+id)){
              $('#inf'+id).remove();
          }
          p = document.createElement('p');
          response.infant_price = response.infant_price;
          $(p).css('display', 'block');
          $(p).addClass('infant_price_desc');
          $(p).attr('id', 'inf'+id);
          $(p).attr('data-payload', response.infant_price);

          infantname = $('#infantname'+id).val();
          infant_price = document.createElement('small');
          $(infant_price).css('color', 'red');
          $(infant_price).text("\u20A6"+response.infant_price + " ");
          $(p).append(infant_price);

          $("#remove-infant"+id).attr("data-payload", response.infant_price);


          discount= document.createElement('small');
          $(discount).css("color", "green");
          $(discount).text(infantname + response.discount+'% Discount');
          $(p).append(discount);
          old_total = $('.booking-item-payment-total>span').text().split("\u20A6");
          old_total = Number((old_total[1].trim()).replace(/[^\d.]/g,''));

          $(".infant-price").append(p);

              old_total = initial_total_cost;

              if($('.infant_price_desc')){
                  total = 0;
                  additional_fee = 0;

                  $('.infant_price_desc').each(function (index) {
                          total += $(this).data('payload');

                  });
                  $(".passengers").each(function (index) {
                      additional_fee += {{$trip->fare  + ((!empty($return_trip))?$return_trip->fare:0)}};

                  });
                  new_total = total + old_total + additional_fee;
              }
              console.log({"infant price":response.infant_price});
              console.log({"old_total": old_total});
              console.log({"new_total": new_total});
          $('.booking-item-payment-total>span').text("\u20A6"+new_total.toLocaleString());
      }
  });
}
  var count = {{$psg}};

  $(document.body).on('click', '#addPassenger', function(){

      var list = $("<li></li>").addClass("passenger-remove");
      var row = $('<div></div>').addClass("row");
      var passenger = $('<div></div>').addClass("col-md-3");
      var removeIcon = $("<i></i>").addClass("fa fa-close remove-icon");
      var psg_label = $("<label></label>").append(removeIcon).append($("<span></span>").text(" Passenger "+(count+1))).appendTo(passenger);

      $(passenger).appendTo(row);

      var name_div = $('<div></div>').addClass("col-md-6");

      var name_inner_div = $('<div></div>').addClass("form-group");

      var name_label = $('<label></label>').text("Full Name").appendTo(name_inner_div);

      var name_input = $('<input></input>');
      $(name_input).addClass("form-control").addClass("form-control").attr("name", "passengers"+(count)).attr("type", "text").attr("id", "passengers"+(count+1)).addClass("passengers").appendTo(name_inner_div);

      $(name_div).append(name_inner_div);
      $(row).append(name_div);

      var gender_div = $('<div></div>').addClass("col-md-3");

      var gender_inner_div = $('<div></div>').addClass("form-group");

      var gender_select = $("<select></select>").addClass("form-control").attr("name","p_gender"+count);
      $("<label></label>").text("Gender").appendTo(gender_inner_div);
      $("<option>Select Your Gender</option>").val("").appendTo(gender_select);
      $("<option></option>").text("Female").val("Female").appendTo(gender_select);
      $("<option></option>").text("Male").val("Male").appendTo(gender_select);

      $(gender_inner_div).append(gender_select);
      $(gender_div).append(gender_inner_div);
      $(row).append(gender_div);
      $(list).append(row);
      $("#passengers-section").append(list);

      old_total = $('.booking-item-payment-total>span').text().split("\u20A6");
      old_total = Number((old_total[1].trim()).replace(/[^\d.]/g,''));
      total = 0;
      // additional_fee = 0;

      additional_fee = {{ $trip->fare  + ((!empty($return_trip))?$return_trip->fare:0)}};
      new_total = old_total + additional_fee;
      count = count+1;
      $("#p_count").val($(".remove-icon").size());
      //ajax call to update passenger count
      $('.booking-item-payment-total>span').text("\u20A6"+new_total.toLocaleString());
  });

  $(document.body).on('click', '.remove-icon,.remove-infant' ,function(){
      count--;
      $(this).parent().parent().parent().parent().remove();

      old_total = $('.booking-item-payment-total>span').text().split("\u20A6");
      old_total = Number((old_total[1].trim()).replace(/[^\d.]/g,''));
      var j = $(this).attr("class").split(" ");

      if(j[2] == "remove-infant"){
          amount = $(this).data("payload")? $(this).data("payload"): 0;
          new_total = old_total - amount;
          $("#i_count").val($(".remove-infant").size());

      }
      else{
          fare = {{$trip->fare  + ((!empty($return_trip))?$return_trip->fare:0) }};
          new_total = old_total - fare;
          $("#p_count").val($(".remove-icon").size());
      }

      cp = $("#p_count").val()? parseInt($("#p_count").val()) + 1 : @if(isset($psg)) {{$psg}} @endif;
      $(".infant_price_desc").each(function () {
                      inf_p = $(this).data("payload");
                      if(inf_p > 0){
                          cp += 1;
                      }
                  });
      seats = $('.seat-selected').size();

      if(seats > cp){
          x = [];
          s = $(".seat-selected");
          for (var i = 0; i < seats - cp; i++) {

              x = $(".seats").data("payload").split(",");
              console.log({'init': x.join(",")});
              val = $(s[i]).text();
              contains = $.inArray(val, x);


              if(contains != -1){

                   $(s[i]).find("img").attr("src", "{{ asset('img/available_seat_img.gif') }}").removeClass("seat-selected");
                  rem = x.splice(contains, 1);

                  console.log({'final': x.join(",")});

                  dd = $(".seats").data("payload", x.join(","));
                  $(".seats").attr("data-payload", x.join(","));

              }
          }
      }

      $('.booking-item-payment-total>span').text("\u20A6"+new_total.toLocaleString());

  });



  <?php if(isset($infants)):?>
      var countx = ({{$infants}})?{{$infants}}: 0;
  <?php endif; ?>
  $(document.body).on('click',"#addInfant",function(){

      var list = $("<li></li>").addClass("infant-remove");
      var row = $('<div></div>').addClass("row");
      var passenger = $('<div></div>').addClass("col-md-3");
      var removeIcon = $("<i></i>").addClass("fa fa-close remove-infant").attr("id", "remove-infant"+(countx+1));


      var psg_label = $("<label></label>").append(removeIcon).append($("<span></span>").text(" Infant "+(countx+1))).appendTo(passenger);

      $(passenger).appendTo(row);

      var name_div = $('<div></div>').addClass("col-md-7");

      var name_inner_div = $('<div></div>').addClass("form-group");

      var name_label = $('<label></label>').text("Full Name").appendTo(name_inner_div);

      var name_input = $('<input></input>');
      $(name_input).addClass("form-control").addClass("form-control").attr("name", "infants"+(countx+1)).attr("type", "text").attr("id", "infantname"+(countx+1)).appendTo(name_inner_div);

      $(name_div).append(name_inner_div);
      $(row).append(name_div);

      var gender_div = $('<div></div>').addClass("col-md-1");

      var gender_inner_div = $('<div></div>').addClass("form-group");

      var gender_select = $("<select></select>").addClass("form-control").attr("name","i_gender"+(countx+1));
      $("<label></label>").text("Gender").appendTo(gender_inner_div);
      $("<option>Select Your Gender</option>").val("").appendTo(gender_select);
      $("<option></option>").text("Female").val("Female").appendTo(gender_select);
      $("<option></option>").text("Male").val("Male").appendTo(gender_select);

      $(gender_inner_div).append(gender_select);
      $(gender_div).append(gender_inner_div);
      $(row).append(gender_div);
      var age_div = $('<div></div>').addClass("col-md-1").append($("<label></label>").text("Age"));
      age_div.append($("<input></input>").addClass("form-control infantHandler").attr("name", "i_age"+(countx+1)).attr("type","text").attr("id",(countx+1)));
      age_div.appendTo(row)
      $(list).append(row);

      $("#infants-section").append(list);
      $("#i_count").val($(".remove-infant").size());

      countx = countx+1;

  });
  $("#submitter").submit(function(e){
      $("<input></input>").attr("name", "selected_seats").attr("type","hidden").val($(".seats").data("payload")).appendTo("#hidden-f");

      var seats = $(".seats").data("payload");
      // alert($('.seat-selected').size());
      // e.preventDefault();
  });

function displaySeats(layout, occupied, selector) {
var settings = {
              layout: layout,
              occupied: occupied,
              };

var s14A = [[1,0,0,0,1],[0,1,1,1,1],[0,1,1,1,1,],[1,1,1,1,1]];
var s15A = [
[1,0,0,0,1],
[1,1,1,1,1],
[0,1,1,1,1,],
[1,1,1,1,1]
];

var bus_layouts = {
"A14": s14A,
"A15": s15A
};
var n14A = [
[1,  0, 0,  0, 11],
[0,  2, 5, 8, 12],
[0,  3, 6, 9, 13],
["d", 4,7, 10, 14]
];
var n15A = [
[2,  0, 0,  0, 15],
[1,  5, 8, 11, 14],
[0,  4, 7, 10, 13],
["d", 3,6, 9, 12]
];
var s_number = {
"A14": n14A,
"A15": n15A
};

  if(settings.layout !="" || settings.layout != null && bus_layouts[settings.layout]){
    slayout = bus_layouts[settings.layout];
    snumber = s_number[settings.layout];

    for(var i= 0; i < slayout.length; i++){
     list = $("<ul></ul>").addClass("seat-list");

      $(selector).append(list);
      for(var j=0; j < slayout[i].length; j++){
        if(slayout[i][j] == 1){
          occupied = $.inArray(snumber[i][j], settings.occupied)
          if(occupied != -1){
              list.append($("<li></li>").text(snumber[i][j]).addClass("seat-list-item occupied disabled").append($("<img>").attr("src", "{{ asset('img/booked_seat_img.gif') }}")));
          }
          else{
              if(snumber[i][j] == "d"){
                  list.append($("<li></li>").addClass("seat-list-item").html($("<span></span>")).append($("<img>").attr("src", "{{ asset('img/steering-wheel-xxl.png') }}")));
              }
              else{
                  list.append($("<li></li>").addClass("seat-list-item").html($("<span></span>").text(snumber[i][j])).append($("<img>").attr("src", "{{ asset('img/available_seat_img.gif') }}")));
              }
          }
        }
        if(slayout[i][j] == 0){
          list.append($("<li></li>").text(" ").addClass("seat-list-item empty disabled"));
        }
      }
    }
 }

 $(selector).append(list);

 seat_selected = [];

 $(document.body).on('click', '.seat-list-item', function(){
  var infant_age_empty_count = 0;
  var infant_age = true;

  $(".infantHandler").each(function (index, value) {

      if($(this).val() == "" || $(this).val() == null){
          infant_age_empty_count += 1;
      }
  });
  if(infant_age_empty_count > 0){
      alert("Select age for all infants first");
      infant_age = false;
  }
  text = parseInt($(this).text());
  if(!isNaN(text) && infant_age){

      contains = $.inArray(text, seat_selected);
      if(!$(this).hasClass("disabled")){

            if(contains != -1){
              seat_selected.splice(contains, 1);
              $(this).removeClass("seat-selected");
              $(selector).attr("data-payload", seat_selected.join(","));
              $(this).find("img").attr("src", "{{ asset('img/available_seat_img.gif') }}");
            }
            else{
                  cp = $("#p_count").val()? parseInt($("#p_count").val()) + 1 : @if(isset($psg)) {{$psg}} @endif;
                  $(".infant_price_desc").each(function () {
                      inf_p = $(this).data("payload");
                      if(inf_p > 0){
                          cp += 1;
                      }
                  });

                  if( cp > seat_selected.length){
                      seat_selected.push(text);
                      $(this).addClass("seat-selected");
                      $(selector).attr("data-payload", seat_selected.join(","));
                      $(this).find("img").attr("src", "{{ asset('img/selected_seat_img.gif') }}");
                  }
                  else{
                    alert("seat chosen for all passengers");
                  }
              }
          }
      }
 });
}
<?php $layout = "A".$trip->bus_type->no_of_seats;?>

displaySeats("{{$layout}}", <?php if(isset($occupied_seats)){ echo json_encode($occupied_seats);}else{echo json_encode([]);}?>, ".seats");
<?php endif ?>
  $('.containerx').rating(function(vote, event){

  });


</script>

</div>
