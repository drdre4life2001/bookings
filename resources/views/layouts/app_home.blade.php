<!DOCTYPE HTML>
<html>

<head>
    <title>Book Bus Tickets | Bus.com.ng</title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Travel, Bus Travel, Nigeria" />
    <meta name="description" content="BUS.com.ng - Find Your Perfect Trip">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/stylesheet.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/material-bootstrap-wizard.css') }}">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}

    <link rel="stylesheet" href="{{ URL::asset('css/reset.css') }}"> <!-- Resource style -->
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}"> <!-- Resource style -->
    <script src="{{ URL::asset('/js/jquery-2.2.4.min.js') }}"></script>
    <script src="{{ URL::asset('js/modernizr.js') }}"></script> <!-- Modernizr -->
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap-select.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Asap|Cabin|Lato|Roboto:900|Rubik|Open+Sans:800|Raleway:900|Work+Sans:800|Montserrat+Alternates" rel="stylesheet">

    <style type="text/css">
        .navbar-default {
            background-color: none;
            border-color: none;
        }

    </style>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-101917040-1', 'auto');
        ga('send', 'pageview');

    </script>


</head>

<body>

<div class="global-wrap">
    <header style="border-bottom: none !important">
        <div class="container">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <a href="{{ url('/') }}"><img src="{{ URL::asset('img/bus-logo.png') }}" width="25%" class="logo"></a>
            </div>

            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <div id="h-side" class="col-lg-8 col-md-8 hidden-sm hidden-xs">

                </div>
                <div id="nav">

                    <div id="nav">
                        <a href="#cd-nav" class="cd-nav-trigger" style="background:#f73838; border-radius: 100%; padding: 2%; color:white"> <span style="color:white"></span>
                            <span><!-- used to create the menu icon --></span>
                        </a> <!-- .cd-nav-trigger -->
                    </div>

                    <nav class="cd-nav-container" id="cd-nav">
                        <header>
                            <h3>Navigation</h3>
                            <a href="#0" class="cd-close-nav">Close</a>
                        </header>

                        <ul class="cd-nav">
                            <li class="cd">
                                <a href="index.php">
                            <span>
                                <i class="fa fa-home" style="font-size: 30px; color:#fff"></i>
                            </span>

                                    <em>Home</em>
                                </a>
                            </li>

                            <li data-menu="projects">
                                <a href="pages/about">
                            <span>
                                <i class="fa fa-users" style="font-size: 30px; color:#fff"></i>
                            </span>


                                    <em>About Us</em>
                                </a>
                            </li>

                            <li data-menu="faq">
                                <a href="pages/faq">
                            <span>
                                <i class="fa fa-question-circle" aria-hidden="true" style="font-size: 30px; color:#fff"></i>
                            </span>

                                    <em>FAQs</em>
                                </a>
                            </li>

                            <li data-menu="services">
                                <a href="{{url('nysc')}}">
                            <span>
                                <i class="fa fa-bus" aria-hidden="true" style="font-size: 30px; color:#fff"></i>
                            </span>

                                    <em>NYSC <br />Straight to camp</em>
                                </a>
                            </li>

                            <li data-menu="bus">
                                <a href="pages/benefits">
                            <span>
                                <i class="fa fa-ticket" aria-hidden="true" style="font-size: 30px; color:#fff"></i>
                            </span>

                                    <em>Bus Ticket</em>
                                </a>
                            </li>
                            <li data-menu="contact">
                                <a href="contact">
                            <span>
                                <i class="fa fa-envelope-open" aria-hidden="true" style="font-size: 30px; color:#fff"></i>
                            </span>

                                    <em>Contact</em>
                                </a>
                            </li>


                        </ul> <!-- .cd-3d-nav -->

                        <a href="#"><img src="{{ URL::asset('img/navdesign.jpg') }}" width="100%"></a>
                    </nav>


                </div>
            </div>

        </div>
    </header>

@yield('content')

@include('layouts.home_footer')

@yield('scripts')

</body>

</html>
