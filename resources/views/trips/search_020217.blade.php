@extends('layouts.app')

@section('content')

@include('bookings.includes.steps')

<div class="container">
    <div class="row">
        <div class="col-md-9 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="booking-title pull-left">
                      @if(count($trips) > 0)
                        {{ count($trips)}}
                        @if(!empty($round_trip_stage)) {{ ($round_trip_stage == 1)?'Ongoing':'Return' }}  @endif
                        {{((count($trips) == 1 )?' trip':' trips') }} from {{ $fromParkObj->name }} to {{ $toParkObj->name }} on {{ date('M jS', strtotime($date)) }} for {{$passengers.(($passengers ==1)?' passenger':' passengers') }}
                        <small><a class="popup-text" href="#search-dialog" data-effect="mfp-zoom-out">Change search</a></small>

                      @elseif(count($altTrips) > 0)
                        <?php $trips = $altTrips; //dump($altTrips);  ?>

                        <small>Lemme tell you a sad story.  There are no trips from {{ $fromParkObj->name }} to {{ $toParkObj->name }} on {{ date('M jS', strtotime($date)) }}. However, we found trips to surronding area. <a class="popup-text" href="#search-dialog" data-effect="mfp-zoom-out">Change search</a></small><br/><br/>

                        {{ count($trips)}}
                        @if(!empty($round_trip_stage)) {{ ($round_trip_stage == 1)?'Ongoing':'Return' }}  @endif
                        {{((count($trips) == 1 )?' trip':' trips') }} from {{ $fromParkObj->name }} to related destinations on {{ date('M jS', strtotime($date)) }} for {{$passengers.(($passengers ==1)?' passenger':' passengers') }}




                      @else

                        There are no trips from {{ $fromParkObj->name }} to {{ $toParkObj->name }} on {{ date('M jS', strtotime($date)) }} for {{$passengers.(($passengers ==1)?' passenger':' passengers') }}
                        <small><a class="popup-text" href="#search-dialog" data-effect="mfp-zoom-out">Change search</a></small>


                      @endif
                    </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pull-right">
                    <div class="nav-drop booking-sort pull-right">
                        <h5 class="booking-sort-title"><a class="emphasize-it" href="#" >Sort: {{ $sort_type }}<i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a>
                        </h5>
                        <ul class="nav-drop-menu">
                            @foreach($sort_types as $key =>$value)
                                <li><a href="#" onclick="updateUrl('sort_by', '{{ $key }}');">{{ $value }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

            </div>


            <ul class="booking-list">

                @foreach($trips as $trip)

                <li>
                    <div class="booking-item-container">
                        <div @if($trip == $trips[0])class="booking-item highlight-first" @else class="booking-item" @endif>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="booking-item-airline-logo">
                                        <img src="{{ config('api.api-host').'logos/'.$trip->operator->img }}" alt="Image Alternative text" title="Image Title" width="100px" />
                                        <!--p>{{ $trip->operator->name}}</p-->
                                    </div>
                                </div>
                                <div class="col-md-5">
                                <div class="booking-item-rating">
                                    <span class="booking-item-rating-number">
                                    {{ $trip->sourcepark->name }} <small>TO</small> {{ $trip->destpark->name }}
                                	</span>
                                        <div class="booking-item-departure">
                                            <h5>{{ $trip->operator->name}}</h5>
                                            <h5>{{ $trip->bus_type->name }} - {{ $trip->bus_type->no_of_seats }} seater [{{ $trip->trip_position }}]</h5>
                                            <p class="booking-item-destination">Departure time: {{ date('gA', strtotime($trip->departure_time)) }} </p>

                                            @if($trip->ac)
                                            <span<i class="fa fa-check-square-o font-green-jungle"></i>  AC</span>
                                            @else
                                            <span><i class="fa fa-times-circle font-red-thunderbird"></i>  AC</span>
                                            @endif

                                            &nbsp;&middot;&nbsp;
<!--
                                            @if($trip->security)
                                                <span><i class="fa fa-check-square-o font-green-jungle"></i>  Security</span>
                                            @else
                                            <span ><i class="fa fa-times-circle font-red-thunderbird"></i>  Security</span>
                                            @endif

                                            &nbsp;&middot;&nbsp;

                                            @if($trip->insurance)
                                            <span ><i class="fa fa-check-square-o font-green-jungle"></i>  Insurance</span>
                                            @else
                                            <span><i class="fa fa-times-circle font-red-thunderbird"></i>  Insurance</span>
                                            @endif

                                            &nbsp;&middot;&nbsp;

                                            <br/>
                                            @if($trip->tv)
                                            <span><i class="fa fa-check-square-o font-green-jungle"></i>  TV</span>
                                            @else
                                            <span><i class="fa fa-times-circle font-red-thunderbird"></i>  TV</span>
                                            @endif

                                            &nbsp;&middot;&nbsp;

                                            @if($trip->passport)
                                            <span><i class="fa fa-check-square-o font-green-jungle"></i>  Passport</span>
                                            @else
                                            <span><i class="fa fa-times-circle font-red-thunderbird"></i>  Passport</span>
                                            @endif -->
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-3">
                                    @if($trip->international !== 1)
                                    <span class="booking-item-price">&#x20A6;<?php echo number_format( $trip->fare * $passengers + ( (1 - ($trip->operator->local_children_discount/ 100)) * $trip->fare * $children)   ) ?>
                                        <a class="btn btn-primary" href="{{ route('new-booking', [$trip->id,
                                    $trip->fare, 0,$passengers, $children, $infants, $date,
                                    $end]) }}">Book</a>
                                    </span>
                                    @else
                                    <span>Virgin Passport:</span><span class="booking-item-price">&#x20A6;<?php echo number_format(
                                            $trip->virgin_passport
                                            * $passengers + ( (1 - ($trip->operator->local_children_discount/ 100)) * $trip->virgin_passport * $children)   ) ?>
                                    @if(!empty($end))
                                    <a class="btn btn-primary" href="{{ route('new-booking', [$trip->id,
                                    $trip->virgin_passport, 'virgin_passport',$passengers, $children, $infants, $date,
                                    $end]) }}">Book</a>
                                    @else
                                    <a class="btn btn-primary" href="{{ route('new-booking', [$trip->id,
                                    $trip->virgin_passport, 'virgin_passport',$passengers,  $children, $infants, $date]) }}">Book</a>
                                	@endif
                                    </span>
                                    <span>Regular Passport:</span><span class="booking-item-price">&#x20A6;<?php echo
                                        number_format(
                                            $trip->regular_passport
                                            * $passengers + ( (1 - ($trip->operator->local_children_discount/ 100)) *
                                                $trip->regular_passport * $children)   ) ?>
                                        @if(!empty($end))
                                        <a class="btn btn-primary" href="{{ route('new-booking', [$trip->id,
                                    $trip->regular_passport, 'regular_passport',$passengers, $children, $infants,
                                    $date,
                                    $end]) }}">Book</a>
                                    @else
                                    <a class="btn btn-primary" href="{{ route('new-booking', [$trip->id,
                                    $trip->regular_passport, 'regular_passport',$passengers,  $children, $infants, $date]) }}">Book</a>
                                	@endif
                                    </span>
                                    <br/>
                                    <span>No Passport: </span><span class="booking-item-price">&#x20A6;<?php echo
                                        number_format(
                                            $trip->no_passport
                                            * $passengers + ( (1 - ($trip->operator->local_children_discount/ 100)) *
                                                $trip->no_passport * $children)   ) ?></span>
                                    <!--<br/><span>{{ $passengers }} adults </span>
                                    <br/> <span> {{$children}} Children </span>
                                    <br/> <span> {{$infants}} Infants </span>
									<p class="booking-item-flight-class">(&#x20A6; {{ number_format($trip->no_passport) }} each)</p>
                                        <small>
                                            <a href="#" rel="tooltip" title="" data-original-title="{!! strip_tags( $trip->operator->local_terms ) !!}">
                                                see operator rules
                                            </a>
                                        </small>
                                    <br/>-->
                                    @if(!empty($end))
                                    <a class="btn btn-primary" href="{{ route('new-booking', [$trip->id,
                                    $trip->no_passport, 'no_passport', $passengers, $children, $infants, $date,
                                    $end]) }}">Book</a>
                                    @else
                                    <a class="btn btn-primary" href="{{ route('new-booking', [$trip->id,
                                    $trip->no_passport, 'no_passport',$passengers,  $children, $infants,
                                    $date]) }}">Book</a>
                                    @endif
                                    @endif
                                </div>

                            </div>
                        </div>

                    </div>
                </li>
                @endforeach


            </ul>

            <div class="row">
                <div class="col-md-6">
                    <p><small>{{$trip_count}} Trips found</small>
                    </p>
                    <ul class="pagination">
                        @if(isset($search_paginate['pages']))
                                @foreach($search_paginate['pages'] as $page)
                                    <li <?php if(isset($page['active']) && $page['active'] == true):?> class="active"<?php endif; ?>>
                                        <a href="{{$page['url']}}">{{$page['page']}}</a>
                                    </li>
                                @endforeach
                            @endif
                                <?php if(isset($search_paginate['next_page_link'])):?>
                                    <li class="next">
                                        <a href="{{$search_paginate['next_page_link']}}">Next Page</a>
                                    </li>
                        <?php endif;?>
                    </ul>
                </div>
                <div class="col-md-6 text-right">
                    {{-- <p>Not what you're looking for? <a class="popup-text" href="#search-dialog" data-effect="mfp-zoom-out">Try your search again</a>
                    </p> --}}
                    <p>Not what you're looking for? <a class="popup-text" href="#search-dialog" data-effect="mfp-zoom-out">Try your search again</a>
                    </p>
                </div>
            </div>

        </div>
           <div class="col-md-3 hidden-xs hidden-sm hidden-md">


            <!--br/><br/-->
            @if($trip_count > 0)
            <aside class="booking-filters text-white">
                <h3>Filter By:</h3>
                <ul class="list booking-filters-list">
                    <li>
                        <h5 class="booking-filters-title">Price</h5>
                        {{-- <input type="text" id="price-slider"> --}}
                        <label>Min Price</label>
                        <input type="number" min="1000" id="minP" name="min_price">
                        <label>Max Price</label>
                        <input type="number" min="1000" id="maxP" name="max_price">
                    </li>
                    </li>
                     <li>
                        <h5 class="booking-filters-title">Bus Types</h5>
                        @foreach($bus_types as $bt)
                        <div class="checkbox">
                            <label>
                                <input class="btype" @if($bus_type == $bt->id) checked="checked" @endif type="radio" name="bustype" value="{{ $bt->id }}" />{{$bt->name}}</label>
                        </div>
                        @endforeach

                    </li>
                     <li>
                        <h5 class="booking-filters-title">Bus Features</h5>
                        <div class="checkbox">
                            <label>
                                <input class="bfeats" type="checkbox" @if($ac == 1) checked="checked" @endif name="ac" />AC</label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input class="bfeats" type="checkbox" @if($security == 1) checked="checked" @endif name="security" />Security</label>
                        </div>
                        <!-- <div class="checkbox">
                            <label>
                                <input class="bfeats" type="checkbox" @if($insurance == 1) checked="checked" @endif name="insurance" />Insurance</label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input class="bfeats" type="checkbox" @if($tv == 1) checked="checked" @endif name="tv" />TV</label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input class="bfeats" type="checkbox" @if($passport == 1) checked="checked" @endif name="passport" />Passport</label>
                        </div> -->
                    </li>

                     <li>
                        <h5 class="booking-filters-title">Operators</h5>
                        <select name="oprad" id="" class="oprad select2" style="width: 90%;">
                            <option value="">--choose park</option>
                            @foreach($operators as $op)
                            <option value="{{ $op->id }}" @if($operator == $op->id) selected @endif>{{ $op->name }}</option>
                            @endforeach
                        </select>
                        {{-- @foreach($operators as $op)
                        <div class="checkbox">
                            <label>
                                <input class="oprad"  @if($operator == $op->id) checked="checked" @endif type="radio" name="oprad" value="{{ $op->id }}" />{{$op->name}}</label>
                        </div>
                        @endforeach --}}

                    </li>


                </ul>


            </aside>

            @endif

            <br/><br/>
             <h4>Refine your search:</h4>
            <form method="post" action="{{ url('trips/loading') }}" class="booking-item-dates-change mb30">
            {{csrf_field()}}
                <div class="form-group form-group-icon-left">
                    <label>From</label>
                     <select name="oneFrom" id="" class="select2" style="width:100%">
                        <option value="">--choose park</option>
                        @foreach($parks as $park)
                        <option value="{{ $park->id }}" <?php if($fromPark == $park->id):?> selected='selected' <?php endif; ?> >{{ $park->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group form-group-icon-left">
                    <label>To</label>
                     <select name="oneTo" id="" class="select2" style="width:100%">
                        <option value="">--choose park</option>
                        @foreach($parks as $park)
                        <option value="{{ $park->id }}" <?php if($toPark == $park->id):?> selected='selected' <?php endif; ?> >{{ $park->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                    <label>Departing</label>
                    <input class="date-pick form-control" data-date-format="MM d, D" type="text" name="departing" value=""/>
                </div>
                <div class="form-group form-group-select-plus">
                    <label>Passengers</label>
                    <div class="btn-group btn-group-select-num @if($passengers > 5) hidden @endif" data-toggle="buttons">
                        <label class="btn btn-primary @if($passengers == 1) active @endif">
                            <input type="radio" name="options" value="1" @if($passengers == 1) checked @endif/>1</label>
                        <label class="btn btn-primary @if($passengers == 2) active @endif">
                            <input type="radio" name="options" value="2" @if($passengers == 2) checked @endif/>2</label>
                        <label class="btn btn-primary @if($passengers == 3) active @endif">
                            <input type="radio" name="options" value="3" @if($passengers == 3) checked @endif/>3</label>
                        <label class="btn btn-primary @if($passengers == 4) active @endif">
                            <input type="radio" name="options" value="4" @if($passengers == 4) checked @endif/>4</label>
                        <label class="btn btn-primary">
                            <input type="radio" name="options" />4+</label>
                    </div>

                    <select name="passengers" class="form-control @if($passengers < 5) hidden @endif">
                        <option value="">choose one</option>
                        @for($i = 1; $i <=16; $i++)
                             <option value="{{$i}}" @if($passengers == $i) selected="selected" @endif>{{$i}}</option>
                        @endfor
                    </select>
                </div>
                <div class="form-group form-group-select-plus">
                    <label>Infants</label>
                    <div class="btn-group btn-group-select-num" data-toggle="buttons">
                        <label class="btn btn-primary @if($children == 1) active @endif">
                            <input type="radio" name="children_options" value="1">1</label>
                        <label class="btn btn-primary  @if($children == 2) active @endif">
                            <input type="radio" name="children_options" value="2">2</label>
                        <label class="btn btn-primary  @if($children == 3) active @endif">
                            <input type="radio" name="children_options" value="3">3</label>
                        <label class="btn btn-primary  @if($children == 4) active @endif">
                            <input type="radio" name="children_options" value="4">4</label>

                        <label class="btn btn-primary">
                            <input type="radio" name="children_options">4+</label>
                    </div>
                    <select name="children" class="form-control hidden">
                        @for($i = 1; $i <=16; $i++)
                            <option value="{{$i}}" @if($children == $i) selected="selected" @endif>{{$i}}</option>
                        @endfor
                    </select>
                </div>
                <input class="btn btn-primary" type="submit" value="Update Search" />
            </form>

        </div>
    </div>
    <div class="gap"></div>
</div>

    <div class="mfp-with-anim mfp-hide mfp-dialog mfp-search-dialog" id="search-dialog" style="overflow: hidden;">
        <h3>Search for Bus Tickets</h3>
        <form action="{{ url('trips/loading') }}" method="post">
            {{csrf_field()}}
            <div class="tabbable">
                <ul class="nav nav-pills nav-sm nav-no-br mb10" id="flightChooseTab">

                    <li class="active"><a href="#flight-search-2" data-toggle="tab">One Way</a>
                    </li>
                    <li ><a href="#flight-search-1" data-toggle="tab">Round Trip</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade" id="flight-search-1">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-highlight"></i>
                                    <label>From</label>
                                    <select name="oneFrom2" id="" class="select2" style="width:300px">
                                        <option value="">--choose park</option>
                                        @foreach($parks as $park)
                                        <option value="{{ $park->id }}" <?php if($fromPark == $park->id):?> selected='selected' <?php endif; ?> >{{ $park->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-highlight"></i>
                                    <label>To</label>
                                    <select name="oneTo2" id="" class="select2" style="width:300px">
                                        <option value="">--choose park</option>
                                        @foreach($parks as $park)
                                        <option value="{{ $park->id }}" <?php if($toPark == $park->id):?> selected='selected' <?php endif; ?> >{{ $park->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                        <label>Departing</label>
                                        <input class="form-control" name="start" type="text" />
                                    </div>
                                </div>

                        </div>
                        <div class="input-daterange" data-date-format="MM d, D">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                        <label>Returning</label>
                                        <input class="form-control" name="end" type="text" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-lg form-group-select-plus">
                                        <label>Adults</label>
                                        <div class="btn-group btn-group-select-num" data-toggle="buttons">
                                            <label class="btn btn-primary active">
                                                <input type="radio" name="options2" value="1" />1</label>
                                            <label class="btn btn-primary">
                                                <input type="radio" name="options2" value="2" />2</label>
                                            <label class="btn btn-primary">
                                                <input type="radio" name="options2" value="3" />3</label>
                                            <label class="btn btn-primary">
                                                <input type="radio" name="options2" value="4" />4</label>
                                            <label class="btn btn-primary">
                                                <input type="radio" name="options2" value="5" />5</label>
                                            <label class="btn btn-primary">
                                                <input type="radio" name="options" />5+</label>
                                        </div>
                                       <select name="passengers2" class="form-control hidden">
                                            <option value="">Choose one</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option >4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                            <option>13</option>
                                            <option>14</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-group-select-plus">
                                    <label>Children</label>
                                    <div class="btn-group btn-group-select-num" data-toggle="buttons">
                                        <label class="btn btn-primary @if($children == 1) active @endif">
                                            <input type="radio" name="children_options2" value="1">1</label>
                                        <label class="btn btn-primary  @if($children == 2) active @endif">
                                            <input type="radio" name="children_options2" value="2">2</label>
                                        <label class="btn btn-primary  @if($children == 3) active @endif">
                                            <input type="radio" name="children_options2" value="3">3</label>
                                        <label class="btn btn-primary  @if($children == 4) active @endif">
                                            <input type="radio" name="children_options2" value="4">4</label>

                                        <label class="btn btn-primary">
                                            <input type="radio" name="children_options2">4+</label>
                                    </div>
                                    <select name="children2" class="form-control hidden">
                                        @for($i = 1; $i <=16; $i++)
                                            <option value="{{$i}}" @if($children == $i) selected="selected" @endif>{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group form-group-select-plus">
                                    <label>Infants</label>
                                    <div class="btn-group btn-group-select-num" data-toggle="buttons">
                                        <label class="btn btn-primary @if($infants == 1) active @endif">
                                            <input type="radio" name="infant_options2" value="1">1</label>
                                        <label class="btn btn-primary  @if($infants == 2) active @endif">
                                            <input type="radio" name="infant_options2" value="2">2</label>
                                        <label class="btn btn-primary  @if($infants == 3) active @endif">
                                            <input type="radio" name="infant_options2" value="3">3</label>
                                        <label class="btn btn-primary  @if($infants == 4) active @endif">
                                            <input type="radio" name="infant_options2" value="4">4</label>

                                        <label class="btn btn-primary">
                                            <input type="radio" name="infant_options2">4+</label>
                                    </div>
                                    <select name="infants2" class="form-control hidden">
                                        @for($i = 1; $i <=16; $i++)
                                            <option value="{{$i}}" @if($infants == $i) selected="selected" @endif>{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade in active" id="flight-search-2">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group form-group-lg form-group-icon-left">
                                    <label>From</label>
                                      <select name="oneFrom" id="" class="select2" style="width:100%">
                                        <option value="">--choose park</option>
                                        @foreach($parks as $park)
                                        <option value="{{ $park->id }}" <?php if($fromPark == $park->id):?> selected='selected' <?php endif; ?> >{{ $park->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-group-lg form-group-icon-left">
                                    <label>To</label>
                                    <select name="oneTo" id="" class="select2" style="width:100%">
                                        <option value="">--choose park</option>
                                        @foreach($parks as $park)
                                        <option value="{{ $park->id }}" <?php if($toPark == $park->id):?> selected='selected' <?php endif; ?> >{{ $park->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                    <label>Departing</label>
                                    <input class="date-pick form-control" data-date-format="M d, D" type="text" name="departing" />
                                </div>
                            </div>
                        </div>
                         <div class="row">

                            <div class="col-md-4">
                                <div class="form-group form-group-lg form-group-select-plus">
                                    <label>Adults</label>
                                    <div class="btn-group btn-group-select-num @if($passengers > 5) hidden @endif" data-toggle="buttons">
                                        <label class="btn btn-primary @if($passengers == 1) active @endif">
                                            <input type="radio" name="options" value="1" @if($passengers == 1) checked @endif/>1</label>
                                        <label class="btn btn-primary @if($passengers == 2) active @endif">
                                            <input type="radio" name="options" value="2" @if($passengers == 2) checked @endif/>2</label>
                                        <label class="btn btn-primary @if($passengers == 3) active @endif">
                                            <input type="radio" name="options" value="3" @if($passengers == 3) checked @endif/>3</label>
                                        <label class="btn btn-primary @if($passengers == 4) active @endif">
                                            <input type="radio" name="options" value="4" @if($passengers == 4) checked @endif/>4</label>
                                        <label class="btn btn-primary">
                                            <input type="radio" name="options" />4+</label>
                                    </div>

                                    <select name="passengers" class="form-control @if($passengers < 5) hidden @endif">
                                        <option value="">choose one</option>
                                        @for($i = 1; $i <=16; $i++)
                                             <option value="{{$i}}" @if($passengers == $i) selected="selected" @endif>{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-group-select-plus">
                                    <label>Children</label>
                                    <div class="btn-group btn-group-select-num" data-toggle="buttons">
                                        <label class="btn btn-primary @if($children == 1) active @endif">
                                            <input type="radio" name="children_options" value="1">1</label>
                                        <label class="btn btn-primary  @if($children == 2) active @endif">
                                            <input type="radio" name="children_options" value="2">2</label>
                                        <label class="btn btn-primary  @if($children == 3) active @endif">
                                            <input type="radio" name="children_options" value="3">3</label>
                                        <label class="btn btn-primary  @if($children == 4) active @endif">
                                            <input type="radio" name="children_options" value="4">4</label>

                                        <label class="btn btn-primary">
                                            <input type="radio" name="children_options">4+</label>
                                    </div>
                                    <select name="children" class="form-control hidden">
                                        @for($i = 1; $i <=16; $i++)
                                            <option value="{{$i}}" @if($children == $i) selected="selected" @endif>{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-group-select-plus">
                                    <label>Infants</label>
                                    <div class="btn-group btn-group-select-num" data-toggle="buttons">
                                        <label class="btn btn-primary @if($infants == 1) active @endif">
                                            <input type="radio" name="infant_options" value="1">1</label>
                                        <label class="btn btn-primary  @if($infants == 2) active @endif">
                                            <input type="radio" name="infant_options" value="2">2</label>
                                        <label class="btn btn-primary  @if($infants == 3) active @endif">
                                            <input type="radio" name="infant_options" value="3">3</label>
                                        <label class="btn btn-primary  @if($infants == 4) active @endif">
                                            <input type="radio" name="infant_options" value="4">4</label>

                                        <label class="btn btn-primary">
                                            <input type="radio" name="infant_options">4+</label>
                                    </div>
                                    <select name="infants" class="form-control hidden">
                                        @for($i = 1; $i <=16; $i++)
                                            <option value="{{$i}}" @if($infants == $i) selected="selected" @endif>{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary btn-lg" type="submit">Search for Trips</button>
        </form>
    </div>

@stop

@section('scripts')
       <script type="text/javascript">

       function updateUrl(name, value){
            window.location.search = jQuery.query.set(name, value);

            return false;
       }


    $(function() {

       // $("#price-slider").ionRangeSlider({
       //      min: 1000,
       //      max: 20000,
       //      type: 'double',
       //      prefix: "&#8358;",
       //      // maxPostfix: "+",
       //      prettify: false,
       //      hasGrid: true,
       //      from:{{ $price_min }},
       //      to:{{ $price_max }},
       //      onFinish: function (data) {
       //          console.log(data);
       //          updateUrl('filter_price', data.fromNumber+":"+data.toNumber)
       //      },
       //  });

       $("#maxP").focusout(function () {
           var toNumber = $(this).val();
           var fromNumber = $("#minP").val();

           if(fromNumber == "" || fromNumber == null){
                $("#minP").css("border","1px solid red");
                return;
           }
           if(toNumber == "" || toNumber == null){
                $(this).css("border", "1px solid red");
                return;
           }

            if((fromNumber != "" || fromNumber != null) &&(toNumber != "" || toNumber != null)){
                updateUrl('filter_price', fromNumber+":"+toNumber)
            }
       });

       $("#minP").focusout(function () {
           var toNumber = $("#maxP").val();
           var fromNumber = $(this).val();

           if(fromNumber == "" || fromNumber == null){
                $(this).css("border","1px solid red");
                return;
           }
           if(toNumber == "" || toNumber == null){
                $("#maxP").css("border", "1px solid red");
                return;
           }

            if((fromNumber != "" || fromNumber != null) &&(toNumber != "" || toNumber != null)){
                updateUrl('filter_price', fromNumber+":"+toNumber)
            }
       });

       $('.btype').iCheck({
            checkboxClass: 'i-check',
            radioClass: 'i-radio'

        }).on('ifChanged', function(e) {
            // Get the field name
            var isChecked = e.currentTarget.checked;

            if (isChecked == true) {

                console.log(e.currentTarget.value);
                updateUrl('bus_type', e.currentTarget.value)

            }
        });

        // $('.oprad').iCheck({
        //     checkboxClass: 'i-check',
        //     radioClass: 'i-radio'

        // }).on('ifChanged', function(e) {
        //     // Get the field name
        //     var isChecked = e.currentTarget.checked;

        //     if (isChecked == true) {

        //         console.log(e.currentTarget.value);
        //         updateUrl('operator', e.currentTarget.value)

        //     }
        // });
        $('.oprad').change(function (e) {
            var selectedValue = e.currentTarget.value;
            if(selectedValue != "" || selectedValue != null)
                updateUrl('operator', selectedValue);
        })

        $('.bfeats').iCheck({
            checkboxClass: 'i-check',
            radioClass: 'i-radio'

        }).on('ifChanged', function(e) {
            // Get the field name
            var isChecked = e.currentTarget.checked;

            if (isChecked == true) {

                console.log(e.currentTarget.value);
                updateUrl(e.currentTarget.name, 1)

            }else{
                updateUrl(e.currentTarget.name, 0)
            }
        });
   });

    $('.btype').on('click change', function(e) {
        console.log(e.type);
    });

   </script>
@endsection
