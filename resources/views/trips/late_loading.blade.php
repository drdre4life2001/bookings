<!DOCTYPE HTML>
<html class="full">

<head>
    <title>Traveler - Loading</title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
   <!--  <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
     --><!-- /GOOGLE FONTS -->

    <link rel="stylesheet" href="{{ URL::asset('/css/bootstrap.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/styles.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/mystyles.css') }}">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <script src="js/modernizr.js"></script>


</head>

<body class="full">

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">

        <div class="full-page">
            <div class="bg-holder full">
                <div class="bg-mask"></div>
                <div class="bg-img" style="background-image:url({{ asset('img/img16.jpg') }});"></div>
                <div class="bg-holder-content full text-white text-center">
                    <a class="logo-holder" href="index.html">
                        <!-- <img src="{{ asset('img/logo_sm.jpg') }}" alt="Image Alternative text" title="Image Title" /> -->
                        <img src="{{ asset('img/bus-edited.png') }}"  />
                    </a>
                    <div class="full-center">
                        <div class="container">
                            <div class="spinner-clock">
                                <div class="spinner-clock-hour"></div>
                                <div class="spinner-clock-minute"></div>
                            </div>
                            <h2 class="mb5">Looking for trips going to {{ $park->name }} ...</h2>
                            <p class="text-bigger">it will take a couple of seconds</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form action="search" method="get" id="formx">
            <input type="text" value="<?php echo $inputs['oneTo'] ?>" name="oneTo">
            <input type="text" value="<?php echo $inputs['oneFrom'] ?>" name="oneFrom">
            <input type="text" value="<?php if(!empty($inputs['departing'])) echo $inputs['departing'] ?>" name="departing">

            <input type="text" value="<?php if(!empty($inputs['options'])) echo $inputs['options']; ?>" name="options">
            <input type="text" value="<?php echo $inputs['passengers'] ?>" name="passengers">
            <input type="text" value="<?php if(!empty($inputs['children_options'])) echo $inputs['children_options']; ?>" name="children_options">

            <input type="text" value="<?php if(!empty($inputs['infant_options'])) echo $inputs['infant_options']; ?>" name="infant_options">
            <input type="text" value="<?php if(!empty($inputs['oneTo2'])) echo $inputs['oneTo2']; ?>" name="oneTo2">
            <input type="text" value="<?php if(!empty($inputs['oneFrom2'])) echo $inputs['oneFrom2']; ?>" name="oneFrom2">
            <input type="text" value="<?php if(!empty($inputs['oneFrom2'])) echo $inputs['oneFrom2']; ?>" name="oneFrom2">
            <input type="text" value="<?php if(!empty($inputs['start'])) echo $inputs['start']; ?>" name="start">
            <input type="text" value="<?php if(!empty($inputs['end'])) echo $inputs['end']; ?>" name="end">
            <input type="text" value="<?php if(!empty($inputs['options2'])) echo $inputs['options2']; ?>" name="options2">
            <input type="text" value="<?php if(!empty($inputs['passengers2'])) echo $inputs['passengers2']; ?>" name="passengers2">
            <input type="text" value="<?php if(!empty($inputs['infant_options2'])) echo $inputs['infant_options2']; ?>" name="infant_options2">
            <input type="text" value="<?php if(!empty($inputs['infants2'])) echo $inputs['infants2'];?>" name="infants2">
        </form>

    <script src="{{ URL::asset('/js/jquery.js') }}" > </script>
    <script src="{{ URL::asset('/js/bootstrap.js') }}" > </script>
    <script src="{{ URL::asset('/js/slimmenu.js') }}" > </script>

    </div>

    <script type="text/javascript">
    $(document).ready(function(){

       setTimeout(function(){
         $('#formx').submit();
        }, 1500);
        })
    </script>


</body>

</html>


