<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Book Tickets on Bus.com.ng </title>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/stylesheet.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/material-bootstrap-wizard.css') }}">
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


  <link rel="stylesheet" href="{{ URL::asset('css/reset.css') }}"> <!-- Resource style -->
  <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}"> <!-- Resource style -->
  <script
          src="https://code.jquery.com/jquery-2.2.4.min.js"
          integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
          crossorigin="anonymous"></script>
  <script src="{{ URL::asset('js/modernizr.js') }}"></script> <!-- Modernizr -->

	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	 
	
	<link href="https://fonts.googleapis.com/css?family=Asap|Cabin|Lato|Roboto:900|Rubik|Open+Sans:800|Raleway:900|Work+Sans:800|Montserrat|Cantarell:700" rel="stylesheet">
	git
<style type="text/css">
.navbar-default {
  background-color: none;
  border-color: none;
}

</style>
</head>
<body class="search-body">

<header>

        <div class="container">
          <div class="col-lg-4 col-lg-offset-4">
            @if(Session::has('error'))
                <div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Oops!</strong> {{ session::get('error') }}.
                </div>
            @endif


            @if(Session::has('success'))
                <div class="alert alert-success fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Success!</strong> {{ session::get('success') }}.
            </div>

            @endif
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <a href="{{ url('/') }}"><img src="{{asset('/img/bus-logo.png')}}" alt="BUS-LOGO" width="25%" class="logo"></a>
        </div>
        
        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
         <div id="h-side" class="col-lg-8 col-md-8 hidden-sm hidden-xs">
        	<ul>
        	<li><img src="{{ URL::asset('img/mobile-app-development.png') }}" style="margin-top:-3%"></li>
        	<li>
        	
        		<i class="fa fa-facebook-square" aria-hidden="true" style="color:#2c54c7; font-size: 30px"></i>
        		<i class="fa fa-twitter-square" aria-hidden="true" style="color:#19add8; font-size: 30px"></i>
        		<i class="fa fa-google-plus-square" aria-hidden="true" style="color:#e92c2c; font-size: 30px"></i>

        	</li>
        	</ul>
        </div>
            <div id="nav">
             		<a href="#cd-nav" class="cd-nav-trigger" style="background:#f73838; border-radius: 100%; padding: 2%; color:white"> <span style="color:white"></span>
					 <span><!-- used to create the menu icon --></span>
				</a> <!-- .cd-nav-trigger -->
            </div>

            <nav class="cd-nav-container" id="cd-nav">
		<header>
			<h3>Navigation</h3>
			<a href="#0" class="cd-close-nav">Close</a>
		</header>

		<ul class="cd-nav">
			<li class="cd">
				<a href="index.php">
					<span>
						<i class="fa fa-home" style="font-size: 30px; color:#fff"></i>
					</span>

					<em>Home</em>
				</a>
			</li>

			<li data-menu="projects">
				<a href="#">
					<span>
						<i class="fa fa-users" style="font-size: 30px; color:#fff"></i>
					</span>


					<em>About Us</em>
				</a>
			</li>

			<li data-menu="about">
				<a href="#">
					<span>
						<i class="fa fa-question-circle" aria-hidden="true" style="font-size: 30px; color:#fff"></i>
					</span>

					<em>FAQs</em>
				</a>
			</li>

			<li data-menu="services">
				<a href="services.html">
					<span>
						<i class="fa fa-bus" aria-hidden="true" style="font-size: 30px; color:#fff"></i>
					</span>

					<em>NYSC <br />Straight to camp</em>
				</a>
			</li>

			<li data-menu="careers">
				<a href="careers.html">
					<span>
						<i class="fa fa-ticket" aria-hidden="true" style="font-size: 30px; color:#fff"></i>
					</span>

					<em>Bus Ticket</em>
				</a>
			</li>
			<li data-menu="contact">
				<a href="contact.html">
					<span>
						 <i class="fa fa-envelope-open" aria-hidden="true" style="font-size: 30px; color:#fff"></i>
					</span>

					<em>Contact</em>
				</a>
			</li>

			
		</ul> <!-- .cd-3d-nav -->

		<a href="#"><img src="{{ URL::asset('img/navdesign.jpg') }}" width="100%"></a>
	</nav>
            
        </div>
    </header>

  <div id="main-content" style="margin-bottom:0%">
  	
  	<div id="searchdetails-box" style="padding:2%;">
  		 <div class="container">
  		 <h2>Payment & Travelling Details </h2>
  		 </div>
  	</div>
  	<div id="paydetails-box" style="">
  				 <div class="container">
  				  <h2 align="center">
  		 		<img src="{{ URL::asset('img/progress.png') }}" width="50%">

  		</h2>
  		<hr/>
  				 	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding:2% 0%">
  				 			<h1><i class="fa fa-bus"></i> Trip Summary</h1>
  				 			<h2> {{ $trip->name }} </h2>
  				 			<p>{{ $trip->created_at }}</p>
  				 			{{-- <p>Toyota (Hiace)</p> --}}

  				 	</div>

  				 	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  style="padding:2% 0%">
  				 		{{-- <h2 align="right">Seat [ 3 , 4 ]</h2> --}}
  				 		<h2 align="right"><i class="fa fa-user"></i> 1 x Adult(s)<b> ₦ {{ number_format($trip->fare, 2) }} </b></h2>
  				 		<h2 align="right"> Booking Code {{ $trip->trip_code }} </b></h2>
  				 		
  				 		<p align="right">
  				 		<button align="right" class="btn btn-large btn-success" style="margin-top:2%">
  				 		<h1 style="font-size:20px;"> Total Fare: &#8358;{{ number_format($trip->fare, 2) }}</h1>
  				 		</button>
  				 		</p>
  				 	</div>
  				 	
  				 </div>
  	</div>
  	<div id="payment-d">
  			<div class="container">

  			<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
  				<div class="p-details">
  					<h2><i class="fa fa-users"></i> Customer Details</h2>
  					<div class="row">
  						<form style="margin-top: 2%">
	  						<div class="col-md-6">

                  <div class="form-group">
                    <label>Leaving From:</label>
                    <input type="text" disabled="" value="{{ $booking->sourcepark->name }}" name="">
                    {{-- <label name=""> {{ $booking->sourcepark->name }} </label> --}}
                  </div>

	  							<div class="form-group">
                    <label>Booking Code:</label>
                    <input type="text" disabled="" value="{{ $booking->booking->booking_code }}" name="">

                  </div>
	  						</div>
	  						<div class="col-md-6">
	  							<div class="form-group">
                    <label>Going To:</label>
                    <input type="text" disabled="" value="{{ $booking->destpark->name }}" name="">
                  </div>

                  <div class="form-group">
                    <label>Full Name</label>
                    <input type="text" name="" disabled value="{{ $booking->booking->next_of_kin }}">
                  </div>

	  						</div>
  						</form>
  					</div>
  				</div>

  				
  			</div>

  			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  				<div class="p-details" style="padding:6%; margin-top: 10%">
  				<h2><i class="fa fa-credit-card-alt" aria-hidden="true"></i> Payment method</h2>
  				<p style="margin-top:4%">Select payment method</p>

  				<p style="margin-top:4%">
  					<a onclick="pay()"><img src="{{ URL::asset('img/pay-with-paystack2.png') }}" width="100%" style="border:1px solid red; padding: 4%"></a>
  				</p>

  				<p style="margin-top:10%">
            <center>
              <a href="#modal-book" data-toggle="modal" align="right" class="btn btn-large btn-danger" style="margin-top:2%">
              <h1 style="font-size:20px;"> Book on Hold!</h1>
              </a>
            </center>
  					 {{-- <b>Book on Hold</b> --}}
  				</p>
  				</div>
  			</div>
	  	
	  		</div>
  	</div>
  
   

   </div>
    @include('layouts.footer')
{{-- <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Trigger modal</a> --}}
<div class="modal fade" id="modal-book">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <center>
          <h2  style="color: red;" class="modal-title">Book on Hold</h2>
        </center>
      </div>
      <div class="modal-body">
        <span style="color:#222;">
          <p>Booking on hold means your booking is reserved for 5 hours while we await
                                            your payment. If
                                            payment is not received by then, you will forfiet your booking and your
                                            booking code will be invalid.</p>
                                        <p></p>
                                        <p>See the list of banks you can pay to below:</p> <br> <br>
                                        <table class="table table-bordered table-striped table-booking-history">
                                            <thead>
                                            <tr>
                                                <th>Bank</th>
                                                <th>Account number</th>
                                                <th>Account Name</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($banks as $bank)
                                                <tr>

                                                    <td>{{ $bank->name }}</td>
                                                    <td>{{ $bank->acount_number }}</td>
                                                    <td class="text-center">{{ $bank->account_name }}
                                                    </td>
                                                    <td class="text-center"><a
                                                                class="btn btn-default btn-sm sendAcctDetails"
                                                                title="SMS this bank details to me."
                                                                data-payload="{{ $bank->name }},{{$bank->acount_number}},{{ $bank->account_name }}"
                                                                href="#"><i class="fa fa-mobile"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                        <p></p>

                                        @if(!empty($cash_offices))
                                            <p>You can also pay into any of our cash offices listed below:</p>
                                            <table class="table table-bordered table-striped table-booking-history">
                                                <thead>
                                                <tr>
                                                    <th>Contact Name</th>
                                                    <th>Phone</th>
                                                    <th>Address</th>
                                                    <th></th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($cash_offices as $co)
                                                    <tr>

                                                        <td>{{ $co->contact_name }}</td>
                                                        <td>{{ $co->phone }}</td>
                                                        <td class="text-center">{{ $co->address }}
                                                        </td>
                                                        <td class="text-center"><a
                                                                    class="btn  btn-default btn-sm sendCashOffice"
                                                                    title="SMS this Cash Office details to me."
                                                                    data-payload="{{$co->contact_name}}|{{$co->phone}}|{{$co->address}}"
                                                                    href="#"><i class="fa fa-mobile"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                        @endif
                                        
        </span>
      </div>
      <div class="modal-footer">
        <a class="btn btn-danger btn-large" href="{{ route('payment_success', [$booking_code, TRUE]) }}">Book on Hold Now!</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->


<script type="text/javascript">
    window.location.href = "{{ route('payment_success', [$booking_code, TRUE]) }}";
</script>

</body>

</html>