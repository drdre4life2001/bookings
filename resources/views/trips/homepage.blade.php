@extends('layouts.app_home')

@section('content')
    <div id="main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                <!-- <form method="post" action="{{ url('trips/loading') }}">
                    <input type="text" name="name">

                    <input type="submit" value="LET'S GO!">
                </form> -->
                    <div class="wizard-container">
                        <div class="card wizard-card" data-color="red" id="wizard">

                            <form method="POST" action="{{ url('trips/loading') }}" id="homepageSearch">

                                <div class="wizard-navigation">
                                    <ul>
                                        <li><a href="#details" data-toggle="tab">Find a bus</a></li>
                                        <li><a href="#modal-charter-bus" data-toggle="modal">Charter bus</a></li>
                                        <li><a href="#description" data-toggle="tab">Check trip</a></li>
                                    </ul>
                                </div>
                                <img style="position: absolute; display: none;" id="imgLoader"
                                     src="{{asset('img2/loading.gif')}}">
                                <div class="tab-content">
                                    <div class="" id="details">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <h4 class="info-text"> Let's start with the basic details.</h4>

                                                {{--<div class="info-text" style="color: #ff0000; font-size: 10px; ">Date--}}
                                                {{--@if(session()->has('flash_message'))--}}
                                                    {{--<div class="alert alert-success">--}}
                                                        {{--{{dd('okay')}}--}}
                                                        {{--{{ session()->get('flash_message') }}--}}
                                                    {{--</div>--}}
                                                {{--@endif--}}
                                                @if($msg!='')
                                                    <div class="alert alert-danger"  style=" text-align: center; font-size: 10px;";>
                                                        {{$msg}}
                                                    </div>
                                                @endif
                                                {{-- </div> --}}
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-group label-floating">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-map-marker"></i>
                                                </span>
                                                    <div class="form-group ">
                                                        <label class="control-label">Search Depature City</label>
                                                        <select class=" selectpicker " data-live-search="true"
                                                                name="oneFrom" id="sourceSelect"
                                                                style="width:100%; margin: 0%; background-color: white;"
                                                                required>
                                                            <option value="" selected="" data-color="black">Departure
                                                                City
                                                            </option>

                                                            @foreach($parks as $park)
                                                                <option value="{{ $park->id }}">{{ $park->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-user"></i>
                                                </span>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Number of Passengers</label>
                                                        <select class="form-control" name="passengers" required>
                                                            <option disabled="" selected=""></option>
                                                            <option value="1"> 1</option>
                                                            <option value="2"> 2</option>
                                                            <option value="3"> 3</option>
                                                            <option value="4"> 4</option>
                                                            <option value="5"> 5</option>
                                                            <option value="6"> 6</option>
                                                            <option value="7"> 7</option>
                                                            <option value="8"> 8</option>
                                                            <option value="9"> 9</option>
                                                            <option value="10"> 10</option>
                                                            <option value="11"> 11</option>
                                                            <option value="12"> 12</option>
                                                            <option value="13"> 13</option>
                                                            <option value="14"> 14</option>
                                                            <option value="15"> 15</option>
                                                            <option value="...">...</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <br>
                                                <br>

                                                <div class="form-group label-floating">
                                                    <label class="control-label"><i
                                                                class="glyphicon glyphicon-map-marker"></i></label>
                                                    <select class="form-control" name="oneTo" id="destSelect" required>
                                                        <option disabled="" selected=""></option>

                                                        @foreach($parks as $park)
                                                            @if(empty($park))
                                                                <option value="Noroutes">No destination</option>
                                                            @else
                                                                <option value="{{ $park->id }}">{{ $park->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group label-floating">

                                                    <div class="form-group ">
                                                        <!-- Date input -->
                                                        <label class="control-label glyphicon glyphicon-calendar "
                                                               for="date">Date</label>
                                                        <input class="form-control " id="date" name="date"
                                                               placeholder="MM/DD/YYYY" type="text" required/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="tab-pane" id="captain">
                                        <h4 class="info-text">What type of room would you want? </h4>
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-offset-1">
                                                <div class="col-sm-4">
                                                    <div class="choice" data-toggle="wizard-radio" rel="tooltip" title="This is good if you travel alone.">
                                                        <input type="radio" name="job" value="Design">
                                                        <div class="icon">
                                                            <i class="material-icons">weekend</i>
                                                        </div>
                                                        <h6>Single</h6>
                                                    </div>
                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="choice" data-toggle="wizard-radio" rel="tooltip" title="Select this room if you're traveling with your family.">
                                                        <input type="radio" name="job" value="Code">
                                                        <div class="icon">
                                                            <i class="material-icons">home</i>
                                                        </div>
                                                        <h6>Family</h6>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="choice" data-toggle="wizard-radio" rel="tooltip" title="Select this option if you are coming with your team.">
                                                        <input type="radio" name="job" value="Code">
                                                        <div class="icon">
                                                            <i class="material-icons">business</i>
                                                        </div>
                                                        <h6>Business</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="description">
                                        <div class="row">
                                            <h4 class="info-text"> Drop us a small description.</h4>
                                            <div class="col-sm-6 col-sm-offset-1">
                                                <div class="form-group">
                                                    <label>Room description</label>
                                                    <textarea class="form-control" placeholder="" rows="6"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">Example</label>
                                                    <p class="description">"The room really nice name is recognized as being a really awesome room. We use it every sunday when we go fishing and we catch a lot. It has some kind of magic shield around it."</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                                <!--visit to correct button-->
                                <div class="wizard-footer">
                                    <div class="pull-right">
                                        <input type="submit" class='btn btn-next btn-fill btn-danger btn-wd'
                                               value='Search Trips'/>
                                        <!-- <input type='button' class='btn btn-finish btn-fill btn-danger btn-wd' name='finish' value='Finish' /> -->
                                    </div>
                                    <div class="pull-left">
                                        <!-- <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' /> -->

                                        <div class="footer-checkbox">
                                            <div class="col-sm-12">
                                                {{--<div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="optionsCheckboxes">
                                                    </label>
                                                    Subscribe to our newsletter
                                                </div>--}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div> <!-- wizard container -->

                </div>
            </div>

        </div>
        {{-- Modal for Charter BUs Here --}}

        <div class="modal fade" id="modal-charter-bus">
            <div class="modal-dialog" style="padding:2%">
                <div class="modal-content modal-lg" style="=padding:2%">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <center>
                            <h3 class="modal-title" style="color: #f44336;">Charter Bus</h3>
                        </center>
                    </div>
                    <div class="modal-body" style="padding:46px !important">
                        <div id="form-charter">
                            <form method="POST" action="{{ url('charter_payment_success') }}">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-lg form-group-icon-left"><i
                                                    style="color: #f44336;" class="fa fa-map-marker input-icon"></i>
                                            <label>Pick-up Location</label>
                                            <input id="pickup" name="pickup" class="typeahead form-control"
                                                   placeholder="City, State" type="text" required="required"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-lg form-group-icon-left"><i
                                                    style="color: #f44336;" class="fa fa-map-marker input-icon"></i>
                                            <label>Drop-off Location</label>
                                            <input id="dropoff" name="dropoff" class="typeahead form-control"
                                                   placeholder="City, State" type="text" required="required"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="input-daterange" data-date-format="M d, D">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i
                                                        style="color: #f44336;"
                                                        class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                <label>Pick-up Date</label>
                                                <input id="date" type="date" class="form-control" name="start"
                                                       required="required"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i
                                                        style="color: #f44336;"
                                                        class="fa fa-clock-o input-icon input-icon-highlight"></i>
                                                <label>Pick-up Time</label>
                                                <input id="pickup_time" class="time-pick form-control"
                                                       name="pickup_time" value="12:00 AM" type="text"
                                                       required="required"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left">
                                                <label>Prefered Operator</label>
                                                <select name="operator_id" id="" class="select2"
                                                        style="width:100%; color: #f44336;" required="required">
                                                    <option value="">--choose Operator</option>
                                                    @foreach($operators as $operator)
                                                        <option value="{{ $operator->id }}">{{ $operator->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="input-daterange" data-date-format="M d, D">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-left"><i
                                                        style="color: #f44336;"
                                                        class="fa fa-user input-icon input-icon-highlight"></i>
                                                <label>Contact Name</label>
                                                <input id="contact_name" class="form-control" name="contact_name"
                                                       type="text" required="required"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i
                                                        style="color: #f44336;"
                                                        class="fa fa-phone input-icon input-icon-highlight"></i>
                                                <label>Contact Phone</label>
                                                <input id="contact_phone" class="form-control" name="contact_phone"
                                                       type="text" required="required"/>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i
                                                        style="color: #f44336;"
                                                        class="fa fa-envelope input-icon input-icon-highlight"></i>
                                                <label>Contact Email</label>
                                                <input id="contact_email" class="form-control" name="contact_email"
                                                       type="text" required="required"/>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group form-group-lg form-group-icon-left">
                                                    <i style="color: #f44336;"
                                                       class="fa fa-user input-icon input-icon-highlight"></i>
                                                    <label>Next of Kin</label>
                                                    <input id="kin" class="form-control" name="kin" type="text"/>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left">
                                                <i style="color: #f44336;"
                                                   class="fa fa-phone input-icon input-icon-highlight"></i>
                                                <label>Next of Kin Phone</label>
                                                <input id="phone_kin" class="form-control" name="phone_kin"
                                                       type="text"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-ift">
                                                <label>No. of Passengers</label>
                                                <input id="passenger" class="form-control" name="passenger" type="text"
                                                       required="required"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i
                                                        style="color: #f44336;"
                                                        class="fa fa-money input-icon input-icon-highlight"></i>
                                                <label>Budget</label>
                                                <input class="form-control" name="budget" type="text"
                                                       required="required"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i
                                                        style="color: #f44336;"
                                                        class="fa fa-map-marker input-icon input-icon-highlight"></i>
                                                <label>Pick Up Address</label>
                                                <input class="form-control" name="pickup_address" type="text"
                                                       required="required"/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i
                                                        style="color: #f44336;"
                                                        class="fa fa-map-marker input-icon input-icon-highlight"></i>
                                                <label>Drop Off Address</label>
                                                <input class="form-control" name="dropoff_address" type="text"
                                                       required="required"/>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <div class="col-md-offset-3">
                                        <button id="submit_charter" type="submit" class="btn btn-danger btn-lg">Submit
                                            Charter Request
                                        </button>
                                        <div id="outing"></div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop
        {{-- <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Trigger modal</a> --}}
        @section('scripts')
            <script type="text/javascript">
                $('.selectpicker').selectpicker();

                $(document).ready(function () {

                    $('#sourceSelect').change(function () {
                        var sourceParkId = $(this).val();
                        // var destID = $("#destSelect");
                        $.ajax({
                            url: "{{ route('get-dests') }}",
                            type: "post",
                            data: {
                                source_park_id: sourceParkId,
                                _token: '{{ csrf_token() }}'
                            },
                            success: function (response) {
                                // you will get response from your php page (what you echo or print)
                                var $el = $("#destSelect");

                                $el.empty(); // remove old options
//                    console.log(response.constructor);
//                    console.log([[].constructor);
                                if (response.constructor === [].constructor) {
                                    $el.append($("<option></option>")
                                        .attr("value", 0).text('Select A Destination'));
//                        alert('No destination available for the source park!');
                                    $.each(response, function (key, value) {
                                        $el.append($("<option></option>")
                                            .attr("value", value.id).text(value.name));
                                    });
                                } else {
                                    alert("No Destination Available for the Selected Park!")
                                    $el.append($("<option></option>")
                                        .attr("value", 0).text('No Destination Available for the Selected Park!'));
                                }

                                // $('#saveSub').html('Get the Travel Points');
                                // $("#subscribe").modal('hide');
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
//                    console.log(textStatus, errorThrown);
//                    alert('No destination');
                            }
                        });
                    });
                    // $("#submitTrip").click(function(){
                    //     <?php
                    //         $one_from = session(['one_from' => $request->input('oneFrom')]);
                    //         $one_to = session(['one_to' => $request->input('oneTo')]);
                    //         $no_of_passengers = session(['no_of_passengers' => $request->input('no_of_passengers')]);
                    //         $date = session(['date' => $request->input('date')]);

                    //     ?>
                    //     // alert('helo');
                    //     $("#imageLoader").show();
                    // });

                    function subscribe() {

                        var email = $("#sub_email").val();
                        var phone = $("#sub_phone").val();

                        if (typeof email === 'undefined' || !email || typeof phone === 'undefined' || !phone) {

                            alert('Please enter your email and phone number');
                            return false;

                        }

                        $('#saveSub').html('<img src="{{ asset('img/facebook.gif') }}"  alt="Sending....." style="height:30px" >');

                        // console.log(bank+' '+acct_name+' number '+acct_number)
                        $.ajax({
                            url: "{{ route('save-subscribe') }}",
                            type: "post",
                            data: {email: email, phone: phone},
                            success: function (response) {
                                // you will get response from your php page (what you echo or print)
                                console.log(response);
                                $('#saveSub').html('Get the Travel Points');
                                $("#subscribe").modal('hide');


                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log(textStatus, errorThrown);
                                $("#subscribe").modal('hide');
                            }
                        });

                        return false;

                    }


                    $("#subscribe").modal({
                        backdrop: 'static',
                        keyboard: false
                    });


                    $("#check-trip").submit(function (e) {

                        var ajax_image = "<img src=" + "<?php echo asset('img2/loading.gif') ?>" + " id='img-loading' alt='Loading...' />";
                        $("#loading-trip").html(ajax_image);
                        var booking_code = $("#booking_code").val();
                        data = {"booking_code": booking_code};
                        var url = "<?php echo config('api.online.trip-from-booking-id'); ?>";
                        $.post(url, data, function (response) {
                            console.log(response.data)
                            booking = response.data;
                            $("#trip-table").empty();
                            $("<tr></tr>").append($("<td>Booking Status</td>")).append($("<td></td>").text(booking.status)).appendTo("#trip-table");
                            $("<tr></tr>").append($("<td>Name</td>")).append($("<td></td>").text(booking.contact_name)).appendTo("#trip-table");
                            $("<tr></tr>").append($("<td>Phone</td>")).append($("<td></td>").text(booking.contact_phone)).appendTo("#trip-table");
                            $("<tr></tr>").append($("<td>Email</td>")).append($("<td></td>").text(booking.contact_email)).appendTo("#trip-table");
                            $("<tr></tr>").append($("<td>Booking Code</td>")).append($("<td></td>").text(booking.booking_code)).appendTo("#trip-table");
                            $('<tr></tr>').append($("<td>Trip</td>")).append($("<td></td>").text(booking.trip.name)).appendTo("#trip-table");
                            $('<tr></tr>').append($("<td>Unit Cost</td>")).append($("<td></td>").text(booking.unit_cost)).appendTo("#trip-table");
                            $('<tr></tr>').append($("<td>Total Cost</td>")).append($("<td></td>").text(booking.final_cost)).appendTo("#trip-table");
                            $('<tr></tr>').append($("<td>Passengers</td>")).append($("<td></td>").text(booking.passenger_count)).appendTo("#trip-table");
                            $('<tr></tr>').append($("<td>Booking Date</td>")).append($("<td></td>").text(booking.date)).appendTo("#trip-table");
                            $('<tr></tr>').append($("<td>Departure Date</td>")).append($("<td></td>").text(booking.departure_date)).appendTo("#trip-table");
                            $('<tr></tr>').append($("<td>Departure Time</td>")).append($("<td></td>").text(booking.trip.departure_time)).appendTo("#trip-table");
                            $('<tr></tr>').append($("<td>Next of Kin</td>")).append($("<td></td>").text(booking.next_of_kin)).appendTo("#trip-table");
                            $('<tr></tr>').append($("<td>Next of Kin Phone</td>")).append($("<td></td>").text(booking.next_of_kin_phone)).appendTo("#trip-table");


                            $("#img-loading").remove();
                        });
                        e.preventDefault();

                    });
                    $('[data-toggle="tooltip"]').tooltip();

                    $("body").backgroundCycle({
                        imageUrls: [
                            "{{ URL::asset('img/bg2.jpg') }}",
                            "{{ URL::asset('img/bg4.png') }}",
                            "{{ URL::asset('img/bg2.jpg') }}",
                            "{{ URL::asset('img/bg4.png') }}"
                        ],
                        fadeSpeed: 2000,
                        duration: 5000,
                        backgroundSize: SCALING_MODE_COVER
                    });


                    $('#date').datepicker({
                        minDate: 1,
                        dateFormat: 'M d, D'
                    });

                });


            </script>

@stop


