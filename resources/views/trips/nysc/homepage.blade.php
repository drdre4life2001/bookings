@extends('layouts.app')

@section('content')
    <style type="text/css">
        .modal-backdrop {
            opacity: 0.8 !important;
        }
    </style>
    <!-- TOP AREA -->
    <div id="main-content">
      <div class="container">
        @if(!empty($status))
            <br/>
            <div class="alert alert-danger" role="alert">{{$status}}</div>
        @endif
          <div class="row">
                <div class="bg-content col-md-6">
                     <div class="wizard-container">
                                        <div class="card wizard-card" data-color="red" id="wizard">
                                            <form action="nysc_search" method="post" id="homepageSearch">
                                                {{csrf_field()}}
                                                <div class="wizard-navigation">
                                                    <ul>
                                                        <li><a href="#details" data-toggle="tab">Find a Bus To Your State of Deployment</a></li>
                                                        <li><a href="#details" data-toggle="tab">Check Your Booking Details</a></li>
                                                    </ul>
                                                </div>

                                                <div class="tab-content">
                                                    <div class="tab-pane" id="details">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <h4 class="info-text"> Let's start with the basic details.</h4>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group form-group-icon-left">
                                                                            <label>From</label>
                                                                            <select class="form-control" name="oneFrom" id="sourceSelect"
                                                                                    class="select2" style="width:100%"
                                                                                    required>
                                                                                <option value="">--Choose Your Leaving Point-</option>
                                                                                @foreach($parks as $park)
                                                                                    <option value="{{ $park->id }}">{{ $park->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            <!-- <input class="typeahead form-control" placeholder="City, Airport, U.S. Zip" type="text" /> -->
                                                                        </div>
                                                                </div>

                                                            <div class="col-sm-6">
                                                                <div class="form-group form-group-icon-left">
                                                                            <label>To</label>
                                                                            <select  name="oneTo" id="destSelect"
                                                                                    class="select2 form-control" style="width:100%" required>
                                                                                <option value="">--Select Your State of Deployment--
                                                                                </option>
                                                                                @foreach($camps as $park)
                                                                                    <option value="{{ $park->id }}">{{ $park->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            <!-- <input class="typeahead form-control" placeholder="City, Airport, U.S. Zip" type="text" /> -->
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--visit to correct button-->
                                                <div class="wizard-footer">
                                                    <div class="pull-right">
                                                        <input type='' class='btn btn-fill btn-danger btn-wd'  value='Next' />
                                                    </div>
                                                    <div class="pull-left">
                                                        <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />

                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </form>
                                        </div>
                                    </div> <!-- wizard container -->
                </div>
            </div>
            <div class="owl-carousel owl-slider owl-carousel-area visible-lg" id="owl-carousel-slider">
                @foreach($operators as $op)
                    <div class="bg-holder full">
                        <div class="nysc-bg-blur img-responsive" style="background-image:url({{ asset('img/nysc.jpg')  }});"></div>
                        <div class="bg-content">
                            <div class="container">
                                <div class="loc-info text-right hidden-xs hidden-sm hidden">
                                    <h3 class="loc-info-title"><img src="{{ config('api.api-host').'logos/'.$op->img }}" alt="" title="" style="width:100px"/></h3>
                                    <p class="loc-info-weather"><span class="loc-info-weather-num">Top Operator</span><i class="im im-cloudy loc-info-weather-icon"></i>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="bg-img hidden-lg"
                 style="background-image:url(<?php echo asset('/') ?>assets/img/nysc.jpg);"></div>
            <div class="bg-mask hidden-lg"></div>
        </div>
    </div>
    <!-- END TOP AREA  -->
    <div class="gap"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12 hidden-xs hidden-sm">
                <div class="row row-wrap" data-gutter="120">
                    <div class="col-md-6 col-sm-6">
                        <div class="mb30 thumb">
                            <div class="thumb-caption">
                                <h4 class="thumb-title">
                                    <span aria-hidden="true" style="color: red; font-weight: bold;">₦</span>
                                    Best Prices Guaranteed</h4>
                                <p class="thumb-desc">Compare prices from top Bus operators across 30 states over the
                                    nation.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="mb30 thumb">
                            <div class="thumb-caption">
                                <h4 class="thumb-title">
                                    <i class="fa fa-globe" style="color: red;" aria-hidden="true"></i> Camp Straight!!!
                                </h4>
                                <p class="thumb-desc">Get moved straight to the NYSC camp of your choice</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-holder hidden-xs hidden-sm">
        <div class="bg-mask"></div>
        <div class="bg-blur" style="background-image:url();"></div>
    </div>

@stop
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#subscribe").modal({
                backdrop: 'static',
                keyboard: false
            });
        });
        $("#check-trip").submit(function (e) {
            var ajax_image = "<img src=" + "<?php echo asset('img/loading.gif') ?>" + " id='img-loading' alt='Loading...' />";
            $("#loading-trip").html(ajax_image);
            var booking_code = $("#booking_code").val();
            data = {"booking_code": booking_code};
            var url = "<?php echo config('api.online.trip-from-booking-id'); ?>";
            $.post(url, data, function (response) {
                console.log(response.data)
                booking = response.data;
                $("#trip-table").empty();
                $("<tr></tr>").append($("<td>Booking Status</td>")).append($("<td></td>").text(booking.status)).appendTo("#trip-table");
                $("<tr></tr>").append($("<td>Name</td>")).append($("<td></td>").text(booking.contact_name)).appendTo("#trip-table");
                $("<tr></tr>").append($("<td>Phone</td>")).append($("<td></td>").text(booking.contact_phone)).appendTo("#trip-table");
                $("<tr></tr>").append($("<td>Email</td>")).append($("<td></td>").text(booking.contact_email)).appendTo("#trip-table");
                $("<tr></tr>").append($("<td>Booking Code</td>")).append($("<td></td>").text(booking.booking_code)).appendTo("#trip-table");
                $('<tr></tr>').append($("<td>Trip</td>")).append($("<td></td>").text(booking.trip.name)).appendTo("#trip-table");
                $('<tr></tr>').append($("<td>Unit Cost</td>")).append($("<td></td>").text(booking.unit_cost)).appendTo("#trip-table");
                $('<tr></tr>').append($("<td>Total Cost</td>")).append($("<td></td>").text(booking.final_cost)).appendTo("#trip-table");
                $('<tr></tr>').append($("<td>Passengers</td>")).append($("<td></td>").text(booking.passenger_count)).appendTo("#trip-table");
                $('<tr></tr>').append($("<td>Booking Date</td>")).append($("<td></td>").text(booking.date)).appendTo("#trip-table");
                $('<tr></tr>').append($("<td>Departure Date</td>")).append($("<td></td>").text(booking.departure_date)).appendTo("#trip-table");
                $('<tr></tr>').append($("<td>Departure Time</td>")).append($("<td></td>").text(booking.trip.departure_time)).appendTo("#trip-table");
                $('<tr></tr>').append($("<td>Next of Kin</td>")).append($("<td></td>").text(booking.next_of_kin)).appendTo("#trip-table");
                $('<tr></tr>').append($("<td>Next of Kin Phone</td>")).append($("<td></td>").text(booking.next_of_kin_phone)).appendTo("#trip-table");
                $("#img-loading").remove();
            });
            e.preventDefault();
        });
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();

            $("body").backgroundCycle({
                imageUrls: [
                    "{{ URL::asset('img/nysc.png') }}",
                    "{{ URL::asset('img/nysc.png') }}"
                ],
                fadeSpeed: 2000,
                duration: 5000,
                backgroundSize: SCALING_MODE_COVER
            });
        });
    </script>
@stop
