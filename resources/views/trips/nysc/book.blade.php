@extends('layouts.app')

@section('content')

<div id="main-content">
    <div id="icon-box" style="background-image: url('{{URL::asset("img/nysc.png")}}'); height: 250px;">
        <!-- <img src="{{ URL::asset('img/nysc.png') }}" width="100%"> -->
    </div>

    <div id="searchdetails-box">
        <div class="container">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding:2% 0%">
                <p> <i class="fa fa-map-marker" aria-hidden="true"></i>
                    Trip from <b>{{ $trips->park }}</b> to <b>{{ $trips->camp }}
                </p>
                <p> <i class="fa fa-car" aria-hidden="true"></i>
                   {{ $trips->operator }}
                </p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
                <a href="{{ url('/') }}" class="btn btn-large btn-warning"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> <b>Modify Search</b></a>
            </div>
        </div>
    </div>
    <div id="box-aval">
            <div class="container">
                <i class="fa fa-bus" aria-hidden="true"></i> Passenger Details
            </div>
        </div>


<script src="https://js.paystack.co/v1/inline.js"></script>

<div id="payment-d">

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="p-details">
                <h2 class="col-sm-12"><i class="fa fa-users"></i> Corper Details</h2>
                    <form action="new-nysc-booking" method="post" id="nyscForm">
                    {{csrf_field()}}
                        <!-- Form start -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="name">Full Name</label>
                                    <input id="name" name="name" type="text" placeholder="Name" class="form-control input-md" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="appointmentfor">Sex</label>
                                    <select id="appointmentfor" name="gender" class="form-control">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="email">Email Address</label>
                                    <input id="email" name="email" type="text" placeholder="E-Mail" class="form-control input-md" required>
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="date">Phone Number</label>
                                    <input id="phone" name="phone" type="number" placeholder="Phone Number" class="form-control input-md" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="name">Next of Kin</label>
                                    <input id="next-of-kin" name="kin" type="text" placeholder="Name" class="form-control input-md" required>
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="date">Next of Kin Phone Number</label>
                                    <input id="next-of-phone" name="kin_phone" type="number" placeholder="Phone Number" class="form-control input-md" required>
                                </div>
                            </div>
                            <!-- Select Basic -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="time">Preferred Travel Date</label>
                                    <input id="date" name="travel_date" type="date" placeholder="10-12-2017" class="form-control input-md" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="date">Source Park</label>
                                    <input id="date" name="source_park" type="text" value="{{ $trips->park }}" class="form-control input-md" readonly required>
                                    <input id="date" name="source_park_id" type="hidden" value="{{ $trips->source_park_id }}" class="form-control input-md" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="date">Destination</label>
                                    <textarea id="date" name="dest_park"  class="form-control input-md" readonly required>{{ $trips->camp }}</textarea>
                                    <input id="date" name="dest_camp_id" type="hidden" value="{{ $trips->dest_camp_id }}" class="form-control input-md" required>
                                </div>
                            </div>
                            <!-- Select Basic -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="payment_mode">Payment options</label>
                                    <select id="payment_mode" name="payment_mode" class="form-control">
                                        <option value="online">Pay With Your ATM Card</option>
                                        {{--<option value="offline">Bank Deposit (Book On Hold)</option>--}}
                                    </select>
                                </div>
                            </div>

                            <input id="fare" type="hidden" value="{{$trips->fare}}">
                            <input id="transaction_ref" type="hidden" name="transaction_ref">
                            <!-- Button -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button id="singlebutton" name="singlebutton" class="btn btn-danger">Book Your Trip</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- form end -->
                </div>
            </div>
            <div class="col-md-4">
                <div class="p-details">
                    <h2 class="col-sm-12"><i class="fa fa-users"></i> Trip Details</h2>
                
                        <div class="col-sm-12"><img src="{{ config('api.img-host').'logos/'.$trips->operator_logo }}" alt="{{ $trips->operator }}" title="{{ $trips->operator }}" width="100px" height="25px" />
                        </div>
                            <h5 class="black col-sm-12">From: {{ $trips->park }}</h5></br>
                            <h5 class="black col-sm-12">To: {{ $trips->camp }}</h5></br>
                            <h5 class="black col-sm-12">FARE: <strong>  &#x20A6; {{ number_format($trips->fare, 2) }}</strong>
                            </h5></br>
                            <div class="booking-item-airline-logo"><img src="{{ asset('img/pay-with-paystack2.png') }}" alt="Payment Options" title="Payment Options"  /></div>
                </div>
            </div>
        </div>
    </div>

</div>

<style type="text/css">
    .black {
        color: #000;
        margin-bottom: 26px;
    }
    .body {
        overflow-x: hidden;
    }
</style>
    </div>
@stop

@section('scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#singlebutton').click(function(e){
            e.preventDefault();
            var online = $('#payment_mode').val();
            if (online === 'online') {
                
                //alert('yes online');

                var handler = PaystackPop.setup({
                      key: '{{$paystack_key}}',
                      email: $('#email').val(),
                      amount: ($('#fare').val() * 100),
                      ref: ''+Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
                      metadata: {
                         custom_fields: [
                            {
                                display_name: $('#name').val(),
                                phone: $('#phone').val()
                            }
                         ]
                      },
                      callback: function(response){
                        $('#transaction_ref').val(response.reference);
                          document.getElementById('nyscForm').submit();
                      },
                      onClose: function(){
                          alert('window closed');
                      }
                    });
                    handler.openIframe();

            }
        })
    });
</script>

@endsection
