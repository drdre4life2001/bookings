@extends('layouts.app')

@section('content')

<div id="main-content" style="margin-bottom:0%">
  
  <div id="searchdetails-box" style="padding:2%; background: #f3f3f3; color:#318f31">
       <div class="container">
       <h2 align="center">
       @include('bookings.includes.steps')
      </h2>
       
       </div>
  </div>
  <div id="paydetails-box2" style="">
               <div class="container">
              <div class="col-lg-3 col-md-3 hidden-sm hidden-xs"></div>
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="ty-box" style="padding:6%; margin-top: 2%">


                              
                              <h1 align="center" style="margin-top: -2%"> <span style="color:#45c05c">Thanks {{ $passenger->name }} </span><br />
                              Your booking with Bus.com.ng is confirmed.</h1>
                              @if(isset($payment_failure))
                                <p class="alert alert-danger">{{ $payment_failure }}</p>
                              @endif
                              @if($on_hold === true)
                                <p class="text-center book_note"><small>Your Booking was successful and it is on Hold for 5 hours!</small><br/>
                                <small>Your Booking details has been sent to your phone number [{{ $passenger->phone }}] and email address [{{ $passenger->email }}]</small><br/>
                                <small>Please note that Booking on hold means your booking is reserved for 5 hours while we await your payment. If payment is not received by then, you will forfiet your booking and your booking code will be invalid.</small>
                                </p>
                              @else
                                <p class="text-center book_note"><small>Your Booking was successful!</small><br/>
                                <small>Your Booking details has been sent to your phone number [{{ $passenger->phone }}] and email address [{{ $passenger->email }}]</small><br/>
                              @endif
                              <h2 align="center" class="t-dc"><i class="fa fa-bus"></i> Trip Details </h2>
                          <p align="center">{{ $trips->park }} to {{ $trips->camp }} </p>
                          <p align="center">Booking Code: {{ $bookings->booking_code }}</p>
                          <p align="center">Your Email: {{ $passenger->email }}</p>
                          <p align="center">Your Phone: {{ $passenger->phone }}</p>
                          <p align="center">Passenger : (1)</p>
                          <p align="center" style="font-size: 25px; font-weight: bolder">Total : N{{ number_format($trips->fare, 2) }}   </p>
                              {{--<p align="center" style="border-bottom: none">--}}
                                  {{--<button class="btn btn-large btn-danger">--}}
                                      {{--Print Confirmation--}}
                                  {{--</button>--}}
                                  {{--<button class="btn btn-large btn-danger">--}}
                                      {{--Save Confirmation to phone--}}
                                  {{--</button>--}}
                              {{--</p>--}}
                              <p>
                                  For more information on your booking please call us on<strong> <a href='tel:080 30 500 600' value='080 30 500 600' target='_blank'>080 30 500 600</a></strong>, or mail us at <a href='mailto:orders@company.com'>info@bus.com.ng</a>
                              </p>

                          </div>
                          

                  </div>

              
                      <div class="col-lg-3 col-md-3 hidden-sm hidden-xs"></div>
                  
                  
               </div>
  </div>

<style type="text/css">
    .book_note {
        color: red;
        font-size: 14px;
        line-height: 1.3;
    }
</style>

@stop
