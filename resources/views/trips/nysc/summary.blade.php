<div bgcolor='#e4e4e4' text='#ff6633' link='#666666' vlink='#666666' alink='#ff6633' style='margin:0;font-family:Arial,Helvetica,sans-serif;border-bottom:1'>
    <table background='' bgcolor='#e4e4e4' width='100%' style='padding:20px 0 20px 0' cellspacing='0' border='0' align='center' cellpadding='0'>
        <tbody>
        <tr>
            <td>
                <table width='620' border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='#FFFFFF' style='border-radius: 5px;'>
                    <tbody>
                    <tr>
                        <td valign='top' style='color:#404041;line-height:16px;padding:25px 20px 0px 20px'>
                            <p>
                                <section style='position: relative;clear: both;margin: 5px 0;height: 1px;border-top: 1px solid #cbcbcb;margin-bottom: 25px;margin-top: 10px;text-align: center;'>
                                    <h3 align='center' style='margin-top: -12px;background-color: #FFF;clear: both;width: 180px;margin-right: auto;margin-left: auto;padding-left: 15px;padding-right: 15px; font-family: arial,sans-serif;'>
                                        <span>Details</span>
                                    </h3>
                                </section>
                            </p>
                        </td>
                    </tr>
                    <tr align='left' >
                        <td style='color:#404041;font-size:12px;line-height:16px;padding:10px 16px 20px 18px'>
                            <table width='0' border='0' align='left' cellpadding='0' cellspacing='0'>
                                <span><h5 style='color: #848484; font-family: arial,sans-serif; font-weight: 200;'>Passenger Details</h5></span>
                                <tbody>
                                <tr>
                                    <td width='0' align='left' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:0px 0px 3px 0px'>
                                        <strong>Name:</strong>
                                    </td>
                                    <td width='0' align='right' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:0px 5px 3px 5px'>
                                        {{ $passenger->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <td align='left' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:5px 0px 3px 0px;'>
                                        <strong>Phone</strong>
                                    </td>
                                    <td width='62' align='right' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:5px 5px 3px 5px;'>
                                        {{ $passenger->phone }}
                                    </td>
                                </tr>
                                <tr>
                                    <td align='left' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:5px 0px 3px 0px;'>
                                        <strong>E-Mail:</strong>
                                    </td>
                                    <td width='120' align='right' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:5px 5px 3px 5px;'>
                                        {{ $passenger->email }}
                                    </td>
                                </tr>
                                </tbody>
                                <tbody>
                                <tr>
                                    <td width='0' align='left' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:15px 0px 3px 0px'>
                                        <strong>Source Park:</strong>
                                    </td>
                                    <td width='0' align='right' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:15px 5px 3px 5px'>
                                        {{ $trips->park }}
                                    </td>
                                </tr>

                                <tr>
                                    <td align='left' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:5px 0px 3px 0px;'>
                                        <strong>Destination:</strong>
                                    </td>
                                    <td width='62' align='right' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:5px 5px 3px 5px;'>
                                        {{ $trips->camp }}
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <table width='0' border='0' align='right' cellpadding='0' cellspacing='0'>
                                <tbody>
                                <tr>
                                    <td align='left' valign='bottom' style='color:#404041;font-size:13px;line-height:16px;padding:5px 0px 3px 0px'>
                                        <h4><strong>Booking Code:</strong></h4>
                                    </td>
                                    <td width='62' align='right' valign='bottom' style='color:#339933;font-size:13px;line-height:16px;padding:5px 5px 3px 5px'>
                                        <strong><h4>{{ $bookings->booking_code }}</h4></strong>
                                    </td>
                                </tr>
                                @if(isset($status))
                                <tr>
                                    <td align='left' valign='bottom' style='color:#404041;font-size:13px;line-height:16px;padding:5px 0px 3px 0px'>
                                        <h4><strong>Booking Status:</strong></h4>
                                    </td>
                                    <td width='62' align='right' valign='bottom' style='color:#339933;font-size:13px;line-height:16px;padding:5px 5px 3px 5px'>
                                        <strong><h4>{{ $status }}</h4></strong>
                                    </td>
                                </tr>
                                @endif
                                <tr>
                                    <td align='left' valign='bottom' style='color:#404041;font-size:13px;line-height:16px;padding:5px 0px 3px 0px'>
                                        <h2><strong>Fare:</strong></h2>
                                    </td>
                                    <td width='62' align='right' valign='bottom' style='color:#339933;font-size:13px;line-height:16px;padding:5px 5px 3px 5px'>
                                        <strong><h2>&#x20A6;{{ number_format($trips->fare, 2) }}</h2></strong>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width='650' border='0' cellspacing='0' cellpadding='0'>
                                <tbody>
                                <tr>
                                    <td style='color:#404041;font-size:12px;line-height:16px;padding:15px 5px 5px 10px'>
                                        For more information on your booking please call us on<strong> <a href='tel:123 467 8961' value='+123 467 8961' target='_blank'>123 467 8961</a></strong>, or mail us at <a href='mailto:orders@company.com'>support@bus.com.ng</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width='510' border='0' cellspacing='0' cellpadding='0'>
                                <tbody>
                                <tr>
                                    <td style='color:#404041;font-size:12px;line-height:16px;padding:5px 15px 10px 10px'>
                                        <p>
                                            Kind regards,<br>
                                            <strong>The <a href='#' target='_blank'>BUS.com.ng</a> team</strong>
                                        </p>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>

</div>

