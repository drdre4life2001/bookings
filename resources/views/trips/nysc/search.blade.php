@extends('layouts.app')

@section('content')
    
    <div id="main-content">
        <div id="icon-box" style="background-image: url('img/nysc.png'); height: 250px;">
            <!-- <img src="{{ URL::asset('img/nysc.png') }}" width="100%"> -->
        </div>

        <div id="searchdetails-box">
            <div class="container">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding:2% 0%">
                    <p> <i class="fa fa-map-marker" aria-hidden="true"></i>
                        @if(count($trips) > 0)
                            {{ count($trips)}}
                            {{((count($trips) == 1 )?' trip':' trips') }} from {{ $fromPark }} to {{ $toPark }}
                        @endif
                    </p>
                     <span style="color:#ffca4a"> {{$passengers.(($passengers ==1)?' passenger':' passengers') }} </span>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
                    <a href="{{ url('/') }}" class="btn btn-large btn-warning"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> <b>Modify Search</b></a>
                </div>
            </div>
        </div>
        <div id="box-aval">
            <div class="container">
                <i class="fa fa-bus" aria-hidden="true"></i> Available Trips
            </div>
        </div>

    <div class="container">
        <div id="search-result-box">
        @if($trip_count > 0)
            <div class="row">
                @foreach($trips as $trip)

                <div class="bus-result">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img src="{{ config('api.img-host').'logos/'.$trip->operator_logo }}"
                                    alt="{{ $trip->operator }}" title="{{ $trip->operator }}" width="80%" />
                      
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
                        <div class="bs-d">
                            <img src="{{ URL::asset('img/bus-sample.jpg') }}" class="be-img">
                            <h2 class="col-sm-8">Trip</h2>
                            <p class="col-sm-8"> {{ $fromPark }} <small>TO</small> {{ $toPark }}<br/><br/>
                                <span style="font-weight:bolder; color:#f12e0e;">
                    AC  <i class="fa fa-check-square" aria-hidden="true" style="color:#0db23b; font-size: 15px"></i> / Pick Up Available</span>
                            </p>
                        </div>
                    </div>
                    
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                        <div class="bs-d">
                        <h3>FARE: <strong>&#x20A6; {{ number_format($trip->fare, 2) }}</strong></h3>
                        <a type="button" href="{{ route('new-nysc-booking', [$trip->id,$trip->fare]) }}" class="btn btn-danger">Book Now</a>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
</div>
@stop

@section('scripts')
@endsection
