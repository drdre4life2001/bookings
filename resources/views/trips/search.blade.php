@extends('layouts.app')

@section('content')
    <div id="main-content" xmlns="http://www.w3.org/1999/html">
        <div id="icon-box">
            <img src="{{ URL::asset('img/booking-banner.jpg') }}" width="100%">
        </div>

        <div id="searchdetails-box">
            <div class="container">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding:2% 0%">
                    <p> <i class="fa fa-map-marker" aria-hidden="true"></i>
                        @if(count($trips) > 0)
                            {{ count($trips)}}
                            @if(!is_null($round_trip_stage))
                                {{ ($round_trip_stage == 1)?'Ongoing':'Return' }}
                            @endif
                            {{((count($trips) == 1 )?' trip':' trips') }} from {{ $fromParkObj->name }} to {{ $toParkObj->name }}
                        @endif
                    </p>
                    <span class="pass-d"> {{ date('M jS Y', strtotime($_GET['start'])) }} - <i class="fa fa-users" aria-hidden="true"></i> <span style="color:#ffca4a"> {{$passengers.(($passengers ==1)?' passenger':' passengers') }} </span>   </span>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
                    <a href="{{ url('/') }}" class="btn btn-large btn-warning"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> <b>Modify Search</b></a>
                </div>
            </div>
        </div>
        <div id="box-aval">
            <div class="container">
                <i class="fa fa-bus" aria-hidden="true"></i> Available Trips
            </div>
        </div>

        <div class="container">

            <div id="search-result-box">

                @foreach($trips as $trip)

                    <div class="bus-result">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            @if(!empty(config('api.img-host').'logos/'.$trip->operator->img))
                                <img src="{{ config('api.img-host').'logos/'.$trip->operator->img }}" alt="{{ $trip->operator->name }}" title="{{ $trip->operator->name }}" width="80%" />
                            @else
                                <img src="{{ config('api.img-host').'logos/'.$trip->operator->img }}" alt="Image Alternative text" title="Image Title" width="80%" />
                            @endif
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="bs-d">
                                <img src="{{ URL::asset('img/bus-sample.png') }}" class="be-img">
                                <h2 class="col-sm-8">{{ $trip->bus_type->name }}</h2>
                                <p class="col-sm-8"> {{ $trip->sourcepark->name }} <small>TO</small> {{ $trip->destpark->name }}<br/><br/>
                                    <span style="font-weight:bolder; color:#f12e0e;">
                        AC  <i class="fa fa-check-square" aria-hidden="true" style="color:#0db23b; font-size: 15px"></i> / Pick Up Available</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                            <div class="bs-d">

                                <h2 style="float: none; margin-top: 11.5%; ">Departing</h2>
                                <span style="float: left; padding-right: 6%; margin-top: 3%"><i class="fa fa-clock-o" aria-hidden="true" style="font-size: 25px; margin-top:5%; color:#f12e0e; "></i></span>
                                <p style="margin-top:6%">{{ date('gA', strtotime($trip->trip->departure_time)) }} [{{ $trip->trip->trip_position }}]</p>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                            <div class="bs-d">

                                <h2 style="float: none; margin-top: 11.5%; ">Seat Available</h2>
                                <img src="{{ URL::asset('img/seat.png') }}" class="be-img" style="width:12%; margin-top:5%">
                                <p style="margin-top:6%;">{{ $trip->bus_type->no_of_seats }} Seat(s) <b> available </b></p>
                            </div>

                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">

                            <div class="bs-d">

                                <h2 class="row" style="float: none; margin-top: 11.5%; ">Total Fare </h2>
                                <p style="font-size: 16px; float: none">


                                            @if($trip->trip->international !== "1")
                                    <div>
                                        <span class="booking-item-price">&#x20A6;<?php echo number_format( $trip->trip->fare *
                                                $passengers + ( (1 - ($trip->operator->local_children_discount/ 100)) *
                                                    $trip->trip->fare * $children) + $bus_fee  ) ?>
                                            <a class="btn btn-large btn-danger vsb" href="{{ route('new-booking', [$trip->trip->id,
                            $trip->trip->fare + $bus_fee , 0,$passengers, $children, $infants, $date,

                            $end]) }}">Book</a>
                            </span>

                                    @else

                                        <span>Virgin Passport:</span><span class="booking-item-price">&#x20A6;<?php echo number_format(
                                                    $trip->trip->virgin_passport
                                                * $passengers + ( (1 - ($trip->operator->local_children_discount/ 100)) *$trip->trip->virgin_passport * $children) + $bus_fee ) ?>
                                            @if(!empty($end))
                                                <a class="btn btn-large btn-danger vsb" href="{{ route('new-booking', [$trip->trip->id,
                          $trip->trip->virgin_passport + $bus_fee , 'virgin_passport',$passengers, $children, $infants, $date,
                            $end]) }}">Book</a>
                                            @else
                                                <a class="btn btn-large btn-danger vsb" href="{{ route('new-booking', [$trip->trip->id,
                           $trip->trip->virgin_passport + $bus_fee , 'virgin_passport',$passengers,  $children, $infants,
                            $date]) }}">Book</a>
                                            @endif
                            </span>
                                        <span>Regular Passport:</span><span class="booking-item-price">&#x20A6;<?php echo
                                            number_format(
                                                $trip->trip->regular_passport
                                                * $passengers + ( (1 - ($trip->operator->local_children_discount/ 100)) *
                                                    $trip->trip->regular_passport * $children) + $bus_fee   ) ?>
                                            @if(!empty($end))
                                                <a class="btn btn-large btn-danger vsb" href="{{ route('new-booking', [$trip->trip->id,
                            $trip->trip->regular_passport + $bus_fee , 'regular_passport',$passengers, $children, $infants,
                            $date,
                            $end]) }}">Book</a>
                                            @else
                                                <a class="btn btn-large btn-danger vsb" href="{{ route('new-booking', [$trip->trip->id,
                            $trip->trip->regular_passport + $bus_fee, 'regular_passport',$passengers,  $children, $infants,
                            $date]) }}">Book</a>
                                            @endif
                            </span>
                                        <br/>
                                        @if($trip->trip->no_passport !==0)
                                        <span> </span><span class="booking-item-price"><?php
                                            number_format(
                                                $trip->trip->no_passport
                                                * $passengers + ( (1 - ($trip->operator->local_children_discount/ 100)) *
                                                    $trip->trip->no_passport * $children)  + $bus_fee) ?></span>

                                        @if(!empty($end))
                                            <a class="btn btn-large btn-danger vsb" href="{{ route('new-booking', [$trip->trip->id,
                            $trip->trip->no_passport  , 'no_passport', $passengers, $children, $infants, $date,
                            $end]) }}">Book</a>
                                    </span>
                                                @endif
                                        @else
                                            <a class="btn btn-large btn-danger vsb" href="{{ route('new-booking', [$trip->trip->id,
                            $trip->trip->no_passport + $bus_fee , 'no_passport',$passengers,  $children, $infants,
                            $date]) }}">Book</a>
                                        @endif
                                    @endif

                                </p>

                            </div>
                        </div>

                        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content" style="padding: 6%; color:#333">
                                    <p>Select Seat</p>
                                    <p align="center">
                                        <a href="payment.php"><img src="{{ URL::asset('img/seat-booking.jpg') }}" width="100%"></a>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div><!---  NEXT LINE -->

                @endforeach


            </div> <!---  search-result-box  -->

        </div>

    </div>


@stop
