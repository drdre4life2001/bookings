@extends('layouts.app')

@section('content')


<script>
//refresh this page after x seconds to prevent CSRF token expired error
setTimeout(function(){
   window.location.reload(1);
}, 110000);

</script>
    <style type="text/css">
        .modal-backdrop
        {
            opacity:0.8 !important;
        }
    </style>

        <!-- TOP AREA -->
        <div class="top-area show-onload">
            <div class="bg-holder full">
                <!--div class="bg-mask"></div>
                <div class="bg-parallax" style="background-image:url(img/img16.jpg);"></div-->
                <div class="bg-front bg-front-mob-rel">
                <div class="bg-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8" >
                                <div class="search-tabs search-tabs-bg mt50" >
                                    <h1 class=" hidden-xs hidden-sm">Find Your Perfect Trip</h1>
                                    <div class="tabbable" >
                                        <ul class="nav nav-tabs" id="myTab">

                                            <li class="active"><a href="#tab-2" data-toggle="tab"><i class="fa fa-search"></i> <span >Find a Bus Ticket</span></a>
                                            </li>

                                            <li><a href="#tab-4" data-toggle="tab"><i class="fa fa-bus"></i> <span >Charter Bus</span></a>
                                            </li>
                                            @if(Auth::check())
                                            <li><a href="#tab-5" data-toggle="tab"><i class="fa fa-bus"></i> <span >Your Recent Trips</span></a>
                                            </li>
                                            @endif
                                            <li><a href="#tab-6" data-toggle="tab"><i class="fa fa-ticket"></i> <span >Check Trip</span></a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">

                                            <div class="tab-pane fade in active" id="tab-2" >
                                                <h2 class="hidden-xs hidden-sm">Search for Bus Tickets</h2>
                                                <h2 class="hidden-md hidden-lg">Book Your Trip</h2>
                                                <form action="trips/loading" method="post" id="homepageSearch">
            {{csrf_field()}}

                                                    <div class="tabbable">
                                                        <ul class="nav nav-pills nav-sm nav-no-br mb10" id="flightChooseTab">
                                                            <li class="active"><a href="#flight-search-2" class="reset-form" data-toggle="tab">One Way</a>
                                                            </li>
                                                            <li ><a href="#flight-search-1" class="reset-form" data-toggle="tab">Round Trip</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div class="tab-pane fade " id="flight-search-1">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group form-group-lg form-group-icon-left">
                                                                            <label>From</label>
                                                                            <select name="oneFrom2" id="returnSourceSelect" class="select2" style="width:100%">
                                                                                <option value="">--choose park</option>
                                                                                @foreach($parks as $park)
                                                                                <option value="{{ $park->id }}" >{{ $park->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            <!-- <input class="typeahead form-control" placeholder="City, Airport, U.S. Zip" type="text" /> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group form-group-lg form-group-icon-left">
                                                                            <label>To</label>
                                                                            <select  name="oneTo2" id="returnDestSelect" class="select2 " style="width:100%">
                                                                                <option  value="" >--choose park</option>
                                                                                @foreach($parks as $park)
                                                                                <option value="{{ $park->id }}" >{{ $park->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            <!-- <input class="typeahead form-control" placeholder="City, Airport, U.S. Zip" type="text" /> -->
                                                                        </div>
                                                                    </div>



                                                                </div>
                                                                <div class="input-daterange" data-date-format="M d, D">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <div class="form-group form-group-lg form-group-icon-left">
                                                                              <i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                                                <label>Departing</label>
                                                                                <input class="form-control" name="start" type="text"  />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group form-group-lg form-group-icon-left">
                                                                              <i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                                                <label>Returning</label>
                                                                                <input class="form-control" name="end" type="text" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3">
                                                                            <div class="form-group form-group-lg form-group-select-plus">
                                                                                <label>Passengers </label>
                                                                                <div class="btn-group btn-group-select-num" data-toggle="buttons">
                                                                                    <label class="btn btn-primary active">
                                                                                        <input type="radio" name="options2" value="1" checked/>1</label>
                                                                                    <label class="btn btn-primary">
                                                                                        <input type="radio" name="options2"  value="2" />2</label>
                                                                                    <label class="btn btn-primary">
                                                                                        <input type="radio" name="options2" value="3" />3</label>
                                                                                    <label class="btn btn-primary">
                                                                                        <input type="radio" name="options" />3+</label>
                                                                                </div>
                                                                                <select name="passengers2" class="form-control hidden">
                                                                                    <option value="">Choose one</option>
                                                                                    <option value="1">1</option>
                                                                                    <option value="2">2</option>
                                                                                    <option value="3">3</option>
                                                                                    <option value="4">4</option>
                                                                                    <option value="5">5</option>
                                                                                    <option value="6">6</option>
                                                                                    <option value="7">7</option>
                                                                                    <option value="8">8</option>
                                                                                    <option value="9">9</option>
                                                                                    <option value="10">10</option>
                                                                                    <option value="11">11</option>
                                                                                    <option value="12">12</option>
                                                                                    <option value="13">13</option>
                                                                                    <option value="14">14</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group form-group-lg form-group-select-plus">
                                                                                <label>Children</label>
                                                                                <div class="btn-group btn-group-select-num" data-toggle="buttons">
                                                                                    <label class="btn btn-primary">
                                                                                        <input type="radio" name="infant_options2" value="1">1</label>
                                                                                    <label class="btn btn-primary">
                                                                                        <input type="radio" name="infant_options2" value="2">2</label>
                                                                                    <label class="btn btn-primary">
                                                                                        <input type="radio" name="infant_options2" value="3">3</label>
                                                                                    <label class="btn btn-primary">
                                                                                        <input type="radio" name="infant_options2">3+</label>
                                                                                </div>
                                                                                <select name="infants2" class="form-control hidden">
                                                                                    <option>1</option>
                                                                                    <option>2</option>
                                                                                    <option>3</option>
                                                                                    <option>4</option>
                                                                                    <option>5</option>
                                                                                    <option>6</option>
                                                                                    <option>7</option>
                                                                                    <option>8</option>
                                                                                    <option>9</option>
                                                                                    <option>10</option>
                                                                                    <option>11</option>
                                                                                    <option>12</option>
                                                                                    <option>13</option>
                                                                                    <option>14</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                         <div class="col-md-3">
                                                                            <div class="form-group form-group-lg form-group-select-plus">
                                                                                <label>Children</label>
                                                                                <div class="btn-group btn-group-select-num" data-toggle="buttons">
                                                                                    <label class="btn btn-primary">
                                                                                        <input type="radio" name="infant_options2" value="1">1</label>
                                                                                    <label class="btn btn-primary">
                                                                                        <input type="radio" name="infant_options2" value="2">2</label>
                                                                                    <label class="btn btn-primary">
                                                                                        <input type="radio" name="infant_options2" value="3">3</label>
                                                                                    <label class="btn btn-primary">
                                                                                        <input type="radio" name="infant_options2">3+</label>
                                                                                </div>
                                                                                <select name="infants2" class="form-control hidden">
                                                                                    <option>1</option>
                                                                                    <option>2</option>
                                                                                    <option>3</option>
                                                                                    <option>4</option>
                                                                                    <option>5</option>
                                                                                    <option>6</option>
                                                                                    <option>7</option>
                                                                                    <option>8</option>
                                                                                    <option>9</option>
                                                                                    <option>10</option>
                                                                                    <option>11</option>
                                                                                    <option>12</option>
                                                                                    <option>13</option>
                                                                                    <option>14</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- ONE TRIP -->
                                                            <div class="tab-pane fade in active" id="flight-search-2">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group form-group-lg form-group-icon-left">
                                                                            <label>From</label>
                                                                            <select name="oneFrom" id="sourceSelect" class="select2" style="width:100%" onchange="loadDests('sourceSelect', 'destSelect')">
                                                                                <option value="">--choose park</option>
                                                                                @foreach($parks as $park)
                                                                                <option value="{{ $park->id }}" >{{ $park->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            <!-- <input class="typeahead form-control" placeholder="City, Airport, U.S. Zip" type="text" /> -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group form-group-lg form-group-icon-left">
                                                                            <label>To</label>
                                                                            <select name="oneTo" id="destSelect" class="select2" style="width:100%">
                                                                                <option value="">--choose park</option>
                                                                                @foreach($parks as $park)
                                                                                <option value="{{ $park->id }}" >{{ $park->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            <!-- <input class="typeahead form-control" placeholder="City, Airport, U.S. Zip" type="text" /> -->
                                                                        </div>
                                                                    </div>
                                                                     <div class="col-md-4">
                                                                        <div class="form-group form-group-lg form-group-icon-left">
                                                                          <i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                                            <label>Departing</label>
                                                                            <!--<input class="date-pick form-control"
                                                                            data-date-format="M d, D" type="text"  /> -->
                                                                            <script>
                                                                                $( "#datepicker" ).datepicker();
                                                                            </script>
                                                                            <input id="departing-datepicker" name="departing" class="form-control" type="text" required="required" />


                                                                            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
                                                                           <script src="//code.jquery.com/jquery-1.12.4.js"></script>
                                                                           <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                                                                              <script>
                                                                              //enable only tomorrow
                                                                              $( "#departing-datepicker" ).datepicker({ minDate: 1, maxDate: "+1D",dateFormat: 'M d, D' });
                                                                              </script>
                                                                        </div>
                                                                    </div>
                                                                    <script type="text/javascript">
                                                                    //prevent empty form submission
                                                                    window.onload = function() {
                                                                       if (window.jQuery) {
                                                                           // jQuery is loaded
                                                                           $('#submitDestButton').attr('disabled', 'disabled');

                                                                           $("#sourceSelect" ).change(function() {
                                                                           $("#destSelect" ).change(function() {
                                                                              $('#submitDestButton').removeAttr('disabled');
                                                                            });
                                                                            });

                                                                            $("#returnSourceSelect" ).change(function() {
                                                                                  $("#returnDestSelect" ).change(function() {
                                                                                   $('#submitDestButton').removeAttr('disabled');
                                                                                 });

                                                                             });


                                                                       } else {
                                                                        location.reload(); //if jquery Did not load
                                                                       }
                                                                    }

                                                                    </script>
                                                                </div>
                                                                <div class="row">
                                                                   <div class="col-md-4">
                                                                        <div class="form-group form-group-lg form-group-select-plus">
                                                                            <label>Adults <small>( > 11 years old)</small></label>
                                                                            <select name="passengers" class="form-control">
                                                                                <option value="1">1</option>
                                                                                <option value="2">2</option>
                                                                                <option value="3" >3</option>
                                                                                <option value="4" >4</option>
                                                                                <option value="5">5</option>
                                                                                <option value="6">6</option>
                                                                                <option value="7" >7</option>
                                                                                <option value="8" >8</option>
                                                                                <option value="9">9</option>
                                                                                <option value="10">10</option>
                                                                                <option value="11">11</option>
                                                                                <option value="12">12</option>
                                                                                <option value="13">13</option>
                                                                                <option value="14">14</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group form-group-lg form-group-select-plus">
                                                                            <label>Children <small>( 4 - 10 years old)</small></label>

                                                                            <select name="children" class="form-control ">
                                                                                <option value="">choose one</option>
                                                                                <option>1</option>
                                                                                <option>2</option>
                                                                                <option>3</option>
                                                                                <option>4</option>
                                                                                <option>5</option>
                                                                                <option>6</option>
                                                                                <option>7</option>
                                                                                <option>8</option>
                                                                                <option>9</option>
                                                                                <option>10</option>
                                                                                <option>11</option>
                                                                                <option>12</option>
                                                                                <option>13</option>
                                                                                <option>14</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                     <div class="col-md-4">
                                                                        <div class="form-group form-group-lg form-group-select-plus">
                                                                            <label>Infants <small>( < 3 years)</small></label>

                                                                            <select name="infants" class="form-control ">
                                                                                <option value="">choose one</option>
                                                                                <option>1</option>
                                                                                <option>2</option>
                                                                                <option>3</option>
                                                                                <option>4</option>
                                                                                <option>5</option>
                                                                                <option>6</option>
                                                                                <option>7</option>
                                                                                <option>8</option>
                                                                                <option>9</option>
                                                                                <option>10</option>
                                                                                <option>11</option>
                                                                                <option>12</option>
                                                                                <option>13</option>
                                                                                <option>14</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <button class="btn btn-primary btn-lg submitDestButton" id="submitDestButton" disabled type="submit">Search for Trips</button>
                                                </form>
                                            </div>

                                           <div class="tab-pane fade" id="tab-4">
                                                <h2>Charter a Bus</h2>
                                                {!! Form::open(array('route' => 'charter_payment_success')) !!}

                                              <div id="form-charter">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                                                                <label>Pick-up Location</label>
                                                                <input id="pickup" name="pickup" class="typeahead form-control" placeholder="City, State" type="text" required="required" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                                                                <label>Drop-off Location</label>
                                                                <input id="dropoff" name="dropoff" class="typeahead form-control" placeholder="City, State" type="text" required="required" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="input-daterange" data-date-format="M d, D">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                                    <label>Pick-up Date</label>
                                                                    <input id="pickup_date" class="form-control" name="start" type="text" required="required" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-clock-o input-icon input-icon-highlight"></i>
                                                                    <label>Pick-up Time</label>
                                                                    <input id="pickup_time" class="time-pick form-control" name="pickup_time" value="12:00 AM" type="text" required="required" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="form-group form-group-lg form-group-icon-left">
                                                                    <label>Prefered Operator</label>
                                                                    <select name="operator_id" id="" class="select2" style="width:100%" required="required" >
                                                                                <option value="">--choose Operator</option>
                                                                                @foreach($operators as $operator)
                                                                                <option value="{{ $operator->id }}" >{{ $operator->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                     </div>
                                                            </div>

                                                        </div>
                                                    </div>



                                                    <div class="input-daterange" data-date-format="M d, D">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group form-group-lg form-group-left"><i class="fa fa-user input-icon input-icon-highlight"></i>
                                                                    <label>Contact Name</label>
                                                                    <input id="contact_name" class="form-control" name="contact_name" type="text" required="required" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-phone input-icon input-icon-highlight"></i>
                                                                    <label>Contact Phone</label>
                                                                    <input id="contact_phone" class="form-control" name="contact_phone" type="text" required="required" />
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group form-group-lg form-group-icon-left">
                                                                <i class="fa fa-user input-icon input-icon-highlight"></i>
                                                                    <label>Next of Kin</label>
                                                                   <input id="kin" class="form-control" name="kin" type="text" />
                                                                   </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group form-group-lg form-group-icon-left">
                                                                <i class="fa fa-phone input-icon input-icon-highlight"></i>
                                                                    <label>Next of Kin Phone</label>
                                                                   <input id="phone_kin" class="form-control" name="phone_kin" type="text" />
                                                                   </div>
                                                            </div>


                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group form-group-lg form-group-ift">
                                                                    <label>No. of Passengers</label>
                                                                    <input id="passenger" class="form-control" name="passenger" type="text" required="required" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-money input-icon input-icon-highlight"></i>
                                                                    <label>Budget</label>
                                                                    <input class="form-control" name="budget" type="text" required="required" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-highlight"></i>
                                                                    <label>Pick Up Address</label>
                                                                    <input class="form-control" name="pickup_address" type="text" required="required" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-highlight"></i>
                                                                    <label>Drop Off Address</label>
                                                                    <input class="form-control" name="dropoff_address" type="text" required="required" />
                                                                </div>
                                                            </div>
                                                    </div>

                                                    <button id="submit_charter" type="submit" class="btn btn-primary btn-lg" >Submit Charter Request</button>
                                                    <div id="outing"></div>
{{ Form::close() }}

                                                    </div>
                                                   </div>

                                            </div>
                                            <div class="tab-pane fade" id="tab-5">
                                                <div class="table-responsive">
                                                  <table class="table table-bordered table-striped table-booking-history">
                                                    <thead>
                                                        <tr>
                                                            <th>Trip</th>
                                                            <th>Booking Code</th>
                                                            <th>Status</th>
                                                            <th>Date</th>
                                                            <th>Total Cost</th>
                                                            {{-- <th>Action</th> --}}
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php if(isset($bookings)): ?>
                                                        @foreach ($bookings->results as $booking)
                                                            <tr>
                                                                <td class="booking-history-type">
                                                                    {{$booking->trip->name}}
                                                                </td>
                                                                <td class="booking-history-title">{{$booking->booking_code}}</td>
                                                                <td>{{$booking->status}}</td>
                                                                <td>{{$booking->date}}</td>
                                                                <td>{{$booking->final_cost}}</td>

                                                            </tr>
                                                            <!-- Modal -->
                                    <div id="feedbackModal{{$booking->id}}" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Give Feedback</h4>
                                          </div>

                                          <div class="modal-body">
                                            <form method="post" action="save_customer_feedback">
                                                <input type="hidden" name="operator_id" value="">
                                                {{-- <input type="hidden" name="passenger_count" id="p_count" value="{{$psg}}"> --}}
                                                <input type="hidden" name="booking_id" value="{{$booking->id}}">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Rating</label>
                                                            <div class="rating">
                                                                <span>&#9734;</span>
                                                                <span>&#9734;</span>
                                                                <span>&#9734;</span>
                                                                <span>&#9734;</span>
                                                                <span>&#9734;</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <textarea name="feedback" class="form-control" rows="5"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <select class="form-control" name="rating" required>
                                                                <option>Rate Trip</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" class="btn btn-success" value="Submit Feedback"/>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                            </form>
                                        </div>

                                      </div>
                                    </div>

                                     <div id="bookingModal{{$booking->id}}" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Booking Details</h4>
                                          </div>

                                          <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6"><b>Trip Name</b></div>
                                                    <div class="col-md-6">{{$booking->trip->name}}</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6"><b>Booking Code</b></div>
                                                    <div class="col-md-6">
                                                        {{$booking->booking_code}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6"><b>Passenger</b></div>
                                                    <div class="col-md-6">
                                                        {{$booking->contact_name}}
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>

                                      </div>
                                    </div>
                                            @endforeach
                                            <?php endif ?>
                                                    </tbody>

                                                  </table>
                                                </div>
                                            </div>

                                            <div class="tab-pane fade" id="tab-6"  style="overflow-y: scroll; height: 400px;">
                                                <div class="row">
                                                <form class="form-inline" id="check-trip">
                                                    <div class="form-group">
                                                        <input class="form-control" placeholder="Enter Booking Code" name="booking_code" id="booking_code" type="text" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="btn btn-primary btn-lg" value="Check Trip"  type="submit" />
                                                    </div>
                                                    <div class="form-group">
                                                        <div id="loading-trip" style="width:100px;"></div>
                                                    </div>
                                                </form>
                                                <div style="margin-bottom:10px;"></div>
                                                    <div class="table-responsive"   >
                                                        <table id="trip-table"  class="table table-bordered table-striped table-booking-history ">
                                                        </table>
                                                    </div>

                                                </div>
                                              </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="owl-carousel owl-slider owl-carousel-area visible-lg" id="owl-carousel-slider">
                @foreach($operators_full as $op)
                    <div class="bg-holder full" >
                        <div class="bg-mask"></div>
                        <div class="bg-blur" style="background-image:url({{ asset('img/img16.jpg')  }});"></div>
                        <div class="bg-content">
                            <div class="container">
                                <div class="loc-info text-right hidden-xs hidden-sm hidden">
                                    <h3 class="loc-info-title"><img src="{{ config('api.api-host').'logos/'.$op->img }}" alt="" title="" style="width:100px" /></h3>
                                    <p class="loc-info-weather"><span class="loc-info-weather-num">Top Operator</span><i class="im im-cloudy loc-info-weather-icon"></i>
                                    </p>
                                <!--    <ul class="loc-info-list">
                                        <li><a href="#"><i class="fa fa-building-o"></i> 125 Hotels from $42/night</a>
                                        </li>
                                        <li><a href="#"><i class="fa fa-home"></i> 270 Rentals from $60/night</a>
                                        </li>
                                        <li><a href="#"><i class="fa fa-car"></i> 109 Car Offers from $34/day</a>
                                        </li>
                                        <li><a href="#"><i class="fa fa-bolt"></i> 195 Activities this Week</a>
                                        </li>
                                    </ul><a class="btn btn-white btn-ghost mt10" href="#"><i class="fa fa-angle-right"></i> See Top Operators</a>
                                  -->
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach


                </div>
                <div class="bg-img hidden-lg" style="background-image:url(<?php echo asset('/') ?>assets/img/img16.jpg);"></div>
                <div class="bg-mask hidden-lg"></div>
        </div>
        </div>
        <!-- END TOP AREA  -->

        <div class="gap"></div>


        <div class="container">
            <div class="row">

              <div class="col-md-10 col-md-offset-1 col-sm-12 hidden-xs hidden-sm">
                  <div class="row row-wrap" data-gutter="120">
                      <div class="col-md-6 col-sm-6">
                          <div class="mb30 thumb">
                              <div class="thumb-caption">
                                  <h4 class="thumb-title">
                                    <span aria-hidden="true" style="color: red; font-weight: bold;">₦</span>
                                    Best Prices Guaranteed</h4>
                                  <p class="thumb-desc">Compare prices from top Bus operators across 30 states over the nation.</p>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-6 col-sm-6">
                          <div class="mb30 thumb">
                            <!--<i class="fa fa-briefcase box-icon-left round box-icon-big box-icon-border animate-icon-top-to-bottom"></i> -->

                              <div class="thumb-caption">
                                  <h4 class="thumb-title">
                                    <i class="fa fa-globe" style="color: red;" aria-hidden="true"></i> Earn Rewards</h4>
                                  <p class="thumb-desc">Register for free to book your tickets now and earn exiting rewards and redeem loyalty.</p>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-6 col-sm-6">
                          <div class="mb30 thumb">
                            <!--<i class="fa fa-send box-icon-left round box-icon-big box-icon-border animate-icon-top-to-bottom"></i> -->
                              <div class="thumb-caption">
                                  <h4 class="thumb-title">
<i class="fa fa-map-marker" style="color: red;" aria-hidden="true"></i>
                                    Great Destinations</h4>
                                  <p class="thumb-desc">Book and travel to over 30 states across the Nation and Internationally to Ghana, Benin, Togo and more.</p>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-6 col-sm-6">
                              <div class="thumb-caption">
                                  <h4 class="thumb-title">
                                    <i class="fa fa-thumbs-o-up" aria-hidden="true"  style="color: red;"></i> Book Smarter</h4>
                                  <p class="thumb-desc">Sign up for free and gain access to your customized travel history and itinerary. Repeat and scheduled trips just got a lot easier.</p>
                              </div>
                          </div>
                      </div>
                  </div>


            </div>
        </div>

        <div class="bg-holder hidden-xs hidden-sm">
            <div class="bg-mask"></div>
            <div class="bg-blur" style="background-image:url();"></div>
            <div class="bg-content">

                <div class="container">
                    <div class="gap"></div>
                    <h1 class="text-center text-white mb20">Recent Customer Testimonials</h1>
                    <div class="row row-wrap">

                        @foreach($feedbacks as $feedback)
                          <div class="col-md-4">
                              <!-- START TESTIMONIAL -->
                              <div class="testimonial text-white">
                                  <div class="testimonial-inner">
                                      <blockquote>
                                          <p>{{ $feedback->feedback }} </p>
                                      </blockquote>
                                  </div>
                                  <div class="testimonial-author">
                                      <!-- <img src="" alt="Image Alternative text" title="Flare lens flare" /> -->
                                      <p class="testimonial-author-name">{{ $feedback->name }}</p>
                                  </div>
                              </div>
                              <!-- END TESTIMONIAL -->
                          </div>
                        @endforeach
                            </div>


                    </div>
                    <div class="gap-small gap"></div>
                </div>


            </div>
        </div>


      <!--   <div class="container">
            <div class="gap"></div>
            <h2 class="text-center">Top Operators</h2>
            <div class="gap">
                <div class="row row-wrap">
                    <div class="col-md-3 col-xs-6">
                        <div class="thumb">
                            <header class="thumb-header">
                                <a class="hover-img curved" href="#">
                                    <img src="{{ asset('img/operators/goodisgood.jpg') }}" alt="Image Alternative text" title="Upper Lake in New York Central Park" /><i class="fa fa-plus box-icon-white box-icon-border hover-icon-top-right round"></i>
                                </a>
                            </header>
                            <div class="thumb-caption">
                                <h4 class="thumb-title">God Is Good</h4>
                                <p class="thumb-desc">Scelerisque montes class curabitur class aenean aliquam eu</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="thumb">
                            <header class="thumb-header">
                                <a class="hover-img curved" href="#">
                                    <img src="{{ asset('img/operators/three.jpg') }}" alt="Image Alternative text" title="lack of blue depresses me" /><i class="fa fa-plus box-icon-white box-icon-border hover-icon-top-right round"></i>
                                </a>
                            </header>
                            <div class="thumb-caption">
                                <h4 class="thumb-title">Chisco</h4>
                                <p class="thumb-desc">Condimentum odio eget curabitur scelerisque vivamus ipsum congue</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="thumb">
                            <header class="thumb-header">
                                <a class="hover-img curved" href="#">
                                    <img src="{{ asset('img/operators/ifesinachi.jpg') }}" alt="Image Alternative text" title="people on the beach" /><i class="fa fa-plus box-icon-white box-icon-border hover-icon-top-right round"></i>
                                </a>
                            </header>
                            <div class="thumb-caption">
                                <h4 class="thumb-title">Ifesnichi</h4>
                                <p class="thumb-desc">Ornare cras scelerisque volutpat nulla porttitor commodo cubilia</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <div class="thumb">
                            <header class="thumb-header">
                                <a class="hover-img curved" href="#">
                                    <img src="{{ asset('img/operators/abc.jpg') }}" alt="Image Alternative text" title="the journey home" /><i class="fa fa-plus box-icon-white box-icon-border hover-icon-top-right round"></i>
                                </a>
                            </header>
                            <div class="thumb-caption">
                                <h4 class="thumb-title">ABC Transport</h4>
                                <p class="thumb-desc">Dictumst risus montes ipsum faucibus vel sodales cubilia</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

@stop

@section('scripts')
<script type="text/javascript">

    function  subscribe(){

        var email = $("#sub_email").val();
        var phone = $("#sub_phone").val();

        if (typeof email === 'undefined' || !email || typeof phone === 'undefined' || !phone){

            alert('Please enter your email and phone number');
            return false;

        }

        $('#saveSub').html('<img src="{{ asset('img/facebook.gif') }}"  alt="Sending....." style="height:30px" >');

        // console.log(bank+' '+acct_name+' number '+acct_number)
        $.ajax({
            url: "{{ route('save-subscribe') }}",
            type: "post",
            data: {email:email, phone:phone},
            success: function (response) {
               // you will get response from your php page (what you echo or print)
               console.log(response);
               $('#saveSub').html('Get the Travel Points');
               $("#subscribe").modal('hide');


            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
               $("#subscribe").modal('hide');
            }
        });

        return false;

    }

    function loadDests(sourceId, destID){

        var sourceParkId = $("#"+sourceId).val();
        console.log(sourceParkId);

         $.ajax({
            url: "{{ route('get-dests') }}",
            type: "post",
            data: {
              source_park_id:sourceParkId,
              _token: '{{ csrf_token() }}'
            },
            success: function (response) {
               // you will get response from your php page (what you echo or print)
               console.log(response);
               var $el = $("#"+destID);
                $el.empty(); // remove old options
				$el.append($("<option></option>")
                     .attr("value", 0).text('Select A Destination'));
                $.each(response, function(key,value) {
                  $el.append($("<option></option>")
                     .attr("value", value.id).text(value.name));
                });
               // $('#saveSub').html('Get the Travel Points');
               // $("#subscribe").modal('hide');
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);

            }
        });


    }


        $(document).ready( function() {
            $("#subscribe").modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        $("#check-trip").submit(function(e){

            var ajax_image = "<img src="+"<?php echo asset('img/loading.gif') ?>"+" id='img-loading' alt='Loading...' />";
            $("#loading-trip").html(ajax_image);

            var booking_code = $("#booking_code").val();
            data = {"booking_code": booking_code};
            var url = "<?php echo config('api.online.trip-from-booking-id'); ?>";
            $.post(url,data, function(response){
               console.log(response.data)
               booking = response.data;
               $("#trip-table").empty();
			   $("<tr></tr>").append($("<td>Booking Status</td>")).append($("<td></td>").text(booking.status)).appendTo("#trip-table");
               $("<tr></tr>").append($("<td>Name</td>")).append($("<td></td>").text(booking.contact_name)).appendTo("#trip-table");
               $("<tr></tr>").append($("<td>Phone</td>")).append($("<td></td>").text(booking.contact_phone)).appendTo("#trip-table");
               $("<tr></tr>").append($("<td>Email</td>")).append($("<td></td>").text(booking.contact_email)).appendTo("#trip-table");
               $("<tr></tr>").append($("<td>Booking Code</td>")).append($("<td></td>").text(booking.booking_code)).appendTo("#trip-table");
               $('<tr></tr>').append($("<td>Trip</td>")).append($("<td></td>").text(booking.trip.name)).appendTo("#trip-table");
               $('<tr></tr>').append($("<td>Unit Cost</td>")).append($("<td></td>").text(booking.unit_cost)).appendTo("#trip-table");
               $('<tr></tr>').append($("<td>Total Cost</td>")).append($("<td></td>").text(booking.final_cost)).appendTo("#trip-table");
               $('<tr></tr>').append($("<td>Passengers</td>")).append($("<td></td>").text(booking.passenger_count)).appendTo("#trip-table");
               $('<tr></tr>').append($("<td>Booking Date</td>")).append($("<td></td>").text(booking.date)).appendTo("#trip-table");
               $('<tr></tr>').append($("<td>Departure Date</td>")).append($("<td></td>").text(booking.departure_date)).appendTo("#trip-table");
               $('<tr></tr>').append($("<td>Departure Time</td>")).append($("<td></td>").text(booking.trip.departure_time)).appendTo("#trip-table");
               $('<tr></tr>').append($("<td>Next of Kin</td>")).append($("<td></td>").text(booking.next_of_kin)).appendTo("#trip-table");
               $('<tr></tr>').append($("<td>Next of Kin Phone</td>")).append($("<td></td>").text(booking.next_of_kin_phone)).appendTo("#trip-table");


               $("#img-loading").remove();
            });
            e.preventDefault();

        });
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
 });
    </script>

    @stop
