@extends('layouts.app')

@section('content')

<div class="gap"></div>

<div class="container">
  <div class="row">
    <div class="col-md-8">

    <h1>Bus.com.ng Privacy Statement</h1>
    <p>
    This Privacy Statement (“Privacy Statement”) explains how Transport Software Limited (“TSL,” “Bus.com” “us,” “our,” and “we”) uses your information and applies to all who use our Web site – www.bus.com or any of our described online activities we own or control (collectively, the “Site”). This Privacy Statement is incorporated in our Terms of Use, which also apply when you use our Site.
    By using the Site, you agree to the terms of this Privacy Statement. If you do not agree with the practices described in this Privacy Statement, please do not provide us with your personal information or interact with the Site.  Please note that we may modify this Privacy Statement at any time.
    <h3> 1. Types of Information We Collect </h3>
    We will collect information, including but not limited to Personal Information, when you interact with us and the Site. This information may include your title, name, gender, date of birth, email address, postal address, delivery address (if different), telephone number, mobile number, fax number, payment details, payment card details or bank account details.
   </p>  <p>
    <h3> 2. Your Choices </h3>
    We think that you benefit from a more personalized experience when we know more about you and what you like. However, you can limit the information you provide to TSL, and you can limit the communications that TSL sends to you.
    (a) Commercial E-mails
    You may choose not to receive commercial e-mails from us by following the instructions contained in any of the commercial e-mails we send or by logging into your account and adjusting your e-mail preferences. Please note that even if you unsubscribe from commercial email messages, we may still e-mail you non-commercial e-mails related to your account and your transactions on the Site. You may update your subscription preferences at any time.
    (b) Cookies and Tracking
    You may manage how your browser handles Cookies by adjusting its privacy and security settings. Browsers are different, so refer to instructions related to your browser to learn about cookie-related and other privacy and security settings that may be available.
    (c) Device Data
    You may manage how your mobile device and mobile browser share certain Device Data with TSL, as well as how your mobile browser handles Cookies by adjusting the privacy and security settings on your mobile device. Please refer to instructions provided by your mobile service provider or the manufacturer of your device to learn how to adjust your settings.
    (d) E-mails from Business Partners
    If you wish to opt out of receiving offers directly from our Business Partners, you can follow the opt-out instructions in the emails that they send you.
    </p>  <p>

    <h3> 3. How TSL Uses Information </h3>
    We (or our Vendors on our behalf), use information collected as described in this Privacy Statement to operate, maintain and improve the Site and our services. We also use the information you provide to enable us to process your orders and to provide you with the services and information offered through our website; which you requested. Furthermore, your information is used to administer your account with us; verify and carry out financial transactions in relation to payments you make online; audit the downloading of data from our website; improve the layout and/or content of the pages of our website and customize them for users; identify visitors on our website; carry out research on our users' demographics; send you information we think you may find useful or which you have requested from us, including information about our products and services, provided you have indicated that you have not objected to being contacted for these purposes. Subject to obtaining your consent we may contact you by email with details of other products and services.
    We also may use information collected as described in this Privacy Statement with your consent or as otherwise required or permitted by law.
    4. When and Why TSL Discloses Information   </p>  <p>
    We (or our Vendors on our behalf) may share your Personal Information as required or permitted by law:
    with any TSL Affiliate who may only use the Personal Information for the purposes described in this Privacy Statement;
    with our Vendors to provide services for us and who are required to protect the Personal Information;
    to report or collect on debts owed to us or our Business Partners;   </p>  <p>
    with relevant Business Partners with whom we jointly offer products and services;   </p>  <p>
    to facilitate a direct relationship with you, including in connection with any promotion we administer on behalf of the Business Partner;
    to enable electronic communications with you as part of purchase, a sponsored reward, offer, contest, program, or other activity in which you have elected to participate;
    to the extent you have purchased a ticket or services offered by a Business Partner or participated in an offer, rewards, contest or other promotion sponsored or offered through TSL on behalf of that Business Partner;
    with a purchaser of TSL or any of the TSL Affiliates (or their assets);
       </p>  <p>
    to comply with legal orders and government requests, or as needed to support auditing, compliance, and corporate governance functions;
    to combat fraud or criminal activity, and to protect our rights or those of our Affiliates, users, and Business Partners, or as part of legal proceedings affecting TSL;
    in response to a subpoena, or similar legal process, including to law enforcement agencies, regulators, and courts in the Federal Republic of Nigeria and other countries where we operate; or with your consent;
    We encourage Business Partners to adopt and post privacy policies. However, their use of Personal Information obtained through TSL is governed by their privacy policies and is not subject to our control;
    with Business Partners for their own analysis and research;
    to facilitate targeted content and ads.
    5. How TSL Collects Information
       </p>  <p>
    You provide us with your Personal Information when you register, subscribe, create an account, make a purchase, or otherwise when you provide us with your Personal Information during your interaction with the Site and other Promotions. We also collect Personal Information when you contact us online for customer service and other support using self-help tools, such as email, text, or by posting to an Online Community.
    TSL will use such information in accordance with applicable laws. Such information, when combined with Personal Information collected as provided in this Privacy Statement, will also be handled in accordance with this Privacy Statement. We also use cookies, tags, web beacons, local shared objects, files, tools and programs to keep records, store your preferences, improve our advertising, and collect Non-Identifying Information, including Device Data and your interaction with the Site and our Business Partners' web sites.
    Device Data may be collected when your device interacts with the Site and TSL, even if you are not logged into the Site using your device. If you have questions about the security and privacy settings of your mobile device, please refer to instructions from your mobile service provider or the manufacturer of your device to learn how to adjust your settings.
    6. Security of Personal Information
    TSL has implemented an information security program that contains administrative, technical and physical controls that are designed to reasonably safeguard Personal Information. No method of transmission over the Internet, or method of electronic storage, is 100% secure, therefore, we cannot guarantee absolute security. If you have any questions about security on our Web site, you can contact us at privacy@bus.com.
    7. Accessing and Correcting Personal Information
   </p>  <p>
    You can access, update and delete information you provided to us by managing this information through your online account or sending us an email at privacy@bus.com. Keeping your Personal Information current helps ensure that we, our Affiliates and Business Partners offer you the offers that are most relevant to you.
    If you want to deactivate your TSL account or have other questions or requests, please contact us. While we are ready to assist you in managing your subscriptions, deactivating your account, and removing your active profile, we cannot always delete records of past interactions and transactions. For example, we are required to retain records relating to previous purchases on the Site for financial reporting and compliance reasons.
    We will retain your Personal Information for as long as your account is active or as needed to provide you services and to maintain a record of your transactions for financial reporting purposes. If you wish to deactivate your account or request that we no longer use your Personal Information to provide you services contact us at support@bus.com. We will retain and use your Personal Information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements.
    9. Privacy Practices of Third Parties
    This Privacy Statement only addresses the use and disclosure of information by TSL through your interaction with the Site. Other websites that may be accessible through links from the Site and Other Programs may have their own privacy statements and personal information collection, use, and disclosure practices. Our Business Partners may also have their own privacy statements. We encourage you to familiarize yourself with the privacy statements provided by these other parties prior to providing them with information or taking advantage of a sponsored offer or promotion.
    11. Children’s Privacy
    TSL does not intend that any portion of the Site will be accessed or used by children or individuals under the age of eighteen (18) years, and such use is prohibited. The Site is designed and intended for adults. By using TSL, you represent that you are at least eighteen years old and understand that you must be at least eighteen years old in order to create an account and purchase the tickets or any other services advertised through the Site. We will promptly delete information associated with any account we obtain actual knowledge is associated with a registered user who is not at least eighteen years old.
 </p>



    </div></div></div>
@stop
