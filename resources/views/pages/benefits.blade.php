@extends('layouts.app')

@section('content')
    <div id="icon-box">
        <img src="{{ URL::asset('img/booking-banner.jpg') }}" width="100%">
    </div>

<div class="gap"></div>

<div class="container">
  <div class="row">
    <div class="col-md-8" style="margin: 10%;line-height:25px; font-size: 15px; color: #7c7e7f; font-family: Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif">

      <h3 style="font-size: 20px; color: red ">Benefits of our services</h3>
        <br>

<p>
   Pre-book bus travel tickets through us and enjoy the benefits below:
</p>
<ul>
    <li><span class="bold">Save time: </span>avoid long queues and waiting, visit parks at the actual departure time.</li>
    <li><span class="bold">Convenience: </span>24/7 access to book bus tickets through our website, telephone access from the comfort of your home, office, school, etc.</li>
    <li><span class="bold">Best services: </span>we help you discover the best transport companies.</li>
    <li><span class="bold">Care: </span>we offer you exceptional customer care service.</li>
    <li><span class="bold">Secure payment: </span>you can pay for tickets through Bank Deposit, Online banking, ATM cards.</li>
    <br>
    <li><span class="bold">No extra charges: </span>same price as booking from the park.</li>
    <li><span class="bold">Wide operator selection: </span>get instant access to all transport companies and travel routes on your finger tips, which improves your chances of finding buses anytime.</li>
    <li><span class="bold">Secure Payment: </span>You can pay for tickets through bank deposits, online banking, ATM card transfer or cash
    on delivery (COD) service where we send an agent to your location for payment collection.</li>
</ul>

<p >
    We will be glad to have you <a style="color: red"  href="{{ url('/') }}">pre-book your bus travel </a>tickets through us.

</p>

    </div>
  </div>
</div>
@stop