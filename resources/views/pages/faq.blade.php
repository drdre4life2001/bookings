@extends('layouts.app')

@section('content')

	<div id="icon-box">
		<img src="{{ URL::asset('img/booking-banner.jpg') }}" width="100%">
	</div>
<div class="gap"></div>


<div class="container" style="margin-bottom:3%; font-size: 15px; margin-top:3%; padding: 10px; color: #7c7e7f;  ">

	<div class="row">

		@if(session('success'))
			<div class="alert alert-danger">
				<ul>
					<li> {{session('success')}}</li>
				</ul>

			</div>

		@endif
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="panel-group" id="accordion">
		        <div class="faqHeader">Frequently Asked Questions</div>
				<br>
	        	<div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">What payment methods are available?</a>
		                </h4>
		            </div>
		            <div id="collapseOne" class="panel-collapse collapse in">
		                <div class="panel-body">
		                    Online payment, bank transfer, Payment at bank and cash payment at bus.com.ng outlets
		                </div>
		            </div>
	        	</div>
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Can I book on the platform and pay at the park?</a>
		                </h4>
		            </div>
		            <div id="collapseTwo" class="panel-collapse collapse">
		                <div class="panel-body">
		                    To enjoy our special travel packages  payments should only be made on bus.com.ng or at approved outlets
		                </div>
		            </div>
		        </div>
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Can I book ahead of my travel date?</a>
		                </h4>
		            </div>
		            <div id="collapseThree" class="panel-collapse collapse">
		                <div class="panel-body">
		                    Yes, you can book ahead of your travel date to gain our special offers
		                </div>
		            </div>
		        </div>
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Can I change my booking time or destination after I have paid for an initial schedule?</a>
		                </h4>
		            </div>
		            <div id="collapseFour" class="panel-collapse collapse">
		                <div class="panel-body">
		                    Yes you can, you can call the customer line or visit any bus.com.ng outlets closer to your location.
		                </div>
		            </div>
		        </div>
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">How do I get accepted at the operator park, once I purchase my ticket through bus.com.ng?</a>
		                </h4>
		            </div>
		            <div id="collapseFive" class="panel-collapse collapse">
		                <div class="panel-body">
		                    All tickets issued from bus.com.ng has a unique booking codes and is recognized by all our partnered operators.
		                </div>
		            </div>
		        </div>

		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Do I get a refund if I cancel my trip before the departure date?</a>
		                </h4>
		            </div>
		            <div id="collapseSix" class="panel-collapse collapse">
		                <div class="panel-body">
		                    No, cancellation has to be done 24 hours before travel date. Visit any of our outlets or call customer line if you wish to have an open ticket valid for 6 months.
		                </div>
		            </div>
		        </div>

		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">

		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Do I get a refund if I miss my trip?</a>
		                </h4>
		            </div>
		            <div id="collapseSeven" class="panel-collapse collapse">
		                <div class="panel-body">
							No, ticket can be reused, however an admin fee might apply. Visit any of our outlets or call our customer line.

		                </div>
		            </div>
		        </div>

		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">Do I gain any benefits for using bus.com.ng platform?</a>
		                </h4>
		            </div>
		            <div id="collapseEight" class="panel-collapse collapse">
		                <div class="panel-body">
							Yes, we offer insurance coverage and rewards (travel points) for every time you book and pay for your tickets.

		                </div>
		            </div>
		        </div>

		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine"> How do I redeem my reward?</a>
		                </h4>
		            </div>
		            <div id="collapseNine" class="panel-collapse collapse">
		                <div class="panel-body">
							You can redeem your points towards your next trip or redeem your points towards paying other forms of bills.
		                </div>
		            </div>
		        </div>
				<div class="panel panel-default">
		            <div class="panel-heading">
		                <h4 class="panel-title">
		                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen"> How much points do I need to earn before I can redeem them?</a>
		                </h4>
		            </div>
		            <div id="collapseTen" class="panel-collapse collapse">
		                <div class="panel-body">

		                </div>
		            </div>
		     	</div>
		    </div>
		</div>
	</div>
</div>

{{-- FAQ
What payment methods are available?
Ans: Online payment, bank transfer, Payment at bank and cash payment at bus.com.ng outlets
Can I book on the platform and pay at the park?
Ans: to enjoy our special travel packages  payments should only be made on bus.com.ng or at approved outlets
Can I book ahead of my travel date?
Ans: Yes, you can book ahead of your travel date to gain our special offers
Can I change my booking time or destination after I have paid for an initial schedule?
Ans: Yes you can, you can call the customer line or visit any bus.com.ng outlets closer to your location
How do I get accepted at the operator park, once I purchase my ticket through bus.com.ng?
Ans: all tickets issued from bus.com.ng has a unique booking codes and is recognized by all our partnered operators
Do I get a refund if I cancel my trip before the departure date?
Ans: No, cancellation has to be done 24 hours before travel date. Visit any of our outlets or call customer line if you wish to have an open ticket valid for 6 months
Do I get a refund if I miss my trip?
Ans: No, ticket can be reused, however an admin fee might apply. Visit any of our outlets or call our customer line.

Do I gain any benefits for using bus.com.ng platform?
Ans: Yes, we offer insurance coverage and rewards (travel points) for every time you book and pay for your tickets.

How do I redeem my reward?
Ans: You can redeem your points towards your next trip or redeem your points towards paying other forms of bills.
How much points do I need to earn before I can redeem them?
 --}}
@stop