@extends('layouts.app')

@section('content')

<div class="container">
            <!-- <h1 class="page-title text-center">Contact Us</h1> -->
        </div>
        <div class="container">
        </div>
            <!-- <img src="{{ asset('img/subscribe.jpg') }}" height="500px"> -->

    <iframe class="col-md-12" height="300px"
    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBnPx0T8DqRqBwVT_r94-1evJGqet-3WME&q=12C+akin+ogunlewe,Victoria+Island,Lagos,Nigeria"

    frameborder="0" style="border:0" ></iframe>
<div class="container"></div>


        <div class="container" style=" margin-bottom:5%; margin-top:3%; font-size: 15px;   color: #7c7e7f;                                    ">
            <div class="gap"></div>
            <div class="row">
                <div class="col-md-7" >
                    <p style="font-size: 30px; color: red">You can drop us a line here.</p>
                    @if (session('status'))
                        <div class="alert alert-success">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">×</span>
                            </button>
                            <p class="text-small">{{session('status')}}</p>
                        </div>
                    @endif
                    <form class="mt30" method="post" action="{{ url('sendemail') }}">
                    {{csrf_field()}}
                        <div class="row">


                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                  <label>Interest</label>
                                  <select id="option" name="option" class="form-control">
                                    <option value="Inquiry">Inquiry</option>
                                    <option value="Booking code issue">Issue with Booking Code</option>
                                    <option value="Issue with Online Payment">Issue with Online Payment</option>
                                    <option value="Issue With Bank Transfer">Issue With Bank Transfer</option>
                                    <option value="Suggestion/Comments">Suggestion/Comments</option>
                                    <option value="Others">Others</option>
                                  </select>
                                </div>
                            </div>

                              <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="form-group">
                                      <label>Name</label>
                                      <input class="form-control" name="name" type="text" required="required">
                                      <input class="form-control" name="type" value="inquiry" type="hidden" required="required">
                                  </div>
                              </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input class="form-control" name="email" type="email" required="required">
                                    <input class="form-control" name="subject" value="Inquiry" type="hidden">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea name="message" class="form-control" required="required"></textarea>
                        </div>
                        <input class="btn btn-danger" type="submit" value="Send Message">
                    </form>
                </div>
                <div class="col-md-4 hidden-sm hidden-xs">
                    <aside class="sidebar-right" style="font-family: Helvetica, Arial, Lucida Grande, sans-serif; line-height:20px;" >
                        <ul class="address-list list">
                            <li>
                                <h5>Email</h5><a style="color: red" href="#">info@bus.com.ng</a>
                            </li>
                            <li>
                                <h5>Phone Number</h5><a style="color: red" href="#">+234-080 30 500 600</a>
                            </li>
                            <li>
                                <h5>Skype</h5><a style="color: red" href="#">busng</a>
                            </li>
                            <li>
                                <h5>Address</h5><address>Bus Booking Ltd.<br>21C Akin Ogunlewe street, off Ligali Ayorinde street<br>Victoria Island, LA 23401<br>Nigeria<br></address>
                            </li>
                        </ul>
                    </aside>
                </div>
            </div>
            <div class="gap"></div>
        </div>
@stop
