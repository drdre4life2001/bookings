@extends('layouts.app_home')

@section('content')
    <div id="main-content">
        <div class="container">

            <div class="row">
                <div class="col-md-6">
                <!-- <form method="post" action="{{ url('trips/loading') }}">
                    <input type="text" name="name">

                    <input type="submit" value="LET'S GO!">
                </form> -->



                    <div class="wizard-container">
                        <div class="card wizard-card" data-color="red" id="wizard">
                            @if($msg==1)
                                <div class="alert alert-success">
                                    <ul>
                                        <li>Charter booking was successfull, You will be contacted soon using the information provided</li>

                                    </ul>
                                    <br>
                                    <p class="btn-success" style="color:white; text-align: center"><a href="/" style="color:white;">Back to home page</a></p>

                                </div>

                            @endif


                        </div>
                    </div> <!-- wizard container -->

                </div>

            </div>

        </div>
        {{-- Modal for Charter BUs Here --}}

        <div class="modal fade" id="modal-charter-bus">
            <div class="modal-dialog" style="padding:2%">
                <div class="modal-content modal-lg" style="=padding:2%">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <center>
                            <h3 class="modal-title" style="color: #f44336;">Charter Bus</h3>
                        </center>
                    </div>
                    <div class="modal-body" style="padding:46px !important">
                        <div id="form-charter">
                            <form method="POST" action="{{ url('charter_payment_success') }}">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-lg form-group-icon-left"><i style="color: #f44336;" class="fa fa-map-marker input-icon"></i>
                                            <label>Pick-up Location</label>
                                            <input id="pickup" name="pickup" class="typeahead form-control" placeholder="City, State" type="text" required="required" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-lg form-group-icon-left"><i style="color: #f44336;" class="fa fa-map-marker input-icon"></i>
                                            <label>Drop-off Location</label>
                                            <input id="dropoff" name="dropoff" class="typeahead form-control" placeholder="City, State" type="text" required="required" />
                                        </div>
                                    </div>
                                </div>

                                <div class="input-daterange" data-date-format="M d, D">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i style="color: #f44336;" class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                <label>Pick-up Date</label>
                                                <input id="date" type="date" class="form-control" name="start" required="required" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i style="color: #f44336;" class="fa fa-clock-o input-icon input-icon-highlight"></i>
                                                <label>Pick-up Time</label>
                                                <input id="pickup_time" class="time-pick form-control" name="pickup_time" value="12:00 AM" type="text" required="required" />
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group form-group-lg form-group-icon-left">
                                                <label>Prefered Operator</label>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="input-daterange" data-date-format="M d, D">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-left"><i style="color: #f44336;" class="fa fa-user input-icon input-icon-highlight"></i>
                                                <label>Contact Name</label>
                                                <input id="contact_name" class="form-control" name="contact_name" type="text" required="required" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i style="color: #f44336;" class="fa fa-phone input-icon input-icon-highlight"></i>
                                                <label>Contact Phone</label>
                                                <input id="contact_phone" class="form-control" name="contact_phone" type="text" required="required" />
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i style="color: #f44336;" class="fa fa-envelope input-icon input-icon-highlight"></i>
                                                <label>Contact Email</label>
                                                <input id="contact_email" class="form-control" name="contact_email" type="text" required="required"/>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group form-group-lg form-group-icon-left">
                                                    <i style="color: #f44336;" class="fa fa-user input-icon input-icon-highlight"></i>
                                                    <label>Next of Kin</label>
                                                    <input id="kin" class="form-control" name="kin" type="text" />
                                                </div>
                                            </div>



                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left">
                                                <i style="color: #f44336;" class="fa fa-phone input-icon input-icon-highlight"></i>
                                                <label>Next of Kin Phone</label>
                                                <input id="phone_kin" class="form-control" name="phone_kin" type="text" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-ift">
                                                <label>No. of Passengers</label>
                                                <input id="passenger" class="form-control" name="passenger" type="text" required="required" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i style="color: #f44336;" class="fa fa-money input-icon input-icon-highlight"></i>
                                                <label>Budget</label>
                                                <input class="form-control" name="budget" type="text" required="required" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i style="color: #f44336;" class="fa fa-map-marker input-icon input-icon-highlight"></i>
                                                <label>Pick Up Address</label>
                                                <input class="form-control" name="pickup_address" type="text" required="required" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i style="color: #f44336;" class="fa fa-map-marker input-icon input-icon-highlight"></i>
                                                <label>Drop Off Address</label>
                                                <input class="form-control" name="dropoff_address" type="text" required="required" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <div class="col-md-offset-3">
                                        <button id="submit_charter" type="submit" class="btn btn-danger btn-lg" >Submit Charter Request</button>
                                        <div id="outing"></div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop


        {{-- <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Trigger modal</a> --}}


        @section('scripts')
            <script type="text/javascript">


                window.setTimeout(function() {
                    window.location.href = '/';
                }, 5000);


            </script>

@stop


