@extends('layouts.app')

@section('content')
<div class="gap"></div>
<div class="container">
    <div class="pull-left hidden-sm hidden-xs" style="width: 55%;margin-top: 30px;">
        <h3>Issue electronic tickets across your parks.</h3>
        <p >Use our solutions and take advantage of the growing online market. Deploy our Electronic Ticketing Solution and Mobile devices  for  e-inventory management,  seamless travelling for customers and real time monitoring of revenue collections</p>
    </div>
</div>

<div id="cta" style="width:60%; margin-left: 100px;  background: url({{ asset('img/Partnership.jpg') }})no-repeat;background-size:cover;" class="hidden-sm hidden-xs">
    <div class="row">
        <div class="container">
					@if (session('status'))
                        <div class="alert alert-success">
                            <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">×</span>
                            </button>
                            <p class="text-small">{{session('status')}}</p>
                        </div>
                    @endif
            <div class="pull-right" >
                <form style="width: 400px;background: #3c3c40;padding: 15px 25px;"  method="post" action="{{ url('sendemail') }}">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Interest</label>
                                <select id="option" name="option" class="form-control">
                                    <option value="All">All</option>
                                    <option value="Operator Ticketing Solution">Operator Ticketing Solution</option>
                                    <option value="bus.com.ng">bus.com.ng</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name of contact person</label>
                                <input id="name" name="name" class="form-control" type="text" required="required" />
                                <input id="type" name="type" class="form-control" type="hidden" value="partnership" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Mobile</label>
                                <input id="phone" name="phone" class="form-control" type="text" required="required" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>E-mail</label>
                                <input id="email" name="email" class="form-control" type="email" required="required" />
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Company Name</label>
                                <input id="company" name="company" class="form-control" type="text" required="required" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Lagos Address</label>
                        <textarea id="lag_address" name="lag_address" class="form-control" required="required"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Headquarter Address</label>
                        <textarea id="hqtr_address" name="hqtr_address" class="form-control" required="required"></textarea>
                    </div>
                    <input class="btn btn-primary" type="submit" value="Submit" />
                </form>
            </div>

        </div>
    </div>
</div>
<div class="container">
    <div class="gap"></div>
    <div class="row">
        <div class="col-md-6">
            <a href="#">
                <img class="img-responsive" src="{{ asset('img/ots.png') }}" alt="">
            </a>
        </div>
        <div class="col-md-6">
            <h3>Operator Ticketing System</h3>
            <ul>
                <li>Electronically manage all inventory (trip management). </li>
                <li>Issue electronic bus ticket to a customer from the park. </li>
                <li>Configure all routes covered by the company across all parks. </li>
                <li>View all tickets booked from the agent Bus.com.ng booking System. </li>
                <li>Register Cash payments and swipe customer cards via mPOS.</li>
                <li>View and track in real time all revenue and operational activities via dashboard.</li>
            </ul>
        </div>
    </div>
    <div class="gap"></div>
    <div class="row">
        <div class="col-md-6">
            <a href="#">
                <img class="img-responsive" src="{{ asset('img/mobile_pos.png') }}" alt="">
            </a>
        </div>
        <div class="col-md-6">
            <h3>Mobile Ticketing System</h3>
            <ul>
                <li>
                    Setup mobile Agent ticketers to issue electronic tickets to travelers from the bus without a desktop counter interface
                </li>
                <li>
                    View issued tickets and revenue collected in real time
                </li>
                <li>
                    Register cash collections, swipe and insert card payment through the Mobile Point of Sale (mPOS) electronic payment device handled by the mobile Agent ticketer
                </li>
                <li>
                    Electronically issue SMS or email tickets as well as print through the mobile printer
                </li>

            </ul>
        </div>
    </div>
    <div class="gap"></div>
    <div class="row">
        <div class="col-md-6">
            <a href="#">
                <img class="img-responsive" src="{{ asset('img/bus.com.ng.png') }}" alt="">
            </a>
        </div>
        <div class="col-md-6">
            <h3>Bus.com.ng</h3>
            <ul>
                <li>Increases bus operator's ticket sales channel.</li>
                <li>Aggregates all bus operators on one platform.</li>
                <li>Issues electronic bus ticket sales to travellers.</li>
                <li>Desktop and mobile enabled</li>
                <li>Integrates with any bus operator's online or local platform, if already exist within the company.</li>
            </ul>

        </div>
    </div>

    <div class="gap"></div>
</div>
<script src="{{ URL::asset('/js/jquery.js') }}" > </script>
<script src="{{ URL::asset('/js/toastr.min.js') }}" > </script>
@stop
