@extends('layouts.app')

@section('content')
<div class="gap"></div>

<div class="container">
    <div class="row">

        <div class="col-sm-8 blog-main">
            <div class="blog-post">
                <h2 class="blog-post-title" id="shelia">{{ $article->title }}</h2>
                <p class="alert-info">     {{ $article->created_at }}</p>
                <?php echo $article->body ?>
                <hr>
            </div>
        </div><!-- /.blog-main -->

        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">

            <div class="sidebar-module">
                <h4>Other Posts</h4>
                <ol class="list-unstyled">
                    @foreach($articles as $item)
                    <li><a href="{{ url('pages/view-article', [$item->slug]) }}">{{ $item->title }}</a></li>
                    @endforeach
                </ol>
            </div>

            <div class="sidebar-module">
                <h4>Follow us:</h4>
                <ol class="list-unstyled">
                    <li><a href="https://instagram.com/bus.com.ng"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a></li>
                    <li><a href="http://twitter.com/BusComNg"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a></li>
                    <li><a href="https://facebook.com/bus.com.ng"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
                </ol>
            </div>
        </div><!-- /.blog-sidebar -->

    </div><!-- /.row -->


    <div class="gap"></div>
</div>

@stop
