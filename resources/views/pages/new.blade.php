@extends('layouts.app')
@section('content')

    <div class="container">
        <h1 class="page-title"></h1>
    </div>

    <div class="container">
      <div class="blog-header">
       <h1 class="blog-title text-center">Welcome To Our Blog</h1>
     </div>
     <div class="row">

             <div class="col-sm-8 blog-main">
                 @foreach($articles as $article)
               <div class="blog-post">
                 <h2 class="blog-post-title" >{{ $article->title }}</h2>
                 <p class="blog-time"><em> Posted on: {{ date('F d, Y', strtotime($article->created_at)) }}</em></p>
                   <!--{{ strip_tags($article->body) }}-->
                  <div class="blog-body"> <?php echo substr($article->body, 0); ?> </div>
                   <a class="readmore" href="{{ url('pages/view-article', [$article->slug]) }}">Read More</a>
                   <hr>
               </div>
                 @endforeach
             </div><!-- /.blog-main -->

             <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
               <div class="sidebar-module">
                 <h4>Follow us:</h4>
                 <ol class="list-unstyled">
                   <li><a href="https://instagram.com/bus.com.ng"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a></li>
                   <li><a href="http://twitter.com/BusComNg"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a></li>
                   <li><a href="https://facebook.com/bus.com.ng"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
                 </ol>
               </div>
             </div><!-- /.blog-sidebar -->

           </div><!-- /.row -->

         </div><!-- /.container -->


<!--
        <h3>What's New</h3>

        <p>
          <iframe width="300"  src="https://www.youtube.com/embed/MsQybyXhte8" frameborder="0" allowfullscreen></iframe>
        </p>

        <p>
          <img src="{{ asset('img/bus21.jpg') }}">
        </p>



    </div>
  <div class="gap">

  </div>
-->

@stop
