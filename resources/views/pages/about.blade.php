@extends('layouts.app')

@section('content')

    <div id="icon-box">
        <img src="{{ URL::asset('img/booking-banner.jpg') }}" width="100%">
    </div>
        <div class="container">
            <div class="row" style="margin-bottom:5%; font-size: 15px; margin-top:5%; line-height:25px; color:#7c7e7f; font-family: Helvetica, Arial, Lucida Grande, sans-serif">
                <div class="col-md-6" >

                    <h2 style="font-size: 20px; margin: 10px; color: red">About Us</h2>
                    <p class="text-bigger ">
                        “Book tickets smarter, Earn rewards”
                        <br>

                    </p>


                    <ul>
                        <li>We have the top bus operators on our platform.</li>
                        <li>Save time and money with incredible discounts online</li>
                        <li>Redeem rewards towards travel and other payments. </li>
                        <li>Travel across over 30 states nationwide</li>
                    </ul>
                </div>
                <div class="col-md-4" style="margin-top: 5%;">
                    <img src="https://cdn.shopify.com/s/files/1/0996/0064/files/Yophishop_About_Us.jpeg?10358541195661059387" ></img>
                </div>
            </div>


            <div class="gap"></div>
        </div>
        <div class="bg-holder">
            <div class="bg-parallax" style="background-image:url({{ asset('img/img16.png')  }} );"></div>
            <div class="bg-mask"></div>
            <div class="bg-holder-content">
                <div class="container">
                    <div class="gap gap-big text-white">
                        <div class="row">
                            <div class="col-md-10" style="margin-bottom:10%; font-size:15px; padding: 10px;font-family: Helvetica, Arial, Lucida Grande, sans-serif; color: #7c7e7f; line-height:25px;">
                                <h2  style="font-size: 20px; margin-bottom:10px; color: red">Our approach</h2>
                                <p class="text-bigger">Bus.com.ng is the largest online bus ticket sales provider and a software service provider for bus operators. Powered by TechAdvance we are offer an integrated platform for online bus tickets bookings and inventory management. We provide online ticket sales for travellers, electronic payment collections, electronic inventory management, data analytics and aggregation systems for bus operators.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        {{-- <div class="container">
            <div class="gap"></div>
            <h2>Our Approach</h2>
            <div class="row">
                <div class="col-md-8">
                    <p class="text-bigger">Cubilia gravida mus senectus donec consectetur dis ac vel quis a suscipit potenti purus aptent nibh gravida aliquet vestibulum varius dictum consectetur semper consectetur at varius donec sociis habitasse vivamus eget faucibus tempus donec lorem etiam volutpat blandit aliquam varius molestie nibh mattis adipiscing sodales dictumst volutpat quam rhoncus sodales</p>
                </div>
            </div>

            <div class="gap"></div>
        </div> --}}
@stop
