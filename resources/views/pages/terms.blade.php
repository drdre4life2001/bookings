@extends('layouts.app')

@section('content')

<div class="gap"></div>

<div class="container">
  <div class="row">
    <div class="col-md-8">

    <h4>Terms and Conditions</h4>



<h5>  Important Points</h5>
<ol>
    <li><p><strong> Bus.com.ng </strong>is a bus operator agency. It does not operate bus services of its own. In order to provide a comprehensive choice of bus operators, departure times and prices to customers, it has tied up with many bus operators. 

        We advice customers to choose bus operators they are aware of and whose service they are comfortable with.

        <strong>Our  responsibilities</strong> include:
        <ul>
            <li>   Issuing a valid ticket (a ticket that will be accepted by the bus operator) for its’ network of  bus operations </li>
            <li>    Providing refund and support in the event of cancellation</li>
            <li>    Providing customer support and information in case of any delays / inconvenience</li>
            <li>    Providing efficient and convenient payment methods for our customers which includes; Online Bank Transfers, Interswitch,
             ATM Card Transfer and Cash on Delivery (COD).</li>
        </ul>
  <strong>
      Our responsibilities</strong>do <strong> NOT </strong>include:

        <ul>
            <li>    The bus operator’s bus not departing / reaching on time</li>
            <li>    The bus operator’s employees being rude</li>
            <li>   The bus operator’s bus seats etc not being up to the customer’s expectation</li>
            <li>   The bus operator canceling the trip due to unavoidable reasons  </li>
            <li>  The baggage of the customer getting lost / stolen / damaged</li>
            <li>   The bus operator changing a customer’s seat at the last minute to accommodate a lady / child</li>
            <li>   The customer waiting at the wrong boarding point (please call the bus operator to find out the exact
                boarding point if you are not a regular traveler on that particular bus)</li>
            <li>   The bus operator changing the boarding point and/or using a pick-up vehicle at the boarding point to take
                customers to the bus departure point </li>
        </ul>
      </p>
    </li>


    <li><p> The <strong>arrival </strong>and <strong>departure times</strong> mentioned on the ticket are only <strong> tentative timings </strong>. However the bus will not leave the source before the time that is mentioned on the ticket. 
        </p></li>
    <li> <p>Passengers are required to furnish the following at the time of boarding the bus:
        
        <ul>
            <li>    A copy of the SMS Ticket (An SMS sent to you after placing an order on our website). </li>
            <li> Identity proof (Driving license, Student ID card, Company ID card, Passport, National Id Card or Voter ID card). </li>
        </ul>
        Failing to do so, they may not be allowed to board the bus. 
        </p> </li>
    <li> <p><strong>Change of bus:</strong> In case the bus operator changes the type of bus due to some reason, we will refund the differential amount to the customer upon being intimated by the customers in 24 hours of the journey. 
        </p></li>
    <li><p> <strong>Cancellation Policy:</strong> The tickets booked through Bus.com.ng are cancelable. Kindly note that the money is not transferred back to the passenger. Instead the money will be refunded in the form of a Cash Coupon which can be used during your next transaction. This coupon is valid up to 3 months from the date of cancellation. 

        <p>Our cancellation fees is 30%</p>
        <p>
        Please note that the cancellation fee and cancellation period may differ from one bus operator to another. Please contact any of our executives for complete details or enter your ticket number on the print ticket tab to read the cancellation policy for your ticket. 
        </p>
        </p>
    </li>
    <li><p> In case one needs the refund to be credited back to his/her bank account, please write your cash coupon details to info@bus.com.ng

        </p></li>
        <em> The transaction charges or the home delivery charges, will not be refunded in the event of ticket cancellation </em>
        <li><p>In case a booking confirmation  sms gets delayed or fails because of technical reasons or as a result of incorrect phone number provided by the user etc, a ticket will be considered<strong> 'booked'</strong> as long as the ticket shows up on the<strong> confirmation page </strong>of www.bus.com.ng 
            </p> </li>
</ol>
    <h5>Communication Policy</h5>
<ol>
    <li><p>By accepting the terms and conditions the customer accepts that we may send the alerts to the mobile phone number provided by the customer while registering for the service or to any such number replaced and informed by the customer. The customer acknowledges that the alerts will be received only if the mobile phone is in ‘On’ mode to receive the SMS. If the mobile phone is in ‘Off’’ mode then the customer may not get / get after delay any alerts sent during such period.
        </p></li>
    <li><p>We will make best efforts to provide the service and it shall be deemed that the customer shall have received the information sent from us as an alert on the mobile phone number provided during the course of ticket booking and we shall not be under any obligation to confirm the authenticity of the person(s) receiving the alert. The customer cannot hold us liable for non-availability of the service in any manner whatsoever.
        </p> </li>
    <li><p>The customer acknowledges that the SMS service provided by Bus.com.ng is a facility provided for the customer’s convenience and that it may be susceptible to error, omission and/ or inaccuracy. In the event the customer observes any error in the information provided in the alert, we shall be immediately informed about the same by the customer and we will make best possible efforts to rectify the error as early as possible. The customer shall not hold us liable for any loss, damages, claim, expense including legal cost that may be incurred/ suffered by the customer on account of the SMS facility.
        </p></li>
    <li> <p>The customer acknowledges that the clarity, readability, accuracy, and promptness of providing the service depend on many factors including the infrastructure, connectivity of the service provider. We shall not be responsible for any non-delivery, delayed delivery or distortion of the alert in any way whatsoever.
        </p></li>
    <li> <p>The customer agrees to indemnify and hold harmless Bus.com.ng and the SMS service provider including its officials from any damages, claims, demands, proceedings, loss, cost, charges and expenses whatsoever including legal charges and attorney fees which we and the SMS service provider may at any time incur, sustain, suffer or be put to as a consequence of or arising out of (i) misuse, improper or fraudulent information provided by the customer, (ii) the customer providing incorrect number or providing a number that belongs to that of an unrelated third party, and/or (iii) the customer receiving any message relating to the reservation number, travel itinerary information, booking confirmation, modification to a ticket, cancellation of ticket, change in bus schedule, delay, and/or rescheduling from us and/or the SMS service provider.
        </p></li>
    <li> <p>By accepting the terms and conditions the customer acknowledges and agrees that we may call the mobile phone number provided by the customer while registering for the service or to any such number replaced and informed by the customer, for the purpose of collecting feedback from the customer regarding their travel, the bus facilities and/or services of the bus operator.
        </p></li>
    <li><p>Grievances and claims related to the bus journey should be reported to Bus.com.ng support team within 10 days of your travel date.
        </p></li>
</ol>



    </div></div></div>
@stop