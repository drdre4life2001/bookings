@extends('layouts.app')

@section('content')

<div class="gap"></div>

<div class="container">
  <div class="row">
    <div class="col-md-8">
  
  <h4>How It Works</h4>

<p>
    You can pre-book your bus travel tickets through telephone oronline find steps below; 
</p>


 <h4 class="bold">FOR ONLINE BOOKING: </h4>


 <ol>
            <li>
                Visit <a href="http://www.bus.com.ng">www.bus.com.ng</a> and search for a bus by entering your trip details; 
            </li>
            <li>
               Select a transport company and fill in the passengers details; then you get a reservation sms.
            </li>
            <li>
                Select a payment option and follow the instructions to make your payment.
            </li>
       
     <li>
       Once we confirm your payment, we will send you an smsTicket.
     </li>
     <li>
       On your travel date and time, present your smsTicket to the park-agents/bus-operators for confirmation; and you will be guided to join the trip.
     </li>
     </ol>
    


 <h4><b>FOR TELEPHONE BOOKING:</b> Call <strong>08030500600</strong></h4>
    


 


<!--
<p>
   That’s all!
</p>-->

<a class="btn btn-primary" href="{{ url('/') }}">Book A Ticket Now</a><br/><br/>



  </div>
  </div>
</div>
@stop