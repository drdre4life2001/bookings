<?php

return [

    /*
    |--------------------------------------------------------------------------
    | ApI Server Host
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'host' => 'https://test.bustickets.ng',
    'api-host' => env('API_HOST'),
    'img-host' => env('IMG_HOST'),


    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */
 'online' => [

        'banks'                 => env('API_HOST').'banks',
        'get-parks'             => env('API_HOST').'get-parks',
        'get-dest-parks'        => env('API_HOST').'get-dest-parks',
        'get-articles'          => env('API_HOST').'get-articles',
        'get-article'           => env('API_HOST').'get-article',
		'get-sample-article'    => env('API_HOST').'get-sample-articles',
        'get-feedbacks'         => env('API_HOST').'get-feedbacks',
        'get-dests'             => env('API_HOST').'get-dests',
        'get-park'              => env('API_HOST').'single-park',
        'get-operators'         => env('API_HOST').'get-operators',
        'get-trip'              => env('API_HOST').'get-trip',
        'search'                => env('API_HOST').'search-trip',
        'trip-from-booking-id'  => env('API_HOST').'get-trip-from-booking-id',
        'trip-from-charter-id'  => env('API_HOST').'get-trip-from-chartered-booking-id',
        'save-charter'          => env('API_HOST').'save-charter',
        'save-booking'          => env('API_HOST').'save-booking',
        'save-psg'              => env('API_HOST').'save-passengers',
        'save-transaction'      => env('API_HOST').'save-transactions',
        'save-customer'         => env('API_HOST').'save-customer',
        'get-customer-bookings' => env('API_HOST').'get-customer-bookings',
        'get-customer-data'         => env('API_HOST').'get-customer-data',
        'get-customer-data-from-phone'         => env('API_HOST').'get-customer-data-from-phone',
        'get-customer-stats' => env('API_HOST').'get-customer-stats',
        'save-feedback'             => env('API_HOST').'save-customer-feed-back',
        'save-customer-feedback'             => env('API_HOST').'save-feed-back',
        'get-operator-booking-rule' => env('API_HOST'). 'get-operator-booking-rule',
        'save-booked-seat'          => env('API_HOST'). 'save-booked-seat',
        'get-customer-booking-by-id'    => env('API_HOST'). 'get-customer-booking-by-id',
 ],

    'trips' => [
        'get-parks'                          => env('API_HOST').'trips/get-parks',
        'get-operators'                      => env('API_HOST').'trips/operators',
        'get-trip'                           => env('API_HOST').'trips/get-trip',
        'search'                             => env('API_HOST').'trips/search-trip',
        'trip-from-booking-id'               => env('API_HOST').'trips/get-trip-from-booking-id',
        'trip-from-charter-id'               => env('API_HOST').'trips/get-trip-from-chartered-booking-id',
    ],

     'bookings' => [

        'save-charter'              => env('API_HOST').'bookings/save-charter',
        'save-booking'              => env('API_HOST').'bookings/save-booking',
        'save-psg'                  => env('API_HOST').'bookings/save-passengers',
        'save-transaction'          => env('API_HOST').'bookings/save-transactions',

    ],

];