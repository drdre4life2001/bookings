<?php
return [
	'charter_alert_numbers' => [
						env("ADA_NUMBER"),
						env("GLORIA_NUMBER"),
						env("CUSTOMER_CARE_NUMBER"),
						],


	'convenience_fee' => env("CONVENIENCE_FEE", 150),
    'bus_com_fee' => env("BUS_FEE", 150),
						
];